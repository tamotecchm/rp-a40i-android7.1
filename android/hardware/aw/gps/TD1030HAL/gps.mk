###############################################################################
# GPS HAL libraries
LOCAL_PATH := $(call my-dir)

PRODUCT_COPY_FILES += \
			hardware/aw/gps/TD1030HAL/AGNSS/libtdcrypto.so:system/lib/libtdcrypto.so \
			hardware/aw/gps/TD1030HAL/AGNSS/libtdssl.so:system/lib/libtdssl.so \
			hardware/aw/gps/TD1030HAL/AGNSS/libtdsupl.so:system/lib/libtdsupl.so \
			hardware/aw/gps/TD1030HAL/AGNSS/supl-client:system/bin/supl-client \
			hardware/aw/gps/TD1030HAL/AGNSS/tdgnss.conf:system/etc/tdgnss.conf


