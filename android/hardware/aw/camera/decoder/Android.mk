LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := libcamera_decorder

LOCAL_MODULE_TAGS := eng option

LOCAL_SRC_FILES := \
    Libve_Decoder2.c \
	

LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libui \
	libutils \

LOCAL_C_INCLUDES += \
	frameworks/av/media/libcedarc/include

LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libnativehelper \
	libutils \
	libMemAdapter \
	libvencoder \
	libvdecoder \
	libvideoengine \
	libui \
	libdl

include $(BUILD_SHARED_LIBRARY)

