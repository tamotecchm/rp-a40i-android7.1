/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define LOG_TAG "audio_3g_call"
#define LOG_NDEBUG 0

#include <errno.h>
#include <pthread.h>
#include <stdint.h>
#include <sys/time.h>
#include <stdlib.h>
#include <system/thread_defs.h>

#include <cutils/log.h>
#include <hardware/hardware.h>
#include <system/audio.h>
#include <hardware/audio.h>

#include <tinyalsa/asoundlib.h>
#include <audio_utils/resampler.h>
#include <fcntl.h>

#include "audio_hw.h"
#include "audio_3g_call.h"

struct pcm_config pcm_config_g3_call = {
    .channels = 2,
    .rate = DEFAULT_SAMPLE_RATE,
    .period_size = 512,
    .period_count = 2,
    .format = PCM_FORMAT_S16_LE,
};

static int find_id_by_name(char * name_linux)
{
    int index;
    int ret = -1;
    int fd = 0;
    char * snd_path = "/sys/class/sound";
    char snd_card[128], snd_node[128];
    char snd_id[32];
    
    memset(snd_card, 0, sizeof(snd_card));
    memset(snd_node, 0, sizeof(snd_node));
    memset(snd_id, 0, sizeof(snd_id));
    
    for (index = 0; index < 16; index++)
    {
        sprintf(snd_card, "%s/card%d", snd_path, index);
        ret = access(snd_card, F_OK);
        if(ret == 0)
        {
            sprintf(snd_node, "%s/card%d/id", snd_path, index);
            ALOGD("read card %s/card%d/id",snd_path, index);

            fd = open(snd_node, O_RDONLY);
            if (fd > 0) 
            {
                ret = read(fd, snd_id, sizeof(snd_id));
                if (ret > 0) 
                {
                    snd_id[ret - 1] = 0;
                    ALOGD("%s, %s, len: %d", snd_node, snd_id, ret);
                }
                close(fd);
            } 
            else 
            {
                return -1;
            }
            if(!strcmp(snd_id, name_linux))	
            {
                return index;
            }
        }
        else
        {
            return -1;
        }
    }
    
    if(index >= 16)
    {
        ALOGE("device %s is not exist!", name_linux);
        return -1;
    }
	
    return -1;
}


int g3call_init(struct sunxi_audio_device *adev)
{
    struct sunxi_g3call_data *g3_data;
    g3_data = calloc(1, sizeof (struct sunxi_g3call_data));
    if (!g3_data) {
        ALOGE("failed to allocate 3g_data memory");
        return -ENOMEM;
    }
    memset(g3_data, 0x00, sizeof(struct sunxi_g3call_data));
    adev->g3_data= g3_data;

    // init mutex and condition
    if (pthread_mutex_init(&g3_data->lock, NULL)) {
        ALOGE("failed to create mutex lock (%d): %m", errno);
        return -EINVAL;
    }

    if (pthread_cond_init(&g3_data->cond, NULL)) {
        ALOGE("failed to create cond(%d): %m", errno);
        return -EINVAL;
    }

    if (!g3_data->thread1) { // define: private data: g3_data
        //pthread_attr_t attr;
        //struct sched_param sched = {0};
        //sched.sched_priority = ANDROID_PRIORITY_AUDIO;
        //pthread_attr_init(&attr);
       // pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
       // pthread_attr_setschedpolicy(&attr, SCHED_OTHER);
        //pthread_attr_setschedparam(&attr, &sched);
        if (pthread_create(&g3_data->thread1, NULL, g3call_thread1, adev)) {
            ALOGE("%s() pthread_create 3ginput to codec output thread failed!!!", __func__);
         //   pthread_attr_destroy(&attr);
            return -EINVAL;
        }
        ALOGD("###thread1 done.");
        //pthread_attr_destroy(&attr);
    }
    return 0;
}

int g3call_init2(struct sunxi_audio_device *adev)
{
    struct sunxi_g3call_data *g3_data;
    g3_data = adev->g3_data;
    if (!g3_data) {
        ALOGE("failed to allocate g3_data memory");
        return -ENOMEM;
    }

    // init mutex and condition
    if(pthread_mutex_init(&g3_data->lock2, NULL)) {
        ALOGE("failed to create mutex lock2 (%d): %m", errno);
        return -EINVAL;
    }

    if(pthread_cond_init(&g3_data->cond2, NULL)) {
        ALOGE("failed to create cond out_cond (%d): %m", errno);
        return -EINVAL;
    }

if (!g3_data->thread2) { // define: private data: g3_data
        //pthread_attr_t attr;
        //struct sched_param sched = {0};
        //sched.sched_priority = ANDROID_PRIORITY_AUDIO;
        //pthread_attr_init(&attr);
        //pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
        //pthread_attr_setschedpolicy(&attr, SCHED_OTHER);
        //pthread_attr_setschedparam(&attr, &sched);
        if(pthread_create(&g3_data->thread2, NULL, g3call_thread2, adev)){
            ALOGE("%s() pthread_create codec input to 3g output thread failed!!!", __func__);
           // pthread_attr_destroy(&attr);
            return -EINVAL;
        }
        ALOGD("###thread2 done.");
        //pthread_attr_destroy(&attr);
    }
    return 0;
}

static int open_g3_inpcm(struct sunxi_audio_device *adev)
{
    int card = 1;
    int dev = 0;

    struct sunxi_g3call_data *g3_data = adev->g3_data;
    struct pcm_config config_in = pcm_config_g3_call;

    config_in.period_size = 512;
    config_in.period_count = 2;
    config_in.rate = CODEC_CATPURE_SAMPLE_RATE;
    config_in.channels = 1;
    //getCardNumbyName(adev,"AUDIO_BT",&card);
    if (card == -1)
        card = 1;

    if (!g3_data->g3_inpcm) {
        g3_data->g3_inpcm = pcm_open(card, dev, PCM_IN, &config_in);

        if (!pcm_is_ready(g3_data->g3_inpcm)) {
            ALOGE("cannot open pcm_in driver: %s", pcm_get_error(g3_data->g3_inpcm));
            g3_data->g3_inpcm = NULL;
            return -ENOMEM;
        }
        ALOGV("%s() ", __func__);
    }
    return 0;
}

static void close_g3_inpcm(struct sunxi_audio_device *adev)
{
    struct sunxi_g3call_data *g3_data = adev->g3_data;

    if (!g3_data)
        return;

    if (g3_data->g3_inpcm) {
       pcm_close(g3_data->g3_inpcm);
       g3_data->g3_inpcm = NULL;
       ALOGW("%s() ", __func__);
    } else {
        ALOGW("%s() g3_data->g3_inpcm == NULL", __func__);
    }
}


static int open_codec_outpcm(struct sunxi_audio_device *adev)
{
    int card = 0;
    int dev = 0;
    struct sunxi_g3call_data *g3_data = adev->g3_data;
    struct pcm_config config_out = pcm_config_g3_call;

    config_out.period_size = 1024;
    config_out.period_count = 4;
    config_out.rate = CODEC_CATPURE_SAMPLE_RATE;
    config_out.channels = 1;

    //getCardNumbyName(adev, "AUDIO_CODEC", &card);
    if (card == -1)
        card = 0;

    if (!g3_data->codec_outpcm) {
        g3_data->codec_outpcm = pcm_open(card, dev, PCM_OUT, &config_out);

        if (!pcm_is_ready(g3_data->codec_outpcm )) {
            ALOGE("cannot open output_pcm driver: %s", pcm_get_error(g3_data->codec_outpcm));
            g3_data->codec_outpcm = NULL;
            return -ENOMEM;
        }
        ALOGW("%s() ", __func__);
    }
    return 0;
}

static void close_codec_outpcm(struct sunxi_audio_device *adev)
{
    struct sunxi_g3call_data *g3_data = adev->g3_data;

    if (!g3_data)
        return;

    if (g3_data->codec_outpcm) {
        pcm_close(g3_data->codec_outpcm);
        g3_data->codec_outpcm = NULL;
        ALOGW("%s() ", __func__);
    } else {
        ALOGW("%s() g3_data->codec_outpcm == NULL", __func__);
    }
}

static int open_codec_inpcm(struct sunxi_audio_device *adev)
{
    int card = 0;
    int dev = 0;

    struct sunxi_g3call_data *g3_data = adev->g3_data;
    struct pcm_config config_in = pcm_config_g3_call;

    config_in.period_size = 512;
    config_in.period_count = 2;
    config_in.rate = CODEC_CATPURE_SAMPLE_RATE;
    config_in.channels = 1;
    //getCardNumbyName(adev,"AUDIO_BT",&card);
    if (card == -1)
        card = 0;

    if (!g3_data->codec_inpcm) {
        g3_data->codec_inpcm = pcm_open(card, dev, PCM_IN, &config_in);

        if (!pcm_is_ready(g3_data->codec_inpcm)) {
            ALOGE("cannot open pcm_in driver: %s", pcm_get_error(g3_data->codec_inpcm));
            g3_data->codec_inpcm = NULL;
            return -ENOMEM;
        }
        ALOGV("%s() ", __func__);
    }
    return 0;
}

static void close_codec_inpcm(struct sunxi_audio_device *adev)
{
    struct sunxi_g3call_data *g3_data = adev->g3_data;

    if (!g3_data)
        return;

    if (g3_data->codec_inpcm) {
       pcm_close(g3_data->codec_inpcm);
       g3_data->codec_inpcm = NULL;
       ALOGW("%s() ", __func__);
    } else {
        ALOGW("%s() g3_data->codec_inpcm == NULL", __func__);
    }
}


static int open_g3_outpcm(struct sunxi_audio_device *adev)
{
    int card = 1;
    int dev = 0;
    struct sunxi_g3call_data *g3_data = adev->g3_data;
    struct pcm_config config_out = pcm_config_g3_call;

    config_out.period_size = 1024;
    config_out.period_count = 4;
    config_out.rate = CODEC_CATPURE_SAMPLE_RATE;
    config_out.channels = 1;

    //getCardNumbyName(adev, "AUDIO_CODEC", &card);
    if (card == -1)
        card = 1;

    if (!g3_data->g3_outpcm) {
        g3_data->g3_outpcm = pcm_open(card, dev, PCM_OUT, &config_out);

        if (!pcm_is_ready(g3_data->g3_outpcm )) {
            ALOGE("cannot open 3g outpcm driver: %s", pcm_get_error(g3_data->g3_outpcm));
            g3_data->g3_outpcm = NULL;
            return -ENOMEM;
        }
        ALOGW("####%s() ", __func__);
    }
    return 0;
}

static void close_g3_outpcm(struct sunxi_audio_device *adev)
{
    struct sunxi_g3call_data *g3_data = adev->g3_data;

    if (!g3_data)
        return;

    if (g3_data->g3_outpcm) {
        pcm_close(g3_data->g3_outpcm);
        g3_data->g3_outpcm = NULL;
        ALOGW("####%s() ", __func__);
    } else {
        ALOGW("%s() g3_data->codec_outpcm == NULL", __func__);
    }
}
int g3call_open(struct sunxi_audio_device *adev)
{
	int err = 0;
	int size = 0;
	int ret = 0;
	struct sunxi_g3call_data *g3_data = adev->g3_data;

	ALOGD("++++++++++++g3call_open++++++++++++++");
	if (!g3_data->g3_inpcm) {
		ret = open_g3_inpcm(adev);
		if (ret != 0) {
			ALOGE("open 3g inpcm pcm failed.");
		}
		err += ret;
	}

	if (!g3_data->codec_outpcm) {
		ret = open_codec_outpcm(adev);
		if (ret != 0) {
			ALOGE("open output pcm failed.");
		}
		err += ret;
	}

	if (!g3_data->codec_inpcm) {
            ret = open_codec_inpcm(adev);
            if(ret != 0){
                ALOGE("open codec_inpcm pcm failed.");
            }
	    err += ret;
        }

        if (!g3_data->g3_outpcm) {
            ret = open_g3_outpcm(adev);
            if (ret != 0) {
                ALOGE("open 3g output pcm failed.");
            }
	   err += ret;
        }

	if (!g3_data->buffer_rx) {
            size = pcm_get_buffer_size(g3_data->g3_inpcm);
            g3_data->buffer_rx = malloc(size);
            if(!g3_data->buffer_rx){
                ALOGE("malloc failed, out of memory.");
		err += 1;
            }
            g3_data->buffer_rxsize = size;
        }
	if (!g3_data->buffer_tx) {
            size = pcm_get_buffer_size(g3_data->codec_inpcm);
            g3_data->buffer_tx = malloc(size);
            if(!g3_data->buffer_tx){
                ALOGE("malloc failed, out of memory.");
		err += 1;
            }
            g3_data->buffer_txsize = size;
        }
	return err;
}
int g3call_close(struct sunxi_audio_device *adev){
	struct sunxi_g3call_data *g3_data = adev->g3_data;
	ALOGD("++++++++++++g3call_close++++++++++++++");
	if(g3_data->g3_inpcm)
		close_g3_inpcm(adev);

	if(g3_data->codec_outpcm)
		close_codec_outpcm(adev);

	if (g3_data->codec_inpcm)
		close_codec_inpcm(adev);

	if (g3_data->g3_outpcm)
		close_g3_outpcm(adev);

	if (g3_data->buffer_rx) {
		free(g3_data->buffer_rx);
		g3_data->buffer_rx = NULL;
		g3_data->buffer_rxsize = 0;
	}
	if (g3_data->buffer_tx) {
		free(g3_data->buffer_tx);
		g3_data->buffer_tx = NULL;
		g3_data->buffer_txsize = 0;
	}
	return 0;
}
// for 3g call read from 3g in pcm, send data to codec output
void* g3call_thread1(void *data)
{
    ALOGV("%s enter", __func__);
    int ret = 0;
    int size = 0;

    struct sunxi_audio_device *adev = (struct sunxi_audio_device *)data;
    struct sunxi_g3call_data *g3_data = adev->g3_data;
    while(1) {
        pthread_mutex_lock(&g3_data->lock);
        if (!g3_data->start_work && !g3_data->exit_work) {
	     ALOGV("%s wait in line = %d\n",__func__,__LINE__);
            pthread_cond_wait(&g3_data->cond, &g3_data->lock);
            ALOGV("%s start.",__func__);
        }
        if (g3_data->exit_work) {
            ALOGD("###3gcall_thread1 exit");
            pthread_mutex_unlock(&g3_data->lock);
            break;
        }
        pthread_mutex_unlock(&g3_data->lock);
       
	if(g3_data->g3_inpcm && g3_data->codec_outpcm && (g3_data->buffer_rx != NULL))
	{
		size = g3_data->buffer_rxsize;
		ret = pcm_read(g3_data->g3_inpcm, g3_data->buffer_rx, size);
		if (ret)
		    ALOGE("PCM in, pcm_read failed, return %s\n", pcm_get_error(g3_data->g3_inpcm));

		ret = pcm_write(g3_data->codec_outpcm,g3_data->buffer_rx, size);
		if (ret)
		    ALOGE("codec out, pcm_write failed, return %s\n", pcm_get_error(g3_data->codec_outpcm));
	}
    }

    g3_data->exit_work = 1;
    g3_data->start_work = 0;
    return NULL;
}


// for 3g call read from codec mic ,send data to 3g pcm
void* g3call_thread2(void *data)
{
    ALOGV("%s enter", __func__);
    int ret = 0;
    int size = 0;

    struct sunxi_audio_device *adev = (struct sunxi_audio_device *)data;
    struct sunxi_g3call_data *g3_data = adev->g3_data;
    while(1) {
        pthread_mutex_lock(&g3_data->lock2);
        if (!g3_data->start_work2 && !g3_data->exit_work2) {
            ALOGV("%s wait in line = %d\n",__func__,__LINE__);

            pthread_cond_wait(&g3_data->cond2, &g3_data->lock2);
            ALOGV("%s start.",__func__);

        }
        if (g3_data->exit_work2) {
            ALOGD("%s exit",__func__);
            pthread_mutex_unlock(&g3_data->lock2);
            break;
        }
        pthread_mutex_unlock(&g3_data->lock2);
	if(g3_data->codec_inpcm && g3_data->g3_outpcm && (g3_data->buffer_tx != NULL))
	{
		size = g3_data->buffer_txsize;
		ret = pcm_read(g3_data->codec_inpcm, g3_data->buffer_tx, size);
		if (ret)
		    ALOGE("codec in, pcm_read failed, return %s\n", pcm_get_error(g3_data->codec_inpcm));
			
		ret = 0;
		ret = pcm_write(g3_data->g3_outpcm, g3_data->buffer_tx, size);
		if (ret)
		    ALOGE("PCM out, pcm_write failed, return %s\n", pcm_get_error(g3_data->g3_outpcm));
	}
    }

    g3_data->exit_work2 = 1;
    g3_data->start_work2 = 0;
    return NULL;
}
