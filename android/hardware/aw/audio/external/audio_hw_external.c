/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "audio_hw_external"
#define LOG_NDEBUG 0

#include <errno.h>
#include <pthread.h>
#include <stdint.h>
#include <sys/time.h>
#include <stdlib.h>

#include <cutils/log.h>
#include <cutils/str_parms.h>
#include <cutils/properties.h>

#include <hardware/hardware.h>
#include <system/audio.h>
#include <hardware/audio.h>

#include <tinyalsa/asoundlib.h>
#include <audio_utils/resampler.h>
#include <audio_utils/echo_reference.h>
#include <hardware/audio_effect.h>
#include <audio_effects/effect_aec.h>
#include <fcntl.h>
#include <noise_reduction.h>

#include <cutils/properties.h> // for property_get

#include "audio_hw_external.h"


enum tty_modes {
    TTY_MODE_OFF,
    TTY_MODE_VCO,
    TTY_MODE_HCO,
    TTY_MODE_FULL
};

struct pcm_config pcm_config_mm_out = {
    .channels = 2,
    .rate = MM_SAMPLING_RATE,
    .period_size = SHORT_PERIOD_SIZE,
    .period_count = PLAYBACK_PERIOD_COUNT,
    .format = PCM_FORMAT_S16_LE,
};

struct pcm_config pcm_config_mm_in = {
    .channels = 2,
    .rate = MM_SAMPLING_RATE,
    .period_size = 1024,
    .period_count = CAPTURE_PERIOD_COUNT,
    .format = PCM_FORMAT_S16_LE,
};

struct route_setting {
    char *ctl_name;
    int intval;
    char *strval;
};

typedef enum e_AUDIO_DEVICE_MANAGEMENT
{
    AUDIO_IN        = 0x01,
    AUDIO_OUT       = 0x02,
}e_AUDIO_DEVICE_MANAGEMENT;

/**
 * NOTE: when multiple mutexes have to be acquired, always respect the following order:
 *        hw device > in stream > out stream
 */
static int adev_set_voice_volume(struct audio_hw_device *dev, float volume);
static int do_input_standby(struct sunxi_stream_in *in);
static int do_output_standby(struct sunxi_stream_out *out);

/** audio_stream_in implementation **/
static int get_next_buffer(struct resampler_buffer_provider *buffer_provider,
                                   struct resampler_buffer* buffer);
static void release_buffer(struct resampler_buffer_provider *buffer_provider,
                                  struct resampler_buffer* buffer);


/*wifi display buffer manager*/
static int WritePcmData(void * pInbuf, int inSize, struct pcm_buf_manager *PcmManager)
{
    ALOGV("RequestWriteBuf ++: size: %d", inSize);

    if (PcmManager->BufValideLen< inSize)
    {
        ALOGE("not enough buffer to write");
        return -1;
    }

    pthread_mutex_lock(&PcmManager->lock);

    if ((PcmManager->BufWritPtr + inSize) > (PcmManager->BufStart + PcmManager->BufTotalLen)) {
        int endSize = PcmManager->BufStart + PcmManager->BufTotalLen
            - PcmManager->BufWritPtr;
        memcpy(PcmManager->BufWritPtr, pInbuf, endSize);
        memcpy(PcmManager->BufStart, (void *)((char *)pInbuf + endSize), inSize - endSize);

        PcmManager->BufWritPtr = PcmManager->BufWritPtr
        + inSize - PcmManager->BufTotalLen;
    } else {
        memcpy(PcmManager->BufWritPtr, pInbuf, inSize);
        PcmManager->BufWritPtr += inSize;
    }

    PcmManager->BufValideLen -= inSize;
    PcmManager->DataLen += inSize;

    ALOGV("after wr: BufTotalLen: %d, DataLen: %d, BufValideLen: %d, BufReadPtr: %p, BufWritPtr: %p",
        PcmManager->BufTotalLen, PcmManager->DataLen, PcmManager->BufValideLen,
        PcmManager->BufReadPtr, PcmManager->BufWritPtr);

    pthread_mutex_unlock(&PcmManager->lock);
    ALOGV("RequestWriteBuf --");
    return 0;
}

static int ReadPcmData(void *pBuf, int uGetLen, struct pcm_buf_manager *PcmManager)
{
    int underflow = 0, fill_dc_size;
    int size_read = uGetLen;
    int timeout = 0, max_wait_count;

    struct sunxi_audio_device *adev = PcmManager->dev;

    max_wait_count = uGetLen * 100 / (PcmManager->SampleRate*PcmManager->Channel * 2) + 1; //normal
    max_wait_count *= 2;//twice

    ALOGV("ReadPcmDataForEnc ++, getLen: %d max_wait_count=%d", uGetLen,max_wait_count);
    if (adev->active_output != NULL) {
        while(PcmManager->DataLen < uGetLen) {
            ALOGV("pcm is not enough for audio encoder! uGetLen: %d, uDataLen: %d\n",
                uGetLen, PcmManager->DataLen);
            usleep(10 * 1000);
            timeout++;
            if (timeout > max_wait_count) {
                if (PcmManager->DataLen < uGetLen) {
                    underflow = 1;
                    size_read = PcmManager->DataLen;
                    fill_dc_size = uGetLen - PcmManager->DataLen;
                    ALOGV("fill with dc size:%d",uGetLen - PcmManager->DataLen);
                }
            break;
            }
        }
    } else {
        ALOGV("pcm is not enough for audio encoder! uGetLen: %d, uDataLen: %d\n",
            uGetLen, PcmManager->DataLen);
        if (PcmManager->DataLen < uGetLen) {
            underflow = 1;
            size_read = PcmManager->DataLen;
            fill_dc_size = uGetLen - PcmManager->DataLen;
            usleep(fill_dc_size * 1000000 / 4 / PcmManager->SampleRate);
            ALOGV("fill with dc size:%d",uGetLen - PcmManager->DataLen);
        }
    }

    if ((PcmManager->BufReadPtr + size_read) > (PcmManager->BufStart + PcmManager->BufTotalLen)) {
        int len1 = PcmManager->BufStart
            + PcmManager->BufTotalLen - PcmManager->BufReadPtr;
        memcpy((void *)pBuf, (void *)PcmManager->BufReadPtr, len1);
        memcpy((void *)((char *)pBuf + len1), (void *)PcmManager->BufStart, size_read - len1);
    } else {
        memcpy(pBuf, PcmManager->BufReadPtr, size_read);
    }

    pthread_mutex_lock(&PcmManager->lock);

    PcmManager->BufReadPtr += size_read;

    if (PcmManager->BufReadPtr >= PcmManager->BufStart + PcmManager->BufTotalLen) {
        PcmManager->BufReadPtr -= PcmManager->BufTotalLen;
    }

    PcmManager->DataLen -= size_read;
    PcmManager->BufValideLen += size_read;

    ALOGV("after rd: BufTotalLen: %d, DataLen: %d, BufValideLen: %d, pBufReadPtr: %p, pBufWritPtr: %p",
        PcmManager->BufTotalLen, PcmManager->DataLen, PcmManager->BufValideLen,
        PcmManager->BufReadPtr, PcmManager->BufWritPtr);

    pthread_mutex_unlock(&PcmManager->lock);
    ALOGV("ReadPcmDataForEnc --");

    if (underflow) {
        char *ptr = (char*)pBuf;
        memset(ptr+size_read, ptr[size_read-1], fill_dc_size);
    }

    return uGetLen;
}

static int init_audio_device_i2s(struct sunxi_audio_device *adev)
{
    int card = -1;
    int ret = -1;
    int fd = 0;
    char * snd_path = "/sys/class/sound";
    char snd_card[128], snd_node[128];
    char snd_id[32];
    
    for (card = 0; card < 16; card++)
    {
        memset(snd_card, 0, sizeof(snd_card));
        memset(snd_node, 0, sizeof(snd_node));
        memset(snd_id, 0, sizeof(snd_id));
     	sprintf(snd_card, "%s/card%d", snd_path, card);
     	ret = access(snd_card, F_OK);
     	if(ret == 0)
     	{
     		sprintf(snd_node, "%s/card%d/id", snd_path, card);
     		ALOGD("read card %s/card%d/id",snd_path, card);
     
     		fd = open(snd_node, O_RDONLY);
     		if (fd > 0) 
     		{
     			ret = read(fd, snd_id, sizeof(snd_id));
     			if (ret > 0) 
     			{
     				snd_id[ret - 1] = 0;
     				ALOGD("%s, %s, len: %d", snd_node, snd_id, ret);
     			}
     			close(fd);
     		} 
     		else 
     		{
     			return -1;
     		}
     		if(!strcmp(snd_id, "snddaudio0")) 
     		{
     		    break;
     		}
     	}
     }

    if(card >= 16)
     {
     	ALOGE("device AUDIO_I2S is not exist!");
     	return -1;
     }

        adev->dev_manager.card = card;
        adev->dev_manager.device = 0;
        adev->dev_manager.flag_exist = true;

        // playback device
        sprintf(snd_node, "%s/card%d/pcmC%dD0p", snd_path, card, card);
        ret = access(snd_node, F_OK);
        if(ret == 0) {
            // there is a playback device
            adev->dev_manager.flag_out = AUDIO_OUT;
            adev->dev_manager.flag_out_active = 1;
        }

        // capture device
        sprintf(snd_node, "%s/card%d/pcmC%dD0c", snd_path, card, card);
        ret = access(snd_node, F_OK);
        if(ret == 0) {
            // there is a capture device
            adev->dev_manager.flag_in = AUDIO_IN;
            adev->dev_manager.flag_in_active = 1;
        }
     
     return card;
}

static int get_output_device_params(unsigned int card,struct sunxi_stream_out *out)
{
    unsigned int min, max;
    struct pcm_params *params = pcm_params_get(card, 0, PCM_OUT);

    if (params == NULL) {
        ALOGD("LINE: %d, FUNC: %s ,Device does not exist.\n",__LINE__,__FUNCTION__);
        pcm_params_free(params);
        return -1;
    }

    min = pcm_params_get_min(params, PCM_PARAM_RATE);
    max = pcm_params_get_max(params, PCM_PARAM_RATE);

    if ((min==44100)||(max==44100)) {
        out->multi_config[card].rate = 44100;
    } else if ((min==48000)||(max==48000)) {
        out->multi_config[card].rate = 48000;
    } else if ((min==32000)||(max==32000)) {
        out->multi_config[card].rate = 32000;
    } else if ((min==24000)||(max==24000)) {
        out->multi_config[card].rate = 24000;
    } else if ((min==22050)||(max==22050)) {
        out->multi_config[card].rate = 22050;
    } else if ((min==16000)||(max==16000)) {
        out->multi_config[card].rate = 16000;
    } else if ((min==12000)||(max==12000)) {
        out->multi_config[card].rate = 12000;
    } else if ((min==11025)||(max==11025)) {
        out->multi_config[card].rate = 11025;
    } else if ((min==8000)||(max==8000)) {
        out->multi_config[card].rate = 8000;
    }

    min = pcm_params_get_min(params, PCM_PARAM_CHANNELS);
    max = pcm_params_get_max(params, PCM_PARAM_CHANNELS);

    if ((min==2)||(max==2)) {
        out->multi_config[card].channels= 2;
    } else {
        out->multi_config[card].channels = 1;
    }

    ALOGD("LINE: %d, FUNC: %s , card = %d , rate = %d , channels = %d",__LINE__,__FUNCTION__,card,out->multi_config[card].rate,out->multi_config[card].channels);
    pcm_params_free(params);
    return 0;
}

static void force_all_standby(struct sunxi_audio_device *adev)
{
    struct sunxi_stream_in *in;
    struct sunxi_stream_out *out;

    if (adev->active_output) {
        out = adev->active_output;
        pthread_mutex_lock(&out->lock);
        do_output_standby(out);
        pthread_mutex_unlock(&out->lock);
    }
    if (adev->active_input) {
        in = adev->active_input;
        pthread_mutex_lock(&in->lock);
        do_input_standby(in);
        pthread_mutex_unlock(&in->lock);
    }
}

/* must be called with hw device and output stream mutexes locked */
static int start_output_stream(struct sunxi_stream_out *out)
{
    int ret = 0;
    struct sunxi_audio_device *adev = out->dev;
    int card = adev->card_id;
    unsigned int port = PORT_I2S;
    struct pcm_params *params;
    char prop_value[512];

    if (adev->mode == AUDIO_MODE_IN_CALL || adev->mode == AUDIO_MODE_MODE_FACTORY_TEST || adev->mode == AUDIO_MODE_FM) {
        ALOGW("mode in call, do not start stream");
        return 0;
    }

    adev->active_output = out;

    if (adev->mode != AUDIO_MODE_IN_CALL) {
        F_LOG;
    }
    /* S/PDIF takes priority over HDMI audio. In the case of multiple
     * devices, this will cause use of S/PDIF or HDMI only */
    out->config.rate = MM_SAMPLING_RATE;
    /* default to low power: will be corrected in out_write if necessary before first write to
     * tinyalsa.
     */
    out->write_threshold = PLAYBACK_PERIOD_COUNT * SHORT_PERIOD_SIZE;
    out->config.start_threshold = SHORT_PERIOD_SIZE * 2;
    out->config.avail_min = SHORT_PERIOD_SIZE;

    out->write_threshold = 0;// SHORT_PERIOD_SIZE * PLAYBACK_PERIOD_COUNT;

    if(card >= 0) {
        if (adev->dev_manager.flag_exist
            && (adev->dev_manager.flag_out == AUDIO_OUT)
            && adev->dev_manager.flag_out_active
            &&(!strcmp(adev->dev_manager.name, AUDIO_NAME_I2S))) {      //only support i2s
            ALOGV("use %s to playback audio", adev->dev_manager.name);
            out->multi_config[card] = pcm_config_mm_out;
            out->multi_config[card].rate = MM_SAMPLING_RATE;
            out->multi_config[card].start_threshold = SHORT_PERIOD_SIZE * 2;
            out->multi_config[card].avail_min = LONG_PERIOD_SIZE;

            out->multi_pcm[card] = pcm_open(card, port, PCM_OUT, &out->multi_config[card]);
            if (!pcm_is_ready(out->multi_pcm[card])) {
                ALOGE("cannot open pcm driver: %s", pcm_get_error(out->multi_pcm[card]));
                pcm_close(out->multi_pcm[card]);
                out->multi_pcm[card] = NULL;
                adev->active_output = NULL;
                return -ENOMEM;
            }

            if (adev->echo_reference != NULL)
                out->echo_reference = adev->echo_reference;

            if (DEFAULT_I2S_OUT_SAMPLING_RATE != out->multi_config[card].rate) {
                ret = create_resampler(DEFAULT_I2S_OUT_SAMPLING_RATE,
                                        out->multi_config[card].rate,
                                        2,
                                        RESAMPLER_QUALITY_DEFAULT,
                                        NULL,
                                        &out->multi_resampler[card]);
                if (ret != 0) {
                    ALOGE("create out resampler failed, %d -> %d", DEFAULT_I2S_OUT_SAMPLING_RATE, out->multi_config[card].rate);
                    return ret;
                }

                ALOGV("create out resampler OK, %d -> %d", DEFAULT_I2S_OUT_SAMPLING_RATE, out->multi_config[card].rate);
            } else
                ALOGV("do not use out resampler");

            if (out->multi_resampler[card]) {
                out->multi_resampler[card]->reset(out->multi_resampler[card]);
            }
        }
    }
    // match end

    return 0;
}

static int check_input_parameters(uint32_t sample_rate, int format, int channel_count)
{
    if (format != AUDIO_FORMAT_PCM_16_BIT)
        return -EINVAL;

    if ((channel_count < 1) || (channel_count > 2))
        return -EINVAL;

    switch(sample_rate) {
    case 8000:
    case 11025:
    case 16000:
    case 22050:
    case 24000:
    case 32000:
    case 44100:
    case 48000:
        break;
    default:
        return -EINVAL;
    }

    return 0;
}

static size_t get_input_buffer_size(uint32_t sample_rate, int format, int channel_count)
{
    size_t size;
    size_t device_rate;

    if (check_input_parameters(sample_rate, format, channel_count) != 0)
        return 0;

    /* take resampling into account and return the closest majoring
    multiple of 16 frames, as audioflinger expects audio buffers to
    be a multiple of 16 frames */
    size = (pcm_config_mm_in.period_size * sample_rate) / pcm_config_mm_in.rate;
    size = ((size + 15) / 16) * 16;

    return size * channel_count * sizeof(short);
}

static void add_echo_reference(struct sunxi_stream_out *out,
                               struct echo_reference_itfe *reference)
{
    pthread_mutex_lock(&out->lock);
    out->echo_reference = reference;
    pthread_mutex_unlock(&out->lock);
}

static void remove_echo_reference(struct sunxi_stream_out *out,
                                  struct echo_reference_itfe *reference)
{
    pthread_mutex_lock(&out->lock);
    if (out->echo_reference == reference) {
        /* stop writing to echo reference */
        reference->write(reference, NULL);
        out->echo_reference = NULL;
    }
    pthread_mutex_unlock(&out->lock);
}

static void put_echo_reference(struct sunxi_audio_device *adev,
                          struct echo_reference_itfe *reference)
{
    if (adev->echo_reference != NULL &&
            reference == adev->echo_reference) {
        if (adev->active_output != NULL)
            remove_echo_reference(adev->active_output, reference);
        release_echo_reference(reference);
        adev->echo_reference = NULL;
    }
}

static struct echo_reference_itfe *get_echo_reference(struct sunxi_audio_device *adev,
                                               uint32_t channel_count,
                                               uint32_t sampling_rate)
{
    put_echo_reference(adev, adev->echo_reference);
    if (adev->active_output != NULL) {
        struct audio_stream *stream = &adev->active_output->stream.common;
        uint32_t wr_channel_count = popcount(stream->get_channels(stream));
        uint32_t wr_sampling_rate = stream->get_sample_rate(stream);

        int status = create_echo_reference(AUDIO_FORMAT_PCM_16_BIT,
                                           channel_count,
                                           sampling_rate,
                                           AUDIO_FORMAT_PCM_16_BIT,
                                           wr_channel_count,
                                           wr_sampling_rate,
                                           &adev->echo_reference);
        if (status == 0)
            add_echo_reference(adev->active_output, adev->echo_reference);
    }
    return adev->echo_reference;
}

static int get_playback_delay(struct sunxi_stream_out *out,
                       size_t frames,
                       struct echo_reference_buffer *buffer)
{
    struct sunxi_audio_device *adev = out->dev;
    size_t kernel_frames;
    int status;
    int card = adev->card_id;

        if (card >= 0 && adev->dev_manager.flag_exist
                && (adev->dev_manager.flag_out == AUDIO_OUT)
                && adev->dev_manager.flag_out_active
                && (!strcmp(adev->dev_manager.name, AUDIO_NAME_I2S))) {

            status = pcm_get_htimestamp(out->multi_pcm[card], &kernel_frames, &buffer->time_stamp);
            if (status < 0) {
                buffer->time_stamp.tv_sec  = 0;
                buffer->time_stamp.tv_nsec = 0;
                buffer->delay_ns           = 0;
                ALOGV("get_playback_delay(): pcm_get_htimestamp error,"
                        "setting playbackTimestamp to 0");
                return status;
            }

            kernel_frames = pcm_get_buffer_size(out->multi_pcm[card]) - kernel_frames;
        }


    /* adjust render time stamp with delay added by current driver buffer.
     * Add the duration of current frame as we want the render time of the last
     * sample being written. */
    buffer->delay_ns = (long)(((int64_t)(kernel_frames + frames)* 1000000000)/
                            MM_SAMPLING_RATE);

    return 0;
}

static uint32_t out_get_sample_rate(const struct audio_stream *stream)
{
    struct sunxi_stream_out *out = (struct sunxi_stream_out *)stream;
    return DEFAULT_I2S_OUT_SAMPLING_RATE;
}

static int out_set_sample_rate(struct audio_stream *stream, uint32_t rate)
{
    struct sunxi_stream_out *out = (struct sunxi_stream_out *)stream;
    ALOGV("fun = %s, rate = %d", __FUNCTION__, rate);
    return 0;
}

static size_t out_get_buffer_size(const struct audio_stream *stream)
{
    struct sunxi_stream_out *out = (struct sunxi_stream_out *)stream;

    /* take resampling into account and return the closest majoring
    multiple of 16 frames, as audioflinger expects audio buffers to
    be a multiple of 16 frames */
    size_t size = (SHORT_PERIOD_SIZE * DEFAULT_I2S_OUT_SAMPLING_RATE) / out->config.rate;
    size = ((size + 15) / 16) * 16;
    return size * audio_stream_out_frame_size((struct audio_stream_out *)stream);
}

static audio_channel_mask_t out_get_channels(const struct audio_stream *stream)
{
    struct sunxi_stream_out *out = (struct sunxi_stream_out *)stream;
    return AUDIO_CHANNEL_OUT_STEREO;
}

static audio_format_t out_get_format(const struct audio_stream *stream)
{
    struct sunxi_stream_out *out = (struct sunxi_stream_out *)stream;
    return AUDIO_FORMAT_PCM_16_BIT;
}

static int out_set_format(struct audio_stream *stream, audio_format_t format)
{
    struct sunxi_stream_out *out = (struct sunxi_stream_out *)stream;
    ALOGV("fun = %s, format = %d", __FUNCTION__, format);
    return 0;
}

/* must be called with hw device and output stream mutexes locked */
static int do_output_standby(struct sunxi_stream_out *out)
{
    struct sunxi_audio_device *adev = out->dev;
    int card = adev->card_id;
    if (!out->standby) {
        if (out->pcm) {
            pcm_close(out->pcm);
            out->pcm = NULL;
        }

        if (out->resampler) {
            release_resampler(out->resampler);
            out->resampler = NULL;
        }

       if(card >= 0) {
            if (out->multi_pcm[card]) {
                pcm_close(out->multi_pcm[card]);
                out->multi_pcm[card] = NULL;
            }

            if (out->multi_resampler[card]) {
                release_resampler(out->multi_resampler[card]);
                out->multi_resampler[card] = NULL;
            }
        }

        adev->active_output = 0;
        //global_output = NULL;
        /* stop writing to echo reference */
        if (out->echo_reference != NULL) {
            out->echo_reference->write(out->echo_reference, NULL);
            out->echo_reference = NULL;
        }
        out->standby = 1;
    }
    return 0;
}

static int out_standby(struct audio_stream *stream)
{
    struct sunxi_stream_out *out = (struct sunxi_stream_out *)stream;
    int status;

    ALOGD("out_standby");
    //pthread_mutex_lock(&out->dev->lock);
    pthread_mutex_lock(&out->lock);
    status = do_output_standby(out);
    pthread_mutex_unlock(&out->lock);
    //pthread_mutex_unlock(&out->dev->lock);
    return status;
}

static int out_dump(const struct audio_stream *stream, int fd)
{
    struct sunxi_stream_out *out = (struct sunxi_stream_out *)stream;
    ALOGV("fun = %s, fd = %d", __FUNCTION__, fd);
    return 0;
}

static int out_set_parameters(struct audio_stream *stream, const char *kvpairs)
{
    struct sunxi_stream_out *out = (struct sunxi_stream_out *)stream;
    ALOGV("out_set_parameters: %s", kvpairs);
    return 0;
}

static char * out_get_parameters(const struct audio_stream *stream, const char *keys)
{
    struct sunxi_stream_out *out = (struct sunxi_stream_out *)stream;
    ALOGV("fun = %s, keys = %s", __FUNCTION__, keys);
    return strdup("");
}

static uint32_t out_get_latency(const struct audio_stream_out *stream)
{
    struct sunxi_stream_out *out = (struct sunxi_stream_out *)stream;

    return (SHORT_PERIOD_SIZE * PLAYBACK_PERIOD_COUNT * 1000) / out->config.rate;
}

static int out_set_volume(struct audio_stream_out *stream, float left,
                          float right)
{
    struct sunxi_stream_out *out = (struct sunxi_stream_out *)stream;
    ALOGV("fun = %s, left = %f, right = %f", __FUNCTION__, left, right);
    return -ENOSYS;
}

static ssize_t out_write(struct audio_stream_out *stream, const void* buffer,
                         size_t bytes)
{
    F_LOG;
    int ret;
    struct sunxi_stream_out *out = (struct sunxi_stream_out *)stream;
    struct sunxi_audio_device *adev = out->dev;
    size_t frame_size = audio_stream_out_frame_size(stream);
    size_t in_frames = bytes / frame_size;
    size_t out_frames = RESAMPLER_BUFFER_SIZE / frame_size;
    bool force_input_standby = false;
    struct sunxi_stream_in *in;
    int kernel_frames;
    void *buf;
    int card = adev->card_id;
    
    if (adev->mode == AUDIO_MODE_IN_CALL || adev->mode == AUDIO_MODE_MODE_FACTORY_TEST || adev->mode == AUDIO_MODE_FM) {
        ALOGD("mode in call, do not out_write");
        int time = bytes*1000*1000/4/DEFAULT_I2S_OUT_SAMPLING_RATE;
        usleep(time);
        return bytes;
    }

    /* acquiring hw device mutex systematically is useful if a low priority thread is waiting
     * on the output stream mutex - e.g. executing select_mode() while holding the hw device
     * mutex
     */
    pthread_mutex_lock(&adev->lock);
    pthread_mutex_lock(&out->lock);

    if (out->standby) {
        ret = start_output_stream(out);
        if (ret != 0) {
            pthread_mutex_unlock(&adev->lock);
            goto exit;
        }
        out->standby = 0;
        /* a change in output device may change the microphone selection */
        if (adev->active_input &&
                adev->active_input->source == AUDIO_SOURCE_VOICE_COMMUNICATION)
            force_input_standby = true;
    }
    pthread_mutex_unlock(&adev->lock);
    out->write_threshold = SHORT_PERIOD_SIZE * PLAYBACK_PERIOD_COUNT;
    out->config.avail_min = SHORT_PERIOD_SIZE;

    if (adev->PcmManager.BufExist) {
		WritePcmData((void *)buffer, out_frames * frame_size, &adev->PcmManager);
        memset(buffer, 0, out_frames * frame_size); //mute
    }

    if (card >= 0 && adev->dev_manager.flag_exist
        && (adev->dev_manager.flag_out == AUDIO_OUT)
        && adev->dev_manager.flag_out_active
        && (!strcmp(adev->dev_manager.name, AUDIO_NAME_I2S)))      //only support i2s
    {
        out->multi_config[card].avail_min = SHORT_PERIOD_SIZE;
        pcm_set_avail_min(out->multi_pcm[card], out->multi_config[card].avail_min);

        if (out->multi_resampler[card]) {
            out->multi_resampler[card]->resample_from_input(out->multi_resampler[card],
                                                            (int16_t *)buffer,
                                                            &in_frames,
                                                            (int16_t *)out->buffer,
                                                            &out_frames);
            buf = out->buffer;
        } else {
            out_frames = in_frames;
            buf = (void *)buffer;
        }

        if (out->echo_reference != NULL) {
            struct echo_reference_buffer b;
            b.raw = (void *)buffer;
            b.frame_count = in_frames;

            get_playback_delay(out, out_frames, &b);
            out->echo_reference->write(out->echo_reference, &b);
        }

        if (out->multi_config[card].channels == 2) {
            ret = pcm_write(out->multi_pcm[card], (void *)buf, out_frames * frame_size);
        } else {
            size_t i;
            char *pcm_buf = (char *)buf;
            for (i = 0; i < out_frames; i++) {
                pcm_buf[2 * i + 2] = pcm_buf[4 * i + 4];
                pcm_buf[2 * i + 3] = pcm_buf[4 * i + 5];
            }
            ret = pcm_write(out->multi_pcm[card], (void *)buf, out_frames * frame_size / 2);
        }

        if (ret!=0) {
            ALOGE("##############out_write()  Warning:write fail, reopen it ret = %d #######################", ret);
            do_output_standby(out);
            usleep(30000);
        }
    }
exit:
    pthread_mutex_unlock(&out->lock);
    if (ret != 0) {
        usleep(bytes * 1000000 / audio_stream_out_frame_size(stream) /
               out_get_sample_rate(&stream->common));
    }

    if (force_input_standby) {
        pthread_mutex_lock(&adev->lock);
        if (adev->active_input) {
            in = adev->active_input;
            pthread_mutex_lock(&in->lock);
            do_input_standby(in);
            pthread_mutex_unlock(&in->lock);
        }
        pthread_mutex_unlock(&adev->lock);
    }

    return bytes;
}

static int out_get_render_position(const struct audio_stream_out *stream,
                                   uint32_t *dsp_frames)
{
    struct sunxi_stream_out *out = (struct sunxi_stream_out *)stream;
    ALOGV("fun = %s, dsp_frames = %u", __FUNCTION__, dsp_frames);
    return -EINVAL;
}

static int out_add_audio_effect(const struct audio_stream *stream, effect_handle_t effect)
{
    struct sunxi_stream_out *out = (struct sunxi_stream_out *)stream;
    return 0;
}

static int out_remove_audio_effect(const struct audio_stream *stream, effect_handle_t effect)
{
    struct sunxi_stream_out *out = (struct sunxi_stream_out *)stream;
    return 0;
}

static int out_get_next_write_timestamp(const struct audio_stream_out *stream,
                                        int64_t *timestamp)
{
    struct sunxi_stream_out *out = (struct sunxi_stream_out *)stream;
    ALOGV("fun = %s, timestamp = %ld", __FUNCTION__, timestamp);
    return -EINVAL;
}

/* must be called with hw device and input stream mutexes locked */
static int start_input_stream(struct sunxi_stream_in *in)
{
    int ret = 0;
    struct sunxi_audio_device *adev = in->dev;

    adev->active_input = in;
    int card = adev->card_id;

    if (adev->mode == AUDIO_MODE_IN_CALL) { // && adev->bluetooth_voice
        ALOGD("in call mode , start_input_stream, return");
        return 0;
    }

    if (in->need_echo_reference && in->echo_reference == NULL)
        in->echo_reference = get_echo_reference(adev,
                                        in->config.channels,
                                        in->requested_rate);

    int in_ajust_rate = in->requested_rate;
    #ifdef EXTERNAL_USE__48000_SAMPLERATE
    if (!(in->requested_rate % SAMPLING_RATE_8K))
        in_ajust_rate = in->requested_rate;
    else {
        in_ajust_rate = SAMPLING_RATE_8K * in->requested_rate / SAMPLING_RATE_11K;
        if (in_ajust_rate > SAMPLING_RATE_48K)
            in_ajust_rate = SAMPLING_RATE_48K;
        ALOGV("out/in stream should be both 48K serial, force capture rate: %d", in_ajust_rate);
    }
    #else
    // out/in stream should be both 44.1K serial
    if (!(in->requested_rate % SAMPLING_RATE_11K))
        in_ajust_rate = in->requested_rate;
    else {
        in_ajust_rate = SAMPLING_RATE_11K * in->requested_rate / SAMPLING_RATE_8K;
        if (in_ajust_rate > SAMPLING_RATE_44K)
            in_ajust_rate = SAMPLING_RATE_44K;

        ALOGV("out/in stream should be both 44.1K serial, force capture rate: %d", in_ajust_rate);
    }
    #endif

    if (adev->dev_manager.flag_exist
        && (adev->dev_manager.flag_in == AUDIO_IN)
        && adev->dev_manager.flag_in_active
        && strcmp(adev->dev_manager.name, AUDIO_NAME_I2S)) {
        ALOGV("use %s to capture audio", adev->dev_manager.name);
        in->pcm = pcm_open(card, PORT_I2S, PCM_IN, &in->config);
    }
    else
    {
        ALOGE("no active input device!");
	 goto err;
    }

    if (!pcm_is_ready(in->pcm)) {
        ALOGE("cannot open pcm_in driver: %s", pcm_get_error(in->pcm));
        pcm_close(in->pcm);
        adev->active_input = NULL;
        return -ENOMEM;
    }

    if (in->requested_rate != in->config.rate) {
        in->buf_provider.get_next_buffer = get_next_buffer;
        in->buf_provider.release_buffer = release_buffer;

        ret = create_resampler(in->config.rate,
                                in->requested_rate,
                                in->config.channels,
                                RESAMPLER_QUALITY_DEFAULT,
                                &in->buf_provider,
                                &in->resampler);
        if (ret != 0) {
            ALOGE("create in resampler failed, %d -> %d", in->config.rate, in->requested_rate);
            ret = -EINVAL;
            goto err;
        }

        ALOGV("create in resampler OK, %d -> %d", in->config.rate, in->requested_rate);
    } else
        ALOGV("do not use in resampler");

    /* if no supported sample rate is available, use the resampler */
    if (in->resampler) {
        in->resampler->reset(in->resampler);
        in->frames_in = 0;
    }

#ifdef ENABLE_DENOISE
    if(!in->de_handle){
        int flag = 0;
        int rate = 16000;
        if(in->config.rate == 8000 || in->config.rate == 16000)
            rate = in->config.rate;

        in->de_handle = rt_init(in->de_handle, flag, rate);
        ALOGD("rt_init:flag=%d,config.rate=%d",flag, in->config.rate);
    }
    if(!in->de_handle)
        ALOGE("rt_init failed");
#endif

    return 0;

err:
    if (in->resampler)
        release_resampler(in->resampler);

    return -1;
}

static uint32_t in_get_sample_rate(const struct audio_stream *stream)
{
    struct sunxi_stream_in *in = (struct sunxi_stream_in *)stream;

    return in->requested_rate;
}

static int in_set_sample_rate(struct audio_stream *stream, uint32_t rate)
{
    struct sunxi_stream_in *out = (struct sunxi_stream_in *)stream;
    ALOGV("fun = %s, rate = %d", __FUNCTION__, rate);
    return 0;
}

static size_t in_get_buffer_size(const struct audio_stream *stream)
{
    struct sunxi_stream_in *in = (struct sunxi_stream_in *)stream;

    return get_input_buffer_size(in->requested_rate,
                                 AUDIO_FORMAT_PCM_16_BIT,
                                 in->config.channels);
}

static audio_channel_mask_t in_get_channels(const struct audio_stream *stream)
{
    struct sunxi_stream_in *in = (struct sunxi_stream_in *)stream;

    if (in->config.channels == 1) {
        return AUDIO_CHANNEL_IN_MONO;
    } else {
        return AUDIO_CHANNEL_IN_STEREO;
    }
}

static audio_format_t in_get_format(const struct audio_stream *stream)
{
    struct sunxi_stream_in *in = (struct sunxi_stream_in *)stream;
    return AUDIO_FORMAT_PCM_16_BIT;
}

static int in_set_format(struct audio_stream *stream, audio_format_t format)
{
    struct sunxi_stream_in *in = (struct sunxi_stream_in *)stream;
    ALOGV("fun = %s, format = %d", __FUNCTION__, format);
    return 0;
}

/* must be called with hw device and input stream mutexes locked */
static int do_input_standby(struct sunxi_stream_in *in)
{
    struct sunxi_audio_device *adev = in->dev;

    if (!in->standby) {
        pcm_close(in->pcm);
        in->pcm = NULL;

        adev->active_input = 0;

        if (in->echo_reference != NULL) {
            /* stop reading from echo reference */
            in->echo_reference->read(in->echo_reference, NULL);
            put_echo_reference(adev, in->echo_reference);
            in->echo_reference = NULL;
        }

        if (in->resampler) {
            release_resampler(in->resampler);
            in->resampler = NULL;
        }

    #ifdef ENABLE_DENOISE
        if(in->de_handle){
            rt_end(in->de_handle);
            in->de_handle = NULL;
        }
    #endif
        in->standby = 1;
    }
    return 0;
}

static int in_standby(struct audio_stream *stream)
{
    struct sunxi_stream_in *in = (struct sunxi_stream_in *)stream;
    int status;

    pthread_mutex_lock(&in->dev->lock);
    pthread_mutex_lock(&in->lock);
    status = do_input_standby(in);
    pthread_mutex_unlock(&in->lock);
    pthread_mutex_unlock(&in->dev->lock);
    return status;
}

static int in_dump(const struct audio_stream *stream, int fd)
{
    struct sunxi_stream_in *in = (struct sunxi_stream_in *)stream;
    ALOGV("fun = %s, fd = %d", __FUNCTION__, fd);
    return 0;
}

static int in_set_parameters(struct audio_stream *stream, const char *kvpairs)
{
    struct sunxi_stream_in *in = (struct sunxi_stream_in *)stream;
    ALOGV("in_set_parameters: %s", kvpairs);

    return 0;
}

static char * in_get_parameters(const struct audio_stream *stream,
                                const char *keys)
{
    struct sunxi_stream_in *in = (struct sunxi_stream_in *)stream;
    ALOGV("in_get_parameters: %s", keys);
    return strdup("");
}

static int in_set_gain(struct audio_stream_in *stream, float gain)
{
    struct sunxi_stream_in *in = (struct sunxi_stream_in *)stream;
    ALOGV("fun = %s, gain = %f", __FUNCTION__, gain);
    return 0;
}

static void get_capture_delay(struct sunxi_stream_in *in,
                       size_t frames,
                       struct echo_reference_buffer *buffer)
{

    /* read frames available in kernel driver buffer */
    size_t kernel_frames;
    struct timespec tstamp;
    long buf_delay = 0;
    long rsmp_delay = 0;
    long kernel_delay = 0;
    long delay_ns = 0;

    if (pcm_get_htimestamp(in->pcm, &kernel_frames, &tstamp) < 0) {
        buffer->time_stamp.tv_sec  = 0;
        buffer->time_stamp.tv_nsec = 0;
        buffer->delay_ns           = 0;
        ALOGW("read get_capture_delay(): pcm_htimestamp error");
        return;
    }

    /* read frames available in audio HAL input buffer
     * add number of frames being read as we want the capture time of first sample
     * in current buffer */
    buf_delay = (long)(((int64_t)(in->frames_in + in->proc_frames_in) * 1000000000)
                                    / in->config.rate);
    /* add delay introduced by resampler */
    if (in->resampler) {
        rsmp_delay = in->resampler->delay_ns(in->resampler);
    }

    kernel_delay = (long)(((int64_t)kernel_frames * 1000000000) / in->config.rate);

    delay_ns = kernel_delay + buf_delay + rsmp_delay;

    buffer->time_stamp = tstamp;
    buffer->delay_ns   = delay_ns;
    ALOGV("get_capture_delay time_stamp = [%ld].[%ld], delay_ns: [%d],"
         " kernel_delay:[%ld], buf_delay:[%ld], rsmp_delay:[%ld], kernel_frames:[%d], "
         "in->frames_in:[%d], in->proc_frames_in:[%d], frames:[%d]",
         buffer->time_stamp.tv_sec , buffer->time_stamp.tv_nsec, buffer->delay_ns,
         kernel_delay, buf_delay, rsmp_delay, kernel_frames,
         in->frames_in, in->proc_frames_in, frames);
}

static int32_t update_echo_reference(struct sunxi_stream_in *in, size_t frames)
{
    struct echo_reference_buffer b;
    b.delay_ns = 0;

    ALOGV("update_echo_reference, frames = [%d], in->ref_frames_in = [%d],  "
          "b.frame_count = [%d]",
         frames, in->ref_frames_in, frames - in->ref_frames_in);
    if (in->ref_frames_in < frames) {
        if (in->ref_buf_size < frames) {
            in->ref_buf_size = frames;
            in->ref_buf = (int16_t *)realloc(in->ref_buf,
                                             in->ref_buf_size *
                                                 in->config.channels * sizeof(int16_t));
        }

        b.frame_count = frames - in->ref_frames_in;
        b.raw = (void *)(in->ref_buf + in->ref_frames_in * in->config.channels);

        get_capture_delay(in, frames, &b);

        if (in->echo_reference->read(in->echo_reference, &b) == 0) {
            in->ref_frames_in += b.frame_count;
            ALOGV("update_echo_reference: in->ref_frames_in:[%d], "
                    "in->ref_buf_size:[%d], frames:[%d], b.frame_count:[%d]",
                 in->ref_frames_in, in->ref_buf_size, frames, b.frame_count);
        }
    } else
        ALOGW("update_echo_reference: NOT enough frames to read ref buffer");

    return b.delay_ns;
}

static int set_preprocessor_param(effect_handle_t handle,
                           effect_param_t *param)
{
    uint32_t size = sizeof(int);
    uint32_t psize = ((param->psize - 1) / sizeof(int) + 1) * sizeof(int) +
                        param->vsize;

    int status = (*handle)->command(handle,
                                   EFFECT_CMD_SET_PARAM,
                                   sizeof (effect_param_t) + psize,
                                   param,
                                   &size,
                                   &param->status);
    if (status == 0)
        status = param->status;

    return status;
}

static int set_preprocessor_echo_delay(effect_handle_t handle,
                                     int32_t delay_us)
{
    uint32_t buf[sizeof(effect_param_t) / sizeof(uint32_t) + 2];
    effect_param_t *param = (effect_param_t *)buf;

    param->psize = sizeof(uint32_t);
    param->vsize = sizeof(uint32_t);
    *(uint32_t *)param->data = AEC_PARAM_ECHO_DELAY;
    *((int32_t *)param->data + 1) = delay_us;

    return set_preprocessor_param(handle, param);
}

static void push_echo_reference(struct sunxi_stream_in *in, size_t frames)
{
    /* read frames from echo reference buffer and update echo delay
     * in->ref_frames_in is updated with frames available in in->ref_buf */
    int32_t delay_us = update_echo_reference(in, frames)/1000;
    int i;
    audio_buffer_t buf;

    if (in->ref_frames_in < frames)
        frames = in->ref_frames_in;

    buf.frameCount = frames;
    buf.raw = in->ref_buf;

    for (i = 0; i < in->num_preprocessors; i++) {
        if ((*in->preprocessors[i])->process_reverse == NULL)
            continue;

        (*in->preprocessors[i])->process_reverse(in->preprocessors[i],
                                               &buf,
                                               NULL);
        set_preprocessor_echo_delay(in->preprocessors[i], delay_us);
    }

    in->ref_frames_in -= buf.frameCount;
    if (in->ref_frames_in) {
        memcpy(in->ref_buf,
               in->ref_buf + buf.frameCount * in->config.channels,
               in->ref_frames_in * in->config.channels * sizeof(int16_t));
    }
}

static int get_next_buffer(struct resampler_buffer_provider *buffer_provider,
                                   struct resampler_buffer* buffer)
{
    struct sunxi_stream_in *in;

    if (buffer_provider == NULL || buffer == NULL)
        return -EINVAL;

    in = (struct sunxi_stream_in *)((char *)buffer_provider -
                                   offsetof(struct sunxi_stream_in, buf_provider));

    if (in->pcm == NULL) {
        buffer->raw = NULL;
        buffer->frame_count = 0;
        in->read_status = -ENODEV;
        return -ENODEV;
    }

//  ALOGV("get_next_buffer: in->config.period_size: %d, audio_stream_frame_size: %d",
//      in->config.period_size, audio_stream_frame_size(&in->stream.common));
    if (in->frames_in == 0) {
        in->read_status = pcm_read(in->pcm,
                                   (void*)in->buffer,
                                   in->config.period_size *
                                       audio_stream_in_frame_size(&in->stream));
        if (in->read_status != 0) {
            ALOGE("get_next_buffer() pcm_read error %d, %s", in->read_status, strerror(errno));
            buffer->raw = NULL;
            buffer->frame_count = 0;
            return in->read_status;
        }
        in->frames_in = in->config.period_size;
    }

    buffer->frame_count = (buffer->frame_count > in->frames_in) ?
                                in->frames_in : buffer->frame_count;
    buffer->i16 = in->buffer + (in->config.period_size - in->frames_in) *
                                                in->config.channels;

    return in->read_status;

}

static void release_buffer(struct resampler_buffer_provider *buffer_provider,
                                  struct resampler_buffer* buffer)
{
    struct sunxi_stream_in *in;

    if (buffer_provider == NULL || buffer == NULL)
        return;

    in = (struct sunxi_stream_in *)((char *)buffer_provider -
                                   offsetof(struct sunxi_stream_in, buf_provider));

    in->frames_in -= buffer->frame_count;
}

/* read_frames() reads frames from kernel driver, down samples to capture rate
 * if necessary and output the number of frames requested to the buffer specified */
static ssize_t read_frames(struct sunxi_stream_in *in, void *buffer, ssize_t frames)
{
    ssize_t frames_wr = 0;

    while (frames_wr < frames) {
        size_t frames_rd = frames - frames_wr;
        if (in->resampler != NULL) {
            in->resampler->resample_from_provider(in->resampler,
                    (int16_t *)((char *)buffer +
                            frames_wr * audio_stream_in_frame_size(&in->stream)),
                    &frames_rd);
        } else {
            struct resampler_buffer buf = {
                    { raw : NULL, },
                    frame_count : frames_rd,
            };
            get_next_buffer(&in->buf_provider, &buf);
            if (buf.raw != NULL) {
                memcpy((char *)buffer +
                           frames_wr * audio_stream_in_frame_size(&in->stream),
                        buf.raw,
                        buf.frame_count * audio_stream_in_frame_size(&in->stream));
                frames_rd = buf.frame_count;
            }
            release_buffer(&in->buf_provider, &buf);
        }
        /* in->read_status is updated by getNextBuffer() also called by
         * in->resampler->resample_from_provider() */
        if (in->read_status != 0)
            return in->read_status;

        frames_wr += frames_rd;
    }
    return frames_wr;
}

/* process_frames() reads frames from kernel driver (via read_frames()),
 * calls the active audio pre processings and output the number of frames requested
 * to the buffer specified */
static ssize_t process_frames(struct sunxi_stream_in *in, void* buffer, ssize_t frames)
{
    ssize_t frames_wr = 0;
    audio_buffer_t in_buf;
    audio_buffer_t out_buf;
    int i;

    while (frames_wr < frames) {
        /* first reload enough frames at the end of process input buffer */
        if (in->proc_frames_in < (size_t)frames) {
            ssize_t frames_rd;

            if (in->proc_buf_size < (size_t)frames) {
                in->proc_buf_size = (size_t)frames;
                in->proc_buf = (int16_t *)realloc(in->proc_buf,
                                         in->proc_buf_size *
                                             in->config.channels * sizeof(int16_t));
                ALOGV("process_frames(): in->proc_buf %p size extended to %d frames",
                     in->proc_buf, in->proc_buf_size);
            }
            frames_rd = read_frames(in,
                                    in->proc_buf + in->proc_frames_in * in->config.channels,
                                    frames - in->proc_frames_in);
            if (frames_rd < 0) {
                frames_wr = frames_rd;
                break;
            }

            in->proc_frames_in += frames_rd;
        }

        if (in->echo_reference != NULL)
            push_echo_reference(in, in->proc_frames_in);

         /* in_buf.frameCount and out_buf.frameCount indicate respectively
          * the maximum number of frames to be consumed and produced by process() */
        in_buf.frameCount   = in->proc_frames_in;
        in_buf.s16          = in->proc_buf;
        out_buf.frameCount  = frames - frames_wr;
        out_buf.s16 = (int16_t *)buffer + frames_wr * in->config.channels;

        for (i = 0; i < in->num_preprocessors; i++)
            (*in->preprocessors[i])->process(in->preprocessors[i],
                                               &in_buf,
                                               &out_buf);

        /* process() has updated the number of frames consumed and produced in
         * in_buf.frameCount and out_buf.frameCount respectively
         * move remaining frames to the beginning of in->proc_buf */
        in->proc_frames_in -= in_buf.frameCount;
        if (in->proc_frames_in) {
            memcpy(in->proc_buf,
                   in->proc_buf + in_buf.frameCount * in->config.channels,
                   in->proc_frames_in * in->config.channels * sizeof(int16_t));
        }

        /* if not enough frames were passed to process(), read more and retry. */
        if (out_buf.frameCount == 0)
            continue;

        frames_wr += out_buf.frameCount;
    }
    return frames_wr;
}

static ssize_t in_read(struct audio_stream_in *stream, void* buffer,
                       size_t bytes)
{
    int ret = 0;
    struct sunxi_stream_in *in      = (struct sunxi_stream_in *)stream;
    struct sunxi_audio_device *adev = in->dev;
    size_t frames_rq                = 0;

#ifdef ENABLE_DENOISE
    size_t frame_len = 128;
    short de_buffer[frame_len];
    short *de_out[frame_len];
    short *de_in = NULL;
    int de_bytes = 2 * 2 * frame_len;
    int de_frame = bytes / de_bytes;
#endif

    if (adev->mode == AUDIO_MODE_IN_CALL) {
        ALOGD("in call mode, in_read, return ;");
        usleep(10000);
        return 1;
    }

    /* acquiring hw device mutex systematically is useful if a low priority thread is waiting
     * on the input stream mutex - e.g. executing select_mode() while holding the hw device
     * mutex
     */
    if (adev->PcmManager.BufExist) {
        pthread_mutex_lock(&adev->lock);

        pthread_mutex_lock(&in->lock);
        if (in->standby) {
            in->standby = 0;
        }
        pthread_mutex_unlock(&adev->lock);

        if (ret < 0)
            goto exit;

        //if (bytes > adev->PcmManager.DataLen)
            //usleep(10000);

        ret = ReadPcmData(buffer, bytes, &adev->PcmManager);

        if (ret > 0)
            ret = 0;

        if (ret == 0 && adev->mic_mute)
            memset(buffer, 0, bytes);

        pthread_mutex_unlock(&in->lock);
            return bytes;
    }

    pthread_mutex_lock(&adev->lock);

    pthread_mutex_lock(&in->lock);
    if (in->standby) {
        ret = start_input_stream(in);
        if (ret == 0)
            in->standby = 0;
    }
    pthread_mutex_unlock(&adev->lock);

    if (ret < 0)
        goto exit;

    /* place after start_input_stream, because start_input_stream() change frame size */
    frames_rq = bytes / audio_stream_in_frame_size(stream);

    if (in->num_preprocessors != 0) {
        ret = read_frames(in, buffer, frames_rq);//ret = process_frames(in, buffer, frames_rq);
    } else if (in->resampler != NULL) {
        ret = read_frames(in, buffer, frames_rq);
    } else {
        ret = pcm_read(in->pcm, buffer, bytes);
    }

    if (ret > 0)
        ret = 0;
        // denoise
#ifdef ENABLE_DENOISE
    if(in->de_handle){
        int i = 0, j = 0, len_sub = 0;
        for(i = 0; i < de_frame; i++){
            len_sub = i * de_bytes;
	     rt_dec(in->de_handle, (short *)((char *)buffer + len_sub), de_out, de_bytes);
            de_in = (short *)((char *)buffer + len_sub);
            memcpy(de_buffer,de_out,de_bytes/2);
            for(j= 0; j < de_bytes/4; j++){
                de_in[2*j]    = de_buffer[j];
                de_in[2*j +1] = de_buffer[j];
            }
        }
    }
#endif

    if (ret == 0 && adev->mic_mute)
        memset(buffer, 0, bytes);

exit:
    if (ret < 0)
        usleep(bytes * 1000000 / audio_stream_in_frame_size(stream) /
               in_get_sample_rate(&stream->common));

    pthread_mutex_unlock(&in->lock);
    return bytes;
}

static uint32_t in_get_input_frames_lost(struct audio_stream_in *stream)
{
    struct sunxi_stream_in *in = (struct sunxi_stream_in *)stream;
    return 0;
}

static int in_add_audio_effect(const struct audio_stream *stream,
                               effect_handle_t effect)
{
    struct sunxi_stream_in *in = (struct sunxi_stream_in *)stream;
    int status;
    effect_descriptor_t desc;

    pthread_mutex_lock(&in->dev->lock);
    pthread_mutex_lock(&in->lock);
    if (in->num_preprocessors >= MAX_PREPROCESSORS) {
        status = -ENOSYS;
        goto exit;
    }

    status = (*effect)->get_descriptor(effect, &desc);
    if (status != 0)
        goto exit;

    in->preprocessors[in->num_preprocessors++] = effect;

    if (memcmp(&desc.type, FX_IID_AEC, sizeof(effect_uuid_t)) == 0) {
        in->need_echo_reference = true;
        do_input_standby(in);
    }

exit:

    pthread_mutex_unlock(&in->lock);
    pthread_mutex_unlock(&in->dev->lock);
    return status;
}

static int in_remove_audio_effect(const struct audio_stream *stream,
                                  effect_handle_t effect)
{
    struct sunxi_stream_in *in = (struct sunxi_stream_in *)stream;
    int i;
    int status = -EINVAL;
    bool found = false;
    effect_descriptor_t desc;

    pthread_mutex_lock(&in->dev->lock);
    pthread_mutex_lock(&in->lock);
    if (in->num_preprocessors <= 0) {
        status = -ENOSYS;
        goto exit;
    }

    for (i = 0; i < in->num_preprocessors; i++) {
        if (found) {
            in->preprocessors[i - 1] = in->preprocessors[i];
            continue;
        }
        if (in->preprocessors[i] == effect) {
            in->preprocessors[i] = NULL;
            status = 0;
            found = true;
        }
    }

    if (status != 0)
        goto exit;

    in->num_preprocessors--;

    status = (*effect)->get_descriptor(effect, &desc);
    if (status != 0)
        goto exit;

    if (memcmp(&desc.type, FX_IID_AEC, sizeof(effect_uuid_t)) == 0) {
        in->need_echo_reference = false;
        do_input_standby(in);
    }

exit:

    pthread_mutex_unlock(&in->lock);
    pthread_mutex_unlock(&in->dev->lock);
    return status;
}

static int adev_open_output_stream(struct audio_hw_device *dev,
                                   audio_io_handle_t handle,
                                   audio_devices_t devices,
                                   audio_output_flags_t flags,
                                   struct audio_config *config,
                                   struct audio_stream_out **stream_out)
{
    struct sunxi_audio_device *ladev = (struct sunxi_audio_device *)dev;
    struct sunxi_stream_out *out;
    int ret;

    ALOGV("adev_open_output_stream, flags: %x", flags);

    out = (struct sunxi_stream_out *)calloc(1, sizeof(struct sunxi_stream_out));
    if (!out)
        return -ENOMEM;

    out->buffer = malloc(RESAMPLER_BUFFER_SIZE); /* todo: allow for reallocing */

    out->stream.common.get_sample_rate  = out_get_sample_rate;
    out->stream.common.set_sample_rate  = out_set_sample_rate;
    out->stream.common.get_buffer_size  = out_get_buffer_size;
    out->stream.common.get_channels     = out_get_channels;
    out->stream.common.get_format       = out_get_format;
    out->stream.common.set_format       = out_set_format;
    out->stream.common.standby          = out_standby;
    out->stream.common.dump             = out_dump;
    out->stream.common.set_parameters   = out_set_parameters;
    out->stream.common.get_parameters   = out_get_parameters;
    out->stream.common.add_audio_effect = out_add_audio_effect;
    out->stream.common.remove_audio_effect = out_remove_audio_effect;
    out->stream.get_latency             = out_get_latency;
    out->stream.set_volume              = out_set_volume;
    out->stream.write                   = out_write;
    out->stream.get_render_position     = out_get_render_position;
    out->stream.get_next_write_timestamp = out_get_next_write_timestamp;

    out->config                         = pcm_config_mm_out;
    out->dev                            = ladev;
    out->standby                        = 1;

    /* FIXME: when we support multiple output devices, we will want to
     * do the following:
	 * adev->out_device = out->device;
     * select_output_device(adev);
     * This is because out_set_parameters() with a route is not
     * guaranteed to be called after an output stream is opened. */
    config->format                = out_get_format(&out->stream.common);
    config->channel_mask          = out_get_channels(&out->stream.common);
    config->sample_rate           = out_get_sample_rate(&out->stream.common);

    ALOGV("+++++++++++++++ adev_open_output_stream: req_sample_rate: %d, fmt: %x, channel_count: %d",
        config->sample_rate, config->format, config->channel_mask);

    *stream_out = &out->stream;
    return 0;

err_open:
    free(out);
    *stream_out = NULL;
    return ret;
}

static void adev_close_output_stream(struct audio_hw_device *dev,
                                     struct audio_stream_out *stream)
{
    struct sunxi_stream_out *out = (struct sunxi_stream_out *)stream;
    struct sunxi_audio_device *adev = out->dev;
    int card = adev->card_id;

    if (adev->mode == AUDIO_MODE_IN_CALL || adev->mode == AUDIO_MODE_MODE_FACTORY_TEST  || adev->mode == AUDIO_MODE_FM) {
        ALOGW("mode in call, do not adev_close_output_stream");
        return ;
    }
	
    out_standby(&stream->common);

    if (out->buffer)
        free(out->buffer);
    if (out->resampler)
        release_resampler(out->resampler);

    if(card >= 0) {
        if (out->multi_resampler[card])
            release_resampler(out->multi_resampler[card]);
    }
    free(stream);
}

static int adev_set_parameters(struct audio_hw_device *dev, const char *kvpairs)
{
    struct sunxi_audio_device *adev = (struct sunxi_audio_device *)dev;
    ALOGV("adev_set_parameters kvpairs = %s", kvpairs);
    return 0;
}

static char * adev_get_parameters(const struct audio_hw_device *dev,
                                  const char *keys)
{
    struct sunxi_audio_device *adev = (struct sunxi_audio_device *)dev;
    ALOGV("fun = %s, keys = %s", __FUNCTION__, keys);
    return strdup("");
}

static int adev_init_check(const struct audio_hw_device *dev)
{
    struct sunxi_audio_device *adev = (struct sunxi_audio_device *)dev;
    return 0;
}

static int adev_set_voice_volume(struct audio_hw_device *dev, float volume)
{
    struct sunxi_audio_device *adev = (struct sunxi_audio_device *)dev;
    ALOGV("fun = %s, volume = %f", __FUNCTION__, volume);
    return 0;
}

static int adev_set_master_volume(struct audio_hw_device *dev, float volume)
{
    F_LOG;
    struct sunxi_audio_device *adev = (struct sunxi_audio_device *)dev;
    ALOGV("fun = %s, volume = %f", __FUNCTION__, volume);
    return -ENOSYS;
}

static int adev_get_master_volume(struct audio_hw_device *dev, float *volume)
{
    struct sunxi_audio_device *adev = (struct sunxi_audio_device *)dev;
    ALOGV("fun = %s, volume = %f", __FUNCTION__, volume);
    return -ENOSYS;
}

static int adev_set_mode(struct audio_hw_device *dev, audio_mode_t mode)
{
    struct sunxi_audio_device *adev = (struct sunxi_audio_device *)dev;
    ALOGV("fun = %s, mode = %d", __FUNCTION__, mode);
    return 0;
}

static int adev_set_mic_mute(struct audio_hw_device *dev, bool state)
{
    struct sunxi_audio_device *adev = (struct sunxi_audio_device *)dev;

    adev->mic_mute = state;

    return 0;
}

static int adev_get_mic_mute(const struct audio_hw_device *dev, bool *state)
{
    struct sunxi_audio_device *adev = (struct sunxi_audio_device *)dev;

    *state = adev->mic_mute;

    return 0;
}

static size_t adev_get_input_buffer_size(const struct audio_hw_device *dev,
                                         const struct audio_config *config)
{
    size_t size;
    int channel_count = popcount(config->channel_mask);
    if (check_input_parameters(config->sample_rate, config->format, channel_count) != 0)
        return 0;

    return get_input_buffer_size(config->sample_rate, config->format, channel_count);
}

static int adev_open_input_stream(struct audio_hw_device *dev,
                                  audio_io_handle_t handle,
                                  audio_devices_t devices,
                                  struct audio_config *config,
                                  struct audio_stream_in **stream_in)
{
    struct sunxi_audio_device *ladev = (struct sunxi_audio_device *)dev;
    struct sunxi_stream_in *in;
    int ret;
    int channel_count = popcount(config->channel_mask);

    *stream_in = NULL;

    if (check_input_parameters(config->sample_rate, config->format, channel_count) != 0)
        return -EINVAL;

    in = (struct sunxi_stream_in *)calloc(1, sizeof(struct sunxi_stream_in));
    if (!in)
        return -ENOMEM;

    in->stream.common.get_sample_rate   = in_get_sample_rate;
    in->stream.common.set_sample_rate   = in_set_sample_rate;
    in->stream.common.get_buffer_size   = in_get_buffer_size;
    in->stream.common.get_channels      = in_get_channels;
    in->stream.common.get_format        = in_get_format;
    in->stream.common.set_format        = in_set_format;
    in->stream.common.standby           = in_standby;
    in->stream.common.dump              = in_dump;
    in->stream.common.set_parameters    = in_set_parameters;
    in->stream.common.get_parameters    = in_get_parameters;
    in->stream.common.add_audio_effect  = in_add_audio_effect;
    in->stream.common.remove_audio_effect = in_remove_audio_effect;
    in->stream.set_gain = in_set_gain;
    in->stream.read 	= in_read;
    in->stream.get_input_frames_lost = in_get_input_frames_lost;

    in->requested_rate 	= config->sample_rate;

    // default config
    memcpy(&in->config, &pcm_config_mm_in, sizeof(pcm_config_mm_in));
    in->config.channels = channel_count;

    ALOGV("to malloc in-buffer: period_size: %d, frame_size: %d",
        in->config.period_size, audio_stream_in_frame_size(&in->stream));
    in->buffer = malloc(in->config.period_size *
                        audio_stream_in_frame_size(&in->stream) * 8);

    if (!in->buffer) {
        ret = -ENOMEM;
        goto err;
    }
    //  memset(in->buffer, 0, in->config.period_size *
    //                        audio_stream_frame_size(&in->stream.common) * 8); //mute

    in->dev     = ladev;
    in->standby = 1;
    in->device 	= devices & ~AUDIO_DEVICE_BIT_IN;

    *stream_in 	= &in->stream;
    return 0;

err:
    if (in->resampler)
        release_resampler(in->resampler);

    free(in);
    return ret;
}

static void adev_close_input_stream(struct audio_hw_device *dev,
                                   struct audio_stream_in *stream)
{
    struct sunxi_stream_in *in          = (struct sunxi_stream_in *)stream;
    struct sunxi_audio_device *ladev    = (struct sunxi_audio_device *)dev;

    in_standby(&stream->common);

    if (in->buffer) {
        free(in->buffer);
        in->buffer = 0;
    }

    if (in->resampler) {
        release_resampler(in->resampler);
    }

    if (ladev->PcmManager.BufStart) {
        ladev->PcmManager.BufExist = false;
        free(ladev->PcmManager.BufStart);
        ladev->PcmManager.BufStart = 0;
    }

    free(stream);

    ALOGD("adev_close_input_stream set voice record status");
    return;
}

static int adev_dump(const audio_hw_device_t *device, int fd)
{
    struct sunxi_audio_device *adev    = (struct sunxi_audio_device *)device;
    ALOGV("fun = %s, fd = %d", __FUNCTION__, fd);

    return 0;
}

static int adev_close(hw_device_t *device)
{
    free(device);
    return 0;
}

static int adev_open(const hw_module_t* module, const char* name,
                     hw_device_t** device)
{
    struct sunxi_audio_device *adev;
    int ret;

    if (strcmp(name, AUDIO_HARDWARE_INTERFACE) != 0)
        return -EINVAL;

    adev = calloc(1, sizeof(struct sunxi_audio_device));
    if (!adev)
        return -ENOMEM;

    adev->hw_device.common.tag              = HARDWARE_DEVICE_TAG;
    adev->hw_device.common.version          = AUDIO_DEVICE_API_VERSION_2_0;
    adev->hw_device.common.module           = (struct hw_module_t *) module;
    adev->hw_device.common.close            = adev_close;

    adev->hw_device.init_check              = adev_init_check;
    adev->hw_device.set_voice_volume        = adev_set_voice_volume;
    adev->hw_device.set_master_volume       = adev_set_master_volume;
    adev->hw_device.get_master_volume       = adev_get_master_volume;
    adev->hw_device.set_mode                = adev_set_mode;
    adev->hw_device.set_mic_mute            = adev_set_mic_mute;
    adev->hw_device.get_mic_mute            = adev_get_mic_mute;
    adev->hw_device.set_parameters          = adev_set_parameters;
    adev->hw_device.get_parameters          = adev_get_parameters;
    adev->hw_device.get_input_buffer_size   = adev_get_input_buffer_size;
    adev->hw_device.open_output_stream      = adev_open_output_stream;
    adev->hw_device.close_output_stream     = adev_close_output_stream;
    adev->hw_device.open_input_stream       = adev_open_input_stream;
    adev->hw_device.close_input_stream      = adev_close_input_stream;
    adev->hw_device.dump                    = adev_dump;

    /* Set the default route before the PCM stream is opened */
    pthread_mutex_lock(&adev->lock);

    adev->mode       = AUDIO_MODE_NORMAL;

    adev->card_id = init_audio_device_i2s(adev);
    if(adev->card_id < 0)
    {
        ALOGE("i2s audio device is not exist!");
        pthread_mutex_unlock(&adev->lock);
        goto error_out;
    }

    pthread_mutex_unlock(&adev->lock);

    *device = &adev->hw_device.common;

    ALOGD("adev_open success ,LINE:%d,FUNC:%s",__LINE__,__FUNCTION__);
    return 0;

error_out:
    free(adev);
    return -EINVAL;
}

static struct hw_module_methods_t hal_module_methods = {
    .open = adev_open,
};

struct audio_module HAL_MODULE_INFO_SYM = {
    .common = {
        .tag                = HARDWARE_MODULE_TAG,
        .module_api_version = AUDIO_MODULE_API_VERSION_0_1,
        .hal_api_version    = HARDWARE_HAL_API_VERSION,
        .id                 = AUDIO_HARDWARE_MODULE_ID,
        .name               = "sunxi audio HW HAL",
        .author             = "author",
        .methods            = &hal_module_methods,
    },
};
