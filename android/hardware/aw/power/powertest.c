#include<stdio.h>
#include<stdlib.h>
#include <cutils/log.h>
#include  "power.h"

static  void power_fwrite(const char *path, char *s)
{
    char buf[64];
    int len;
    int fd = open(path, O_WRONLY);
    if (fd < 0)
    {
        strerror_r(errno, buf, sizeof(buf));
        ALOGE("Error opening %s: %s\n", path, buf);
        return;
    }
    len = write(fd, s, strlen(s));
    if (len < 0)
    {
        strerror_r(errno, buf, sizeof(buf));
        ALOGE("Error writing to %s: %s\n", path, buf);
    }
    close(fd);
}

void  command_power_hint(power_hint_t hint, void *data)
{
    switch (hint)
    {
        case  BOOTCOMPLETE:
            ALOGI("++BOOTCOMPLETE MODE++");
            power_fwrite(CPU0LOCK,"1");
            power_fwrite(CPU0GOV,CPU_GOVERNOR);
            power_fwrite(CPUHOT,"1");
        #if defined A64
            power_fwrite(DRAMPAUSE,"0");
        #endif
            break;
        case  BENCHMARK:
            if(data==NULL)
                break;
            ALOGI("++BENCHMARK MODE++");
            char  *spid=(char *)data;
            power_fwrite(TASKS,spid);
            power_fwrite(ROOMAGE,roomage_little[BENCHMARK_INDEX]);

        #if defined A33 || A83T || A80
            power_fwrite(DRAMSCEN,DRAM_NORMAL);
        #endif

        #if defined A80
            if(BENCHMARK_INDEX==APP_TYPE_CPU_GPU|| BENCHMARK_INDEX==APP_TYPE_GPU)
                power_fwrite(GPUFREQ,GPU_PERF);
        #endif

        #if defined A83T || A33
            power_fwrite(DRAMSCEN,DRAM_NORMAL);
            if(BENCHMARK_INDEX==APP_TYPE_CPU_GPU|| BENCHMARK_INDEX==APP_TYPE_GPU)
                power_fwrite(GPUCOMMAND,"1");
        #endif
        #if defined A64
            power_fwrite(DRAMPAUSE, "1");
        #endif
            break;
        case NORMAL:
            ALOGI("++NORMAL MODE++");
            power_fwrite(ROOMAGE,ROOMAGE_NORMAL);
        #if defined A33 || A83T || A80
            power_fwrite(DRAMSCEN,DRAM_NORMAL);
        #endif

        #if defined A83T || A33
            power_fwrite(DRAMSCEN,DRAM_NORMAL);
            if(BENCHMARK_INDEX==APP_TYPE_CPU_GPU|| BENCHMARK_INDEX==APP_TYPE_GPU)
                power_fwrite(GPUCOMMAND,"0");
        #endif

        #if defined A80
            power_fwrite(GPUFREQ,GPU_NORMAL);
        #endif

        #if defined A64
            power_fwrite(TP_SUSPEND,"1");
            power_fwrite(DRAMPAUSE,"0");
        #endif
            break;
        case MUSIC:
            ALOGI("++MUSIC MODE++");
            power_fwrite(ROOMAGE, ROOMAGE_NORMAL);
        #if defined A33 || A83T || A80
            power_fwrite(DRAMSCEN,DRAM_BGMUSIC);
        #endif

        #if defined A80
            power_fwrite(GPUFREQ,GPU_NORMAL);
        #endif

        #if defined A83T || A33
            if(BENCHMARK_INDEX==APP_TYPE_CPU_GPU|| BENCHMARK_INDEX==APP_TYPE_GPU)
                power_fwrite(GPUCOMMAND,"0");
        #endif
        #if  defined A64
            power_fwrite(TP_SUSPEND,"0");
        #endif
            break;
        default:
            break;
    }
 }


int main(int argc, char **argv)
{
    char propval[100]={0};
    if(argc<2 )
    {
        printf("missing arguments\n");
        return 0;
    }
    if(!strcmp(argv[1],"resume"))
    {
        if(argc!= 2)
        {
            printf("Usage:resume");
            return -1;
        }
        printf("quit  debug mode!\n");
        property_set("sys.p_debug","false");
        property_set("sys.p_bootcomplete","true");
        property_set("sys.p_benchmark","true");
        property_set("sys.p_normal","true");
        property_set("sys.p_music","true");
        return 0;
    }

    if(!strcmp(argv[1],"enter"))
    {
        if(!strcmp("benchmark", argv[2]))
        {
            if(argc!=4)
            {
                printf("Usage:enter benchmark [pid0]\n");
                return -1;
            }
            property_get("sys.p_benchmark", propval,false);
            if(!strcmp(propval,"false"))
            {
                printf("benchmark mode has been disabled!\n");
                return -1;
            }
            property_set("sys.p_debug","true");
            command_power_hint(BENCHMARK, argv[3]);
            printf("benchmark succeed!\n");
        }

        else if(!strcmp("normal",argv[2]))
        {
            if(argc!=3)
            {
                printf("Usage:enter normal\n");
                return -1;
            }
            property_get("sys.p_normal",propval,false);
            if(!strcmp(propval,"false"))
            {
                printf("normal mode has been disabled!\n");
                return -1;
            }
            property_set("sys.p_debug","true");
            command_power_hint(NORMAL, "1");
            printf("normal succeed!\n");
        }
        else if(!strcmp("music",argv[2]))
        {
            if(argc!=3)
            {
                printf("Usage:enter music\n");
                return -1;
            }
            property_get("sys.p_music" , propval,false);
            if(!strcmp(propval,"false"))
            {
                printf("music mode has been disabled!\n");
                return -1;
            }
            property_set("sys.p_debug","true");
            command_power_hint(MUSIC, "2");
            printf("music succeed!\n");
        }

        else if(!strcmp("bootcomplete",argv[2]))
        {
            if(argc!=3)
            {
                printf("Usage:enter bootcomplete\n");
                return -1;
            }
            property_get("sys.p_bootcomplete",propval,false);
            if(!strcmp(propval,"false"))
            {
                printf("benchmark mode has been disabled!\n");
                return -1;
            }
            property_set("sys.p_debug","true");
            command_power_hint(BOOTCOMPLETE,"false");
            printf("bootcomplete succeed!\n");
        }
        else
        {
            printf("un-defined command:%s!\n", argv[1]);
            return -1;
        }
    }

    else if(!strcmp(argv[1],"enable"))
    {
        if(!strcmp("benchmark", argv[2]))
        {
            if(argc!=3)
            {
                printf("Usage:enable benchmark");
                return -1;
            }
            property_set("sys.p_benchmark","true");
            printf("enable benchmark succeed!\n");
        }
        else if(!strcmp("normal", argv[2]))
        {
            if(argc!=3)
            {
                printf("Usage:enable normal\n");
                return -1;
            }
            property_set("sys.p_normal","true");
            printf("enable normal  succeed!\n");
        }

        else if(!strcmp("music", argv[2]))
        {
            if(argc!=3)
            {
                printf("Usage:enable music\n");
                return -1;
            }
            property_set("sys.p_music","true");
            printf("enable music succeed!\n");
        }

        else  if(!strcmp("bootcomplete", argv[2]))
        {
            if(argc!=3)
            {
                printf("Usage:enable bootcomplete\n");
                return -1;
            }
            property_set("sys.p_bootcomplete","true");
            printf("enable bootcomplete succeed!\n");
        }
        else
        {
            printf("un-defined command:%s!\n",argv[1] );
            return -1;
        }
    }


    else if(!strcmp(argv[1],"disable"))
    {
        if(!strcmp("benchmark", argv[2]))
        {
            if(argc!=3)
            {
                printf("Usage:disable benchmark");
                return -1;
            }
            property_set("sys.p_benchmark","false");
            printf("disable benchmark succeed!\n");
        }
        else if(!strcmp("normal", argv[2]))
        {
            if(argc!=3)
            {
                printf("Usage:disable normal\n");
                return -1;
            }
            property_set("sys.p_normal","false");
            printf("disable normal succeed!\n");
        }
        else if(!strcmp("music", argv[2]))
        {
            if(argc!=3)
            {
                printf("Usage:disable music\n");
                return -1;
            }
            property_set("sys.p_music","false");
            printf("disable music succeed!\n");
        }

        else  if(!strcmp("bootcomplete", argv[2]))
        {
            if(argc!=3)
            {
                printf("Usage:disable bootcomplete\n");
                return -1;
            }
            property_set("sys.p_bootcomplete","false");
            printf("disable bootcomplete succeed!\n");
        }
        else
        {
            printf("un-defined command:%s\n", argv[1]);
            return -1;
        }
    }

    else
    {
        printf("un-defined command:%s\n",argv[1]);
        return -1;
    }
    return 0;
}

