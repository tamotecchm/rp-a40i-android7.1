/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define LOG_TAG "Legacy PowerHAL"
#include <utils/Log.h>

#include <hardware/hardware.h>
#include <hardware/power.h>
#include "power.h"

char propdebug[100]={0};

static void power_init(struct power_module *module)
{
}

static void power_set_interactive(struct power_module *module, int on)
{
}

static  void power_fwrite(const char *path, char *s)
{
    char buf[64];
    int len;
    int fd = open(path, O_WRONLY);
    if (fd < 0)
    {
        strerror_r(errno, buf, sizeof(buf));
        ALOGE("Error opening %s: %s\n", path, buf);
        return;
    }
    len = write(fd, s, strlen(s));
    if (len < 0)
    {
        strerror_r(errno, buf, sizeof(buf));
        ALOGE("Error writing to %s: %s\n", path, buf);
    }
    close(fd);
}


static void power_hint(struct power_module *module, power_hint_t hint,
                       void *data) {
    property_get("sys.p_debug",propdebug,NULL);

    if(!strcmp(propdebug,"true"))
    {
        ALOGI("ScenseControl debug mode,scense would not change automatically!");
        return ;
    }
    else if(!strcmp(propdebug,"false"))
    {
        switch (hint)
        {
            case BOOTCOMPLETE:
                property_get("sys.p_bootcomplete",propdebug,false);
                if(!strcmp(propdebug,"false"))
                {
                    ALOGI("BOOTCOMPLETE MODE disabled");
                    return ;
                }
                ALOGI("==BOOTCOMPLETE MODE==");
                power_fwrite(CPU0LOCK,"1");
                power_fwrite(CPU0GOV,CPU_GOVERNOR);
                power_fwrite(CPUHOT,"1");
            #if defined A64
                power_fwrite(DRAMPAUSE,"0");
            #endif
                break;
            case BENCHMARK:
                property_get("sys.p_benchmark",propdebug,false);
                if(!strcmp(propdebug,"false"))
                {
                    ALOGI("BENCHMARK MODE disabled");
                    return ;
                }
                if(data==NULL)
                {
                    break;
                }
                ALOGI("==BENCHMARK MODE==");
                int  ipid=*((int *)data);
                char  spid[20]= {0};
                sprintf(spid,"%d", ipid);
                power_fwrite(TASKS,spid);
                power_fwrite(ROOMAGE,roomage_little[BENCHMARK_INDEX]);
            #if defined A33 || A83T || A80
                power_fwrite(DRAMSCEN,DRAM_NORMAL);
            #endif

            #if defined A80
                if(BENCHMARK_INDEX==APP_TYPE_CPU_GPU|| BENCHMARK_INDEX==APP_TYPE_GPU)
                    power_fwrite(GPUFREQ,GPU_PERF);
            #endif

            #if defined A83T || A33
                power_fwrite(DRAMSCEN,DRAM_NORMAL);
                if(BENCHMARK_INDEX==APP_TYPE_CPU_GPU|| BENCHMARK_INDEX==APP_TYPE_GPU)
                    power_fwrite(GPUCOMMAND,"1");
            #endif
            #if defined A64
                power_fwrite(DRAMPAUSE, "1");
            #endif
                break;
            case NORMAL:
                property_get("sys.p_normal",propdebug,false);
                if(!strcmp(propdebug,"false"))
                {
                    ALOGI("NORMAL MODE disabled");
                    return ;
                }
                ALOGI("==NORMAL MODE==");
                power_fwrite(ROOMAGE,ROOMAGE_NORMAL);
            #if defined A33 || A83T || A80
                power_fwrite(DRAMSCEN,DRAM_NORMAL);
            #endif

            #if defined A80
                power_fwrite(GPUFREQ,GPU_NORMAL);
            #endif

            #if defined A83T || A33
                power_fwrite(DRAMSCEN,DRAM_NORMAL);
                if(BENCHMARK_INDEX==APP_TYPE_CPU_GPU|| BENCHMARK_INDEX==APP_TYPE_GPU)
                    power_fwrite(GPUCOMMAND,"0");
            #endif

            #if defined A64
                power_fwrite(TP_SUSPEND,"1");
                power_fwrite(DRAMPAUSE,"0");
            #endif
                break;
            case MUSIC:
                property_get("sys.p_music",propdebug,false);
                if(!strcmp(propdebug,"false"))
                {
                    ALOGI("MUSIC MODE disabled");
                    return ;
                }
                ALOGI("==MUSIC MODE==");
                power_fwrite(ROOMAGE, ROOMAGE_NORMAL);
            #if defined A33 || A83T || A80
                power_fwrite(DRAMSCEN,DRAM_BGMUSIC);
            #endif

            #if defined A80
                power_fwrite(GPUFREQ,GPU_NORMAL);
            #endif

            #if defined A83T || A33
                if(BENCHMARK_INDEX==APP_TYPE_CPU_GPU|| BENCHMARK_INDEX==APP_TYPE_GPU)
                    power_fwrite(GPUCOMMAND,"0");
            #endif

            #if defined A64
                power_fwrite(TP_SUSPEND,"0");
            #endif
                break;
            default:
                break;
           }
       }
}

static struct hw_module_methods_t power_module_methods = {
    .open = NULL,
};

struct power_module HAL_MODULE_INFO_SYM = {
    .common = {
        .tag = HARDWARE_MODULE_TAG,
        .module_api_version = POWER_MODULE_API_VERSION_0_2,
        .hal_api_version = HARDWARE_HAL_API_VERSION,
        .id = POWER_HARDWARE_MODULE_ID,
        .name = "Default Power HAL",
        .author = "The Android Open Source Project",
        .methods = &power_module_methods,
    },

    .init = power_init,
    .setInteractive = power_set_interactive,
    .powerHint = power_hint,
};
