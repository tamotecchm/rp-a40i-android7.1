/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.softwinner;

import java.io.IOException;
import java.lang.Integer;
import java.lang.String;

/**
 * Class that provides access to some of the gpio management functions.
 *
 */
public class AWDisplay {
    public static final String TAG = "awdisplay";
    public static final int DISP0 = 0;
    public static final int DISPLAY_CMD_SET_3DMODE = 0x01;    //取值范围 0,1,2,3,4,5,6
    public static final int DISPLAY_CMD_GET_3DMODE = 0x02;
    public static final int DISPLAY_CMD_SET_BACKLIGHT = 0x03; //取值范围 0,1,2
    public static final int DISPLAY_CMD_GET_BACKLIGHT = 0x04;
    public static final int DISPLAY_CMD_SET_ENHANCE = 0x05;   //取值范围0,1,2,3
    public static final int DISPLAY_CMD_GET_ENHANCE = 0x06;
    public static int s3DMode; // 3D模式
    public static int sIntelligentBacklight; // 智能背光
    public static int sSmartColor; // 丽色系统

    // can't instantiate this class
    private AWDisplay() {
    }

    static {
        System.loadLibrary("display_jni");
        nativeInit();
        s3DMode = dispCtrl(DISP0, DISPLAY_CMD_GET_3DMODE, 0, 0);
        sIntelligentBacklight = dispCtrl(DISP0, DISPLAY_CMD_GET_BACKLIGHT, 0, 0);
        sSmartColor = dispCtrl(DISP0, DISPLAY_CMD_GET_ENHANCE, 0, 0);
    }

    private native static void nativeInit();
    private native static int nativeDispCtrl(int disp, int cmd, int val0, int val1);

    public static int dispCtrl(int disp, int cmd, int val0, int val1) {
        return nativeDispCtrl(disp, cmd, val0, val1);
    }

    public static boolean getIntelligentBacklight() {
        return (sIntelligentBacklight & 0x01) != 0;
    }

    public static void setIntelligentBacklight(boolean on) {
        if (on) sIntelligentBacklight |= 0x01;
        else sIntelligentBacklight &= 0x02;
        dispCtrl(DISP0, DISPLAY_CMD_SET_BACKLIGHT, sIntelligentBacklight, 0);
    }

    public static boolean getIntelligentBacklightDemo() {
        return (sIntelligentBacklight & 0x02) != 0;
    }

    public static void setIntelligentBacklightDemo(boolean on) {
        if (on) sIntelligentBacklight |= 0x02;
        else sIntelligentBacklight &= 0x01;
        dispCtrl(DISP0, DISPLAY_CMD_SET_BACKLIGHT, sIntelligentBacklight, 0);
    }

    public static boolean getSmartColor() {
        return (sSmartColor & 0x01) != 0;
    }

    public static void setSmartColor(boolean on) {
        if (on) sSmartColor |= 0x01;
        else sSmartColor &= 0x02;
        dispCtrl(DISP0, DISPLAY_CMD_SET_ENHANCE, sSmartColor, 0);
    }

    public static boolean getSmartColorDemo() {
        return (sSmartColor & 0x02) != 0;
    }

    public static void setSmartColorDemo(boolean on) {
        if (on) sSmartColor |= 0x02;
        else sSmartColor &= 0x01;
        dispCtrl(DISP0, DISPLAY_CMD_SET_ENHANCE, sSmartColor, 0);
    }
}

