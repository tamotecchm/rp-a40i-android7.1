
#define LOG_TAG "com_softwinner_AWDisplay"
#define LOG_NDEBUG 0

#include "JNIHelp.h"
#include "jni.h"

#include "utils/Log.h"
#include "utils/misc.h"
#include "cutils/properties.h"
#include "android_runtime/AndroidRuntime.h"
#include "android_runtime/Log.h"

#include <string.h>
#include <pthread.h>

#include <sys/stat.h>
#include <fcntl.h>


#include <binder/IServiceManager.h>
#include <binder/IPCThreadState.h>
#include "../displayservice/IDisplayService.h"

using namespace android;

static sp<IDisplayService> displayService;

static void nativeInit(JNIEnv *, jobject)
{
	ALOGD("init display native..");
	sp<IServiceManager> sm = defaultServiceManager();
	sp<IBinder> binder;
	do{
		binder = sm->getService(String16("aw_display"));
		if (binder != 0) {
			break;
		}
		ALOGW("aw display service not published, waiting...");
		usleep(500000);
	} while(true);

	displayService = interface_cast<IDisplayService>(binder);
}

static void throw_NullPointerException(JNIEnv *env, const char* msg)
{
	jclass clazz;
	clazz = env->FindClass("java/lang/NullPointerException");
	env->ThrowNew(clazz, msg);
}

static jint nativeDispCtrl(JNIEnv *env, jobject, jint disp, jint cmd, jint val0, jint val1)
{
    int ret = 0;

	if(displayService == NULL){
		throw_NullPointerException(env, "display service has not start!");
	}

	ret = displayService->displayCtrl(disp, cmd, val0, val1);
	return ret;
}

static JNINativeMethod sMethods[] = {
    {"nativeInit", "()V", (void *) nativeInit},
    {"nativeDispCtrl", "(IIII)I", (void *) nativeDispCtrl},
};

jint JNI_OnLoad(JavaVM *jvm, void *reserved)
{
    JNIEnv *env;
    int status;

    ALOGV("AWPlayer : loading JNI\n");

    // Check JNI version
    if (jvm->GetEnv((void **)&env, JNI_VERSION_1_6)) {
        ALOGE("JNI version mismatch error");
        return JNI_ERR;
    }

    jniRegisterNativeMethods(env, "com/softwinner/AWDisplay", sMethods, NELEM(sMethods));
    return JNI_VERSION_1_6;
}
