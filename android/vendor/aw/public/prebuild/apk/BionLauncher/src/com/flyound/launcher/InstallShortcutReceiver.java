/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flyound.launcher;

import com.flyound.launcher.R;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.ContentResolver;
import android.database.Cursor;
import android.util.Log;
import static android.util.Log.d;
import android.widget.Toast;

public class InstallShortcutReceiver extends BroadcastReceiver {
	private static final String TAG = "InstallShortcutReceiver";
	private static final String ACTION_INSTALL_SHORTCUT = "com.android.launcher.action.INSTALL_SHORTCUT";

	private final int[] mCoordinates = new int[2];

	public void onReceive(Context context, Intent data) {
		if (LauncherModel.DEBUG_LOADERS)
			d(TAG, ACTION_INSTALL_SHORTCUT);

		if (data == null || !ACTION_INSTALL_SHORTCUT.equals(data.getAction())) {
			return;
		}

		Intent intent = data.getParcelableExtra(Intent.EXTRA_SHORTCUT_INTENT);
		String packageName = null;
		if (intent.getComponent() != null) {
			packageName = intent.getComponent().getPackageName();
		}
		String name = data.getStringExtra(Intent.EXTRA_SHORTCUT_NAME);
		if (LauncherModel.shortcutNeedFilter(packageName) || LauncherModel.shortcutExists(context, name, intent)) {
			return; // TODO
		}
		if (LauncherModel.DEBUG_LOADERS)
			d(LauncherModel.LOG_TAG, "Installed packageName:" + packageName);

		boolean bInstalled = false;
		LauncherSettings.FavoriteItem item = LauncherModel.queryFavoriteItem(
				context, packageName, intent.getComponent().getClassName());
		if (item != null) {
			bInstalled = installFavoriteItem(context, data, item);
		}

		if (!bInstalled) {
			int screen = Launcher.getScreen();
			if (!installShortcut(context, data, screen)) {
				// The target screen is full, let's try the other screens
				final int count = Launcher.getScreenCount(context);
				boolean bInstall = false;
				for (int i = 0; i < count; i++) {
					if (i != screen
							&& (bInstall = installShortcut(context, data, i)))
						break;
				}

				if (!bInstall) {
					Workspace workSpace = Launcher.getInstance().getWorkspace();
					int index = workSpace.getChildCount();
					workSpace.addScreen(index);
					installShortcut(context, data, index);
				}
			}
		}
	}

	private boolean installShortcut(Context context, Intent data, int screen) {
		String name = data.getStringExtra(Intent.EXTRA_SHORTCUT_NAME);
		Intent intent = data.getParcelableExtra(Intent.EXTRA_SHORTCUT_INTENT);

		if (findEmptyCellNotOccupyFavorite(context, intent.getComponent()
				.getPackageName(), intent.getComponent().getClassName(),
				mCoordinates, screen)) {
			installImpl(context, screen, data, name, intent);
			
			return true;
		} else {
		}

		return false;
	}

	private boolean installFavoriteItem(Context context, Intent data,
			LauncherSettings.FavoriteItem item) {
		final int xCount = Launcher.NUMBER_CELLS_X;
		final int yCount = Launcher.NUMBER_CELLS_Y;

		String name = data.getStringExtra(Intent.EXTRA_SHORTCUT_NAME);
		Intent intent = data.getParcelableExtra(Intent.EXTRA_SHORTCUT_INTENT);

		Workspace workSpace = Launcher.getInstance().getWorkspace();
		if (LauncherModel.DEBUG_LOADERS)
			d(TAG, "installFavoriteItem addScreen");
		for (int i = workSpace.getChildCount(); i < item.screen + 1; i++) {
			workSpace.addScreen(i);
		}

		boolean[][] occupied = new boolean[xCount][yCount];
		setOccupied(context, item.screen, occupied);
		if (CellLayout.isVacantCell(item.cellX, item.cellY, item.spanX,
				item.spanY, xCount, yCount, occupied)) {
			mCoordinates[0] = item.cellX;
			mCoordinates[1] = item.cellY;
			installImpl(context, item.screen, data, name, intent);

			return true;
		}

		return false;
	}

	private void installImpl(Context context, int screen, Intent data,
			String name, Intent intent) {
		CellLayout.CellInfo cell = new CellLayout.CellInfo();
		cell.cellX = mCoordinates[0];
		cell.cellY = mCoordinates[1];
		cell.screen = screen;

		if (intent.getAction() == null) {
			intent.setAction(Intent.ACTION_VIEW);
		}
		// add by sto
		intent.addCategory(Intent.CATEGORY_LAUNCHER);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);

		if (!LauncherModel.shortcutExists(context, name, intent)) {
			Launcher.addShortcut(context, data, cell, true);
			Toast.makeText(context,
					context.getString(R.string.shortcut_installed, name),
					Toast.LENGTH_SHORT).show();
		}
	}

	public static boolean findEmptyCell(Context context, int[] xy, int screen) {
		final int xCount = Launcher.NUMBER_CELLS_X;
		final int yCount = Launcher.NUMBER_CELLS_Y;

		boolean[][] occupied = new boolean[xCount][yCount];
		setOccupied(context, screen, occupied);

		return CellLayout.findVacantCell(xy, 1, 1, xCount, yCount, occupied);
	}

	public static boolean findEmptyCellNotOccupyFavorite(Context context,
			String packageName, String className, int[] xy, int screen) {
		final int xCount = Launcher.NUMBER_CELLS_X;
		final int yCount = Launcher.NUMBER_CELLS_Y;

		boolean[][] occupied = new boolean[xCount][yCount];

		LauncherModel.setFavoriteOccupied(context, packageName, className,
				screen, occupied);
		setOccupied(context, screen, occupied);
		LauncherModel.queryFavoriteItem(context, packageName, className);

		return CellLayout.findVacantCell(xy, 1, 1, xCount, yCount, occupied);
	}

	public static void setOccupied(Context context, int screen,
			boolean[][] occupied) {
		final int xCount = Launcher.NUMBER_CELLS_X;
		final int yCount = Launcher.NUMBER_CELLS_Y;

		final ContentResolver cr = context.getContentResolver();
		Cursor c = cr.query(LauncherSettings.Favorites.CONTENT_URI,
				new String[] { LauncherSettings.Favorites.CELLX,
						LauncherSettings.Favorites.CELLY,
						LauncherSettings.Favorites.SPANX,
						LauncherSettings.Favorites.SPANY },
				LauncherSettings.Favorites.SCREEN + "=?",
				new String[] { String.valueOf(screen) }, null);

		final int cellXIndex = c
				.getColumnIndexOrThrow(LauncherSettings.Favorites.CELLX);
		final int cellYIndex = c
				.getColumnIndexOrThrow(LauncherSettings.Favorites.CELLY);
		final int spanXIndex = c
				.getColumnIndexOrThrow(LauncherSettings.Favorites.SPANX);
		final int spanYIndex = c
				.getColumnIndexOrThrow(LauncherSettings.Favorites.SPANY);

		try {
			while (c.moveToNext()) {
				int cellX = c.getInt(cellXIndex);
				int cellY = c.getInt(cellYIndex);
				int spanX = c.getInt(spanXIndex);
				int spanY = c.getInt(spanYIndex);

				for (int x = cellX; x < cellX + spanX && x < xCount; x++) {
					for (int y = cellY; y < cellY + spanY && y < yCount; y++) {
						occupied[x][y] = true;
					}
				}
			}
		} catch (Exception e) {
		} finally {
			c.close();
		}
	}
}
