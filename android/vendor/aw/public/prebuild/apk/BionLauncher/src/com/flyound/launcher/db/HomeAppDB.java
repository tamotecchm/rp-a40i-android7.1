package com.flyound.launcher.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import com.flyound.launcher.bean.AppItem;

/**
 * @author zhongzhiwen
 * @date 2017/9/1
 * @mail zhongzhiwen24@gmail.com
 */

public class HomeAppDB{
    private static DBHelper mDBHelper;

    public static void insertAppInfo(Context context, AppItem item) {
        if (mDBHelper == null) {
            mDBHelper = new DBHelper(context);
        }
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBHelper.PACKAGE_NAME, item.pkg);
        values.put(DBHelper.ACTIVITY_NAME, item.cls);
        values.put(DBHelper.APP_NAME, item.title);
        db.insert(DBHelper.TABLE_NAME, null, values);
    }

    public static List<AppItem> queryAppInfos(Context context) {
        if (mDBHelper == null) {
            mDBHelper = new DBHelper(context);
        }
        List<AppItem> appInfos = new ArrayList<>();
        SQLiteDatabase db = mDBHelper.getReadableDatabase();
        String[] projection = {
                DBHelper.PACKAGE_NAME,
                DBHelper.ACTIVITY_NAME,
                DBHelper.APP_NAME
        };
        Cursor cursor = db.query(DBHelper.TABLE_NAME, projection,null, null, null, null, null);
        while (cursor.moveToNext()) {
            String pkg = cursor.getString(cursor.getColumnIndex(DBHelper.PACKAGE_NAME));
            String cls = cursor.getString(cursor.getColumnIndex(DBHelper.ACTIVITY_NAME));
            String title = cursor.getString(cursor.getColumnIndex(DBHelper.APP_NAME));
            AppItem item = new AppItem(pkg, cls, title);
            appInfos.add(item);
        }

        return appInfos;
    }

    public static void deleteAppInfo(Context context, AppItem item) {
        if (mDBHelper == null) {
            mDBHelper = new DBHelper(context);
        }

        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        String whereClause = DBHelper.PACKAGE_NAME + "=?" + " and " + 
                DBHelper.ACTIVITY_NAME + "=?" + " and " +
                DBHelper.APP_NAME + "=?";
        String[] whereArgs = new String[] {item.pkg, item.cls, item.title};
        db.delete(DBHelper.TABLE_NAME, whereClause, whereArgs);

    }

    public static boolean isAppInfoExist(Context context, AppItem item) {
        if (mDBHelper == null) {
            mDBHelper = new DBHelper(context);
        }

        SQLiteDatabase db = mDBHelper.getReadableDatabase();
        
        String sql = "SELECT * FROM " + DBHelper.TABLE_NAME + "WHERE " + 
                DBHelper.PACKAGE_NAME + "=? and" + 
                DBHelper.ACTIVITY_NAME + "=? and" + 
                DBHelper.APP_NAME + "=?";
        
        Cursor cursor = db.rawQuery(sql, new String[]{item.pkg, item.cls, item.title});
        if (cursor.getCount() == 0) {
            return false;
        } else {
            return true;
        }
    }

    public static class DBHelper extends SQLiteOpenHelper {
        private static final String TAG = "DBHelper";
        private static final String DATABASE_NAME = "bion.db";
        private static final int DATABASE_VERSION = 1;
        private static final String TABLE_NAME = "home_app";
        private static final String PACKAGE_NAME = "package_name";
        private static final String ACTIVITY_NAME = "activity_name";
        private static final String APP_NAME = "app_name";
        private static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        PACKAGE_NAME + " TEXT, " +
                        ACTIVITY_NAME + " TEXT, " +
                        APP_NAME + " TEXT);";

        public DBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }


        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_ENTRIES);
        }


        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}
