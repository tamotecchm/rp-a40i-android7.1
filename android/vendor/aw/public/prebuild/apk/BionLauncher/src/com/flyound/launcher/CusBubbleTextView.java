package com.flyound.launcher;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.util.AttributeSet;

public class CusBubbleTextView extends BubbleTextView {
	Drawable mForeground = null;
	
	public CusBubbleTextView(Context context) {
        super(context);
    }

    public CusBubbleTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CusBubbleTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    
    public void setIconForeground(Drawable d){
    	mForeground = d;
    }
    
    @Override
    public void draw(Canvas canvas) {
    	
        super.draw(canvas);
    }
}
