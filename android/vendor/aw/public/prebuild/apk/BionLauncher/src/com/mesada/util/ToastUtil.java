package com.mesada.util;

import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

public class ToastUtil {
	public static TextView tv;
	public static WindowManager mWindowManager;

	public static void showMessage(String msg, Context context) {
		showMessage(msg, context, Toast.LENGTH_SHORT);

	}

	public static void showMessage(String msg, Context context, int Length) {
		mWindowManager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		if (tv == null) {
			tv = new TextView(context);
			tv.setText(msg);
			tv.setTextSize(17);
			tv.setTextColor(0xffffffff);
			//tv.setBackgroundResource(R.drawable.toast_frame_holo);
			// tv.setPadding(0, 0, 0, 30);
		}

		if (tv.getParent() == null) {
			WindowManager.LayoutParams params = new WindowManager.LayoutParams();
			params.gravity = Gravity.BOTTOM;
			// params.alpha = 0.65f;
			params.x = 0;

			params.height = WindowManager.LayoutParams.WRAP_CONTENT;
			params.width = WindowManager.LayoutParams.WRAP_CONTENT;
			params.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
					| WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
					| WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
					| WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
			params.format = PixelFormat.TRANSLUCENT;
			params.windowAnimations = 0;
			mWindowManager.addView(tv, params);
			tv.setText(msg);
			handler.sendEmptyMessageDelayed(101, 5000);
		} else {
			tv.setText(msg);
			handler.removeMessages(101);
			handler.sendEmptyMessageDelayed(101, 5000);
		}
	}

	static Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if (tv.getParent() != null) {
				mWindowManager.removeView(tv);
			}
		}

	};

	public static void cancelCurrentToast() {
		if (tv != null && tv.getParent() != null) {
			mWindowManager.removeView(tv);
		}
	}
}
