package com.flyound.launcher;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.Toast;

public class DeleteZoneRearView extends DeleteZone {
	public DeleteZoneRearView(Context context) {
        super(context);
    }

    public DeleteZoneRearView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DeleteZoneRearView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    
    @Override
    public void onDrop(DragSource source, int x, int y, int xOffset, int yOffset, Object dragInfo) {
    	final ItemInfo item = (ItemInfo) dragInfo;

        if (item.container == -1 || item instanceof ApplicationInfo || item instanceof LauncherAppWidgetInfo) return;

        final LauncherModel model = Launcher.getModel();

        if (item.container == LauncherSettings.Favorites.CONTAINER_DESKTOP) {
            if (item instanceof LauncherAppWidgetInfo) {
                model.removeDesktopAppWidget((LauncherAppWidgetInfo) item);
            } else {
                model.removeDesktopItem(item);
            }
        } else {
            if (source instanceof UserFolder) {
                final UserFolder userFolder = (UserFolder) source;
                final UserFolderInfo userFolderInfo = (UserFolderInfo) userFolder.getInfo();
                model.removeUserFolderItem(userFolderInfo, item);
            }
        }
        if (item instanceof UserFolderInfo) {
            final UserFolderInfo userFolderInfo = (UserFolderInfo)item;
            LauncherModel.deleteUserFolderContentsFromDatabase(mLauncher, userFolderInfo);
            model.removeUserFolder(userFolderInfo);
        } else if (item instanceof LauncherAppWidgetInfo) {
            final LauncherAppWidgetInfo launcherAppWidgetInfo = (LauncherAppWidgetInfo) item;
            final LauncherAppWidgetHost appWidgetHost = mLauncher.getAppWidgetHost();
            mLauncher.getWorkspace().unbindWidgetScrollableId(launcherAppWidgetInfo.appWidgetId);
            if (appWidgetHost != null) {
                appWidgetHost.deleteAppWidgetId(launcherAppWidgetInfo.appWidgetId);
            }
        }
        
        LauncherModel.deleteItemFromDatabase(mLauncher, item);
    }
    
    @Override
    public void onDragStart(View v, DragSource source, Object info, int dragAction) {
    	mLauncher.fullScreenChange(true);
    	
    	super.onDragStart(v, source, info, dragAction);
    }
    
    @Override
    public void onDragEnd() {
    	super.onDragEnd();
    	
    	mLauncher.fullScreenChange(false);
    }
    
    @Override
    public void onDragEnter(DragSource source, int x, int y, int xOffset, int yOffset,
            Object dragInfo) {

    	//ADW: show uninstall message
    	final ItemInfo item = (ItemInfo) dragInfo;
        mTransition.reverseTransition(TRANSITION_DURATION);
    	if (item instanceof ApplicationInfo){
    		if (((ApplicationInfo) item).isSystemApp(getContext())) {
    			//Toast.makeText(mContext, R.string.drop_to_uninstall_systemapp_connot, 200).show();
    			Toast.makeText(mContext, "系统应用不能卸载", 200).show();
        		shouldUninstall = false;
    		} else {
//    			Toast.makeText(mContext, R.string.drop_to_uninstall, 200).show();
        		shouldUninstall = true;	
    		}
    	} else if (item instanceof LauncherAppWidgetInfo){
    		//Toast.makeText(mContext, R.string.drop_to_uninstall_systemtool_connot, 200).show();
    		Toast.makeText(mContext, "卸载应用程序", 200).show();
    		shouldUninstall = false;
    	}
    }
}
