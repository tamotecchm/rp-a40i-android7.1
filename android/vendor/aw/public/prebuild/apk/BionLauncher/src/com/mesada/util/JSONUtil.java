package com.mesada.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONUtil {
	public static JSONObject createJSONObject(String jsonStr){
		try {
			JSONObject obj = new JSONObject(jsonStr);
			return obj;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public static JSONArray getJSONArray(JSONObject jsonObj,String key){
		if(jsonObj != null && key != null){
			try {
				return jsonObj.getJSONArray(key);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public static JSONObject getJSONObject(JSONArray jsonArray, int index){
		if(jsonArray != null && index >=0 && index < jsonArray.length()){
			try {
				return jsonArray.getJSONObject(index);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
	public static int getInt(JSONObject jsonObj,String key ,int defValue){
		if(jsonObj != null){
			try {
				return jsonObj.getInt(key);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return defValue;
	}
	
	public static boolean getBoolean(JSONObject jsonObj,String key ,boolean defValue){
		if(jsonObj != null){
			try {
				return jsonObj.getBoolean(key);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return defValue;
	}
	
	public static String getString(JSONObject jsonObj,String key ,String defValue){
		if(jsonObj != null){
			try {
				return jsonObj.getString(key);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return defValue;
	}
	
}
