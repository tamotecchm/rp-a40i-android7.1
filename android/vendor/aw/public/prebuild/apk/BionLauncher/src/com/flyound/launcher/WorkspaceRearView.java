package com.flyound.launcher;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;

import com.flyound.launcher.ItemInfo;

public class WorkspaceRearView extends Workspace {

	public WorkspaceRearView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public WorkspaceRearView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public boolean acceptDrop(DragSource source, int x, int y, int xOffset,
			int yOffset, Object dragInfo) {
		
		ItemInfo itemInfo = (ItemInfo)dragInfo;
		// 图标拖动是交换位置的
		if (itemInfo != null && itemInfo.itemType == LauncherSettings.Favorites.ITEM_TYPE_APPLICATION ||
				itemInfo.itemType == LauncherSettings.Favorites.ITEM_TYPE_SHORTCUT){
			return true;
		}
		
		return super.acceptDrop(source, x, y, xOffset, yOffset, dragInfo);
	}
	
	public void onDropCompleted(View target, boolean success) {
		if (mDragInfo != null && mDragInfo.cell != null) {
			Object obj = ((ItemInfo)mDragInfo.cell.getTag());
			if (obj instanceof LauncherAppWidgetInfo)
				success = false;
			else 
				success = !(obj instanceof ApplicationInfo);
		}
		
		super.onDropCompleted(target, success);
	}
	
	public void onDrop(DragSource source, int x, int y, int xOffset,
			int yOffset, Object dragInfo) {

		final CellLayout cellLayout = getCurrentDropLayout();
		if (source != this) {
			onDropExternal(x - xOffset, y - yOffset, dragInfo, cellLayout);
		} else {
			// Move internally
			if (mDragInfo != null && mDragInfo.cell != null) {
				boolean moved = false;
				final View cell = mDragInfo.cell;
				int index = mScroller.isFinished() ? mCurrentScreen
						: mNextScreen;

				int itemType = ((ItemInfo) dragInfo).itemType;
				if (itemType != LauncherSettings.Favorites.ITEM_TYPE_APPLICATION
						&& itemType != LauncherSettings.Favorites.ITEM_TYPE_SHORTCUT) {

					if (index != mDragInfo.screen) {
						final CellLayout originalCellLayout = (CellLayout) getChildAt(mDragInfo.screen);
						originalCellLayout.removeView(cell);
						cellLayout.addView(cell);
						moved = true;
					}

					mTargetCell = estimateDropCell(x - xOffset, y - yOffset,
							mDragInfo.spanX, mDragInfo.spanY, cell, cellLayout,
							mTargetCell);

					cellLayout.onDropChild(cell, mTargetCell);

					if (mTargetCell[0] != mDragInfo.cellX
							|| mTargetCell[1] != mDragInfo.cellY)
						moved = true;
					final ItemInfo info = (ItemInfo) cell.getTag();
					if (moved) {
						CellLayout.LayoutParams lp = (CellLayout.LayoutParams) cell
								.getLayoutParams();
						LauncherModel.moveItemInDatabase(mLauncher, info,
								LauncherSettings.Favorites.CONTAINER_DESKTOP,
								index, lp.cellX, lp.cellY);
					}
				} else {
					final int[] originalCell = new int[2];

					mTargetCell = new int[2];
					cellLayout.pointToCellExact(x, y, mTargetCell);

					View replaceChild = cellLayout.getChildFromCell(mTargetCell[0], mTargetCell[1]);

					// 位置没有发生变化
					if (index == mDragInfo.screen
							&& (mDragInfo.cellX == mTargetCell[0] && mDragInfo.cellY == mTargetCell[1])) {
						return;
					}
					
					boolean[] occupiedCells = cellLayout.getOccupiedCells();
					if (null == replaceChild) {
						if (occupiedCells[mTargetCell[1] * cellLayout.getCountX()
								+ mTargetCell[0]])
							return;
					} else if (((ItemInfo) replaceChild.getTag()).itemType == LauncherSettings.Favorites.ITEM_TYPE_APPWIDGET) {
						return;
					}

					final CellLayout originalCellLayout = (CellLayout) getChildAt(mDragInfo.screen);
					
					originalCell[0] = mDragInfo.cellX;
					originalCell[1] = mDragInfo.cellY;
					
					int [] to = new int[2];
					originalCellLayout.cellToPoint(originalCell[0], originalCell[1], to);
					
					originalCellLayout.removeView(cell);
					
					cellLayout.addView(cell);

					if (replaceChild != null) {
						int []from = new int[2];
						from[0] = replaceChild.getLeft();
						from[1] = replaceChild.getTop();

						TranslateAnimation ta = null;

						// 同屏幕
						if (originalCellLayout == cellLayout) {
							ta = new TranslateAnimation(from[0] - to[0], 0, from[1] - to[1], 0);
							
							cellLayout.removeView(replaceChild);
							originalCellLayout.addView(replaceChild);
							originalCellLayout.onDropChild(replaceChild,
									originalCell);
						} else { 		// 非同屏
							final View fReplaceChild = replaceChild;

							int movex = 0;
							if (index > mDragInfo.screen) { // -1
								movex = -(from[0] + (getWidth() - to[0]));
							} else {
								movex = (getWidth() - from[0]) + to[0];
							}

							ta = new TranslateAnimation(0, movex, 0, to[1]
									- from[1]);
							ta.setAnimationListener(new AnimationListener() {
								@Override
								public void onAnimationStart(Animation animation) {
									// TODO Auto-generated method stub
								}

								@Override
								public void onAnimationRepeat(
										Animation animation) {
									// TODO Auto-generated method stub
								}

								@Override
								public void onAnimationEnd(Animation animation) {
									// TODO Auto-generated method stub
									post(new Runnable() {
										@Override
										public void run() {
											cellLayout
													.removeView(fReplaceChild);

											originalCellLayout
													.addView(fReplaceChild);
											originalCellLayout
													.onDropChild(fReplaceChild,
															originalCell);
										}
									});
								}
							});
						}
						
						if (null != ta) {
							ta.setInterpolator(new AccelerateDecelerateInterpolator());
							ta.setDuration(500);
							replaceChild.startAnimation(ta);
						}

						LauncherModel.moveItemInDatabase(mLauncher,
								(ItemInfo) replaceChild.getTag(),
								LauncherSettings.Favorites.CONTAINER_DESKTOP,
								mDragInfo.screen, originalCell[0], originalCell[1]);
					}

					cellLayout.onDropChild(cell, mTargetCell);

					CellLayout.LayoutParams lp = (CellLayout.LayoutParams) cell
							.getLayoutParams();
					LauncherModel.moveItemInDatabase(mLauncher,
							(ItemInfo) cell.getTag(),
							LauncherSettings.Favorites.CONTAINER_DESKTOP, index,
							lp.cellX, lp.cellY);
				}
			}
		}
	}
	
	@Override
	void startDrag(CellLayout.CellInfo cellInfo) {
			super.startDrag(cellInfo);
	}
}
