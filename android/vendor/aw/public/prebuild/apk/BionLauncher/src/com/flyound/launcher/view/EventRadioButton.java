package com.flyound.launcher.view;

import org.json.JSONArray;
import org.json.JSONObject;

import com.flyound.launcher.R;
import com.flyound.launcher.StateEventOfView;
import com.flyound.launcher.view.EventViewHelper.OnCheckStateChangeEventListener;
import com.flyound.launcher.view.EventViewHelper.OnClickStateEventListener;
import com.mesada.util.JSONUtil;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.RadioButton;

public class EventRadioButton extends RadioButton {
	OnClickStateEventListener onClickStateEventListener = null;
	OnCheckStateChangeEventListener onCheckStateChangeEventListener = null;
	private static final String TAG = EventRadioButton.class.getSimpleName();
	String eventDefineStr;

	public EventRadioButton(Context context) {
		super(context, null, 0);
		// TODO Auto-generated constructor stub
		paserAttribute(context, null, 0);
		init();
	}

	public EventRadioButton(Context context, AttributeSet attrs) {
		super(context, attrs, 0);
		// TODO Auto-generated constructor stub
		paserAttribute(context, attrs, 0);
		init();
	}

	public EventRadioButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		paserAttribute(context, attrs, defStyle);
		init();
	}

	private void paserAttribute(Context context, AttributeSet attrs,
			int defStyle) {
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.EventTextView, defStyle, 0);
		eventDefineStr = a.getString(R.styleable.EventTextView_eventDefine);
		Log.i(TAG, "eventDefineStr:" + eventDefineStr);
		a.recycle();

	}

	private void init() {

		if (eventDefineStr != null) {
			JSONObject eventJsonObj = JSONUtil.createJSONObject(eventDefineStr);
			if (eventJsonObj != null) {
				JSONArray stateEventsJsonArray = JSONUtil.getJSONArray(
						eventJsonObj, "stateEvents");
				if (stateEventsJsonArray != null) {
					for (int i = 0; i < stateEventsJsonArray.length(); i++) {
						StateEventOfView stateEvent = StateEventOfView
								.createFrom(JSONUtil.getJSONObject(
										stateEventsJsonArray, i));
						if (stateEvent.stateName.equals("onClick")) {
							onClickStateEventListener = EventViewHelper
									.getInstance().new OnClickStateEventListener(
									stateEvent);
							setOnClickListener(onClickStateEventListener);
							Log.i(TAG, "setOnClickListener:  stateName:"
									+ stateEvent.stateName + " eventDefine"
									+ eventDefineStr);
						} else if (stateEvent.stateName.equals("checked_true")) {
							if (onCheckStateChangeEventListener == null) {
								onCheckStateChangeEventListener = EventViewHelper
										.getInstance().new OnCheckStateChangeEventListener(
										null, null);
								setOnCheckedChangeListener(onCheckStateChangeEventListener);
							}
							onCheckStateChangeEventListener
									.setCheckedStateEvent(stateEvent);
							
							Log.i(TAG, "setOnCheckedChangeListener: stateName:"
									+ stateEvent.stateName + "  eventDefine"
									+ eventDefineStr);
						} else if (stateEvent.stateName.equals("checked_false")) {
							if (onCheckStateChangeEventListener == null) {
								onCheckStateChangeEventListener = EventViewHelper
										.getInstance().new OnCheckStateChangeEventListener(
										null, null);
								setOnCheckedChangeListener(onCheckStateChangeEventListener);
								
							}
							onCheckStateChangeEventListener
									.setUnCheckedStateEvent(stateEvent);
							
							Log.i(TAG, "setOnCheckedChangeListener: stateName:"
									+ stateEvent.stateName + " eventDefine"
									+ eventDefineStr);
						}
					}
				}
			}
		}

	}

	public OnClickStateEventListener getOnClickStateEventListener() {
		return onClickStateEventListener;
	}
	public OnCheckedChangeListener getOnCheckedChangeListener(){
		return onCheckStateChangeEventListener;
	}
}
