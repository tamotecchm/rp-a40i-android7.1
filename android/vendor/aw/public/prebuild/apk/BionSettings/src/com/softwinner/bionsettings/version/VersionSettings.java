package com.softwinner.bionsettings.version;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import com.softwinner.bionsettings.R;
import com.softwinner.bionsettings.util.Utils;

/**
 * @author Zhiwen, Zhong
 * @time 2017/5/18 7:17
 * @mail zhongzhiwen24@gmail.com
 */

public class VersionSettings extends PreferenceFragment{
    Preference androidPreference;
    Preference mcuPreference;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_version);
        setHasOptionsMenu(true);

        init();
    }

    private void init() {
        androidPreference = findPreference("android");
        mcuPreference = findPreference("mcu");

        androidPreference.setSummary(Utils.getSDKVersion());
        mcuPreference.setSummary("nothing");
    }
}
