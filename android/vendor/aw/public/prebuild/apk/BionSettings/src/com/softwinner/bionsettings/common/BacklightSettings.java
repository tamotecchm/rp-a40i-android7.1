package com.softwinner.bionsettings.common;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;
import android.provider.Settings;
import android.util.Log;

import com.softwinner.bionsettings.R;

/**
 * @author Zhiwen, Zhong
 * @time 2017/5/18 7:17
 * @mail zhongzhiwen24@gmail.com
 */

public class BacklightSettings extends PreferenceFragment{
    private static final String TAG = "BacklightSettings";
    private SwitchPreference adjustRightPreference;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_backlight);
        setHasOptionsMenu(true);

        init();
    }

    private void init() {
        adjustRightPreference = (SwitchPreference) findPreference("auto_adjust_right");

        int defBright = Settings.System.getInt(getActivity().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, 1);
        adjustRightPreference.setChecked(defBright == 0 ? false : true);
        adjustRightPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object value) {
                boolean checked = Boolean.getBoolean(value.toString());
                Log.w(TAG, "---------oh no--------" + value.toString());
                if (checked) { 
                    Settings.System.putInt(getActivity().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, 1);
                } else {
                    Log.w(TAG, "---------fuck you---------");
                    Settings.System.putInt(getActivity().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, 0);
                }

                return true;
            }
        });

    }

}
