package com.softwinner.bionsettings.widget;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;

// import com.softwinner.bionsettings.common.DateTimeSettings;
import com.softwinner.bionsettings.common.DialogCreatable;

/**
 * @author Zhiwen, Zhong
 * @time 2017/5/18 12:22
 * @mail zhongzhiwen24@gmail.com
 */

public class SettingsDialogFragment extends DialogFragment {
    private static final String TAG = "SettingsDialogFragment";
    private static final String KEY_DIALOG_ID = "key_dialog_id";
    private static final String KEY_PARENT_FRAGMENT_ID = "key_parent_fragment_id";

    private int dialogId;

    private Fragment parentFragment;

    public SettingsDialogFragment() {}

    public static SettingsDialogFragment newInstance(DialogCreatable fragment, int dialogId) {
        Log.d(TAG, "------------SettingsDialogFragment newInstance-------------");

        SettingsDialogFragment instance = new SettingsDialogFragment();
        instance.dialogId = dialogId;
        if (!(fragment instanceof Fragment)) {
            throw new IllegalArgumentException("fragment argument must be an instance of "
                    + Fragment.class.getName());
        }
        instance.parentFragment = (Fragment) fragment;

        return instance;
    }

    public int getDialogId() {
        return dialogId;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (parentFragment != null) {
            outState.putInt(KEY_DIALOG_ID, dialogId);
            outState.putInt(KEY_PARENT_FRAGMENT_ID, parentFragment.getId());
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Log.d(TAG, "----------------SettingsDialogFragment onCreateDialog-----------");
        if (savedInstanceState != null) {
            dialogId = savedInstanceState.getInt(KEY_DIALOG_ID, 0);
            parentFragment = getParentFragment();
            int parentFragmentId = savedInstanceState.getInt(KEY_PARENT_FRAGMENT_ID, -1);
            if (parentFragment == null) {
                parentFragment = getFragmentManager().findFragmentById(parentFragmentId);
            }
            if (!(parentFragment instanceof DialogCreatable)) {
                throw new IllegalArgumentException(
                        (parentFragment != null
                                ? parentFragment.getClass().getName()
                                : parentFragment)
                                + " must implement "
                                + DialogCreatable.class.getName());
            }
            // if (parentFragment instanceof DateTimeSettings) {
            //     // restore dialogFragment in parentFragment
            //     ((DateTimeSettings) parentFragment).setSettingsDialogFragment(this);
            // }
        }

        return ((DialogCreatable) parentFragment).onCreateDialog(dialogId);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // if (parentFragment instanceof DateTimeSettings) {
        //     // in case the dialog is not explicitly removed by removeDialog()
        //     if (((DateTimeSettings) parentFragment).getSettingsDialogFragment() == this) {
        //         ((DateTimeSettings) parentFragment).setSettingsDialogFragment(null);
        //     }
        // }
    }
}
