package com.softwinner.bionsettings.driving;


import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.provider.Settings;
import android.preference.SwitchPreference;
import android.util.Log;

import com.softwinner.bionsettings.R;

/**
 * @author Zhiwen, Zhong
 * @time 2017/5/18 7:17
 * @mail zhongzhiwen24@gmail.com
 */

public class DrivingSettings extends PreferenceFragment{
    private static final String TAG = "DrivingSettings";
    // SwitchPreference revPreference;  // 倒车启动
    // SwitchPreference refeLinePreference; // 参考线
    SwitchPreference revSilencePreference;  // 倒车静音
    SwitchPreference mirrPreference;   //镜像
    SwitchPreference noVideoPreference; // 开车静止视频


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_driving);
        setHasOptionsMenu(true);

        init();
    }

    public void init() {
        // revPreference = (SwitchPreference) findPreference("driving_reverse");
        // refeLinePreference = (SwitchPreference) findPreference("driving_reference_line");
        revSilencePreference = (SwitchPreference) findPreference("driving_reverse_silence");
        mirrPreference = (SwitchPreference) findPreference("driving_mirror");
        noVideoPreference = (SwitchPreference) findPreference("driving_no_video");

        // //TODO 就差改变SettingsProvider
        // //倒车启动
        // int defRev = Settings.System.getInt(getActivity().getContentResolver(), Settings.System.DRIVING_REVERSE, 0);
        // revPreference.setChecked(defRev == 0 ? false : true);
        // revPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
        //     @Override
        //     public boolean onPreferenceChange(Preference preference, Object value) {
        //         boolean checked = Boolean.getBoolean(value.toString());
        //         Log.w(TAG, "---------oh no--------" + value.toString());
        //         Log.w(TAG, "---------oh no---checked-----" + checked);
        //         if (checked) {
        //             Settings.System.putInt(getActivity().getContentResolver(), Settings.System.DRIVING_REVERSE, 1);
        //         } else {
        //             Log.w(TAG, "---------fuck you---------");
        //             Settings.System.putInt(getActivity().getContentResolver(), Settings.System.DRIVING_REVERSE, 0);
        //         }

        //         return true;
        //     }
        // });

        // 静音
        int defSilence = Settings.System.getInt(getActivity().getContentResolver(), Settings.System.DRIVING_REVERSE_SILENCE, 0);
        revSilencePreference.setChecked(defSilence == 0 ? false : true);
        revSilencePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object value) {
                boolean checked = (Boolean) value;
                Log.w(TAG, "---------oh no--------" + value.toString() +"wen");
                Log.w(TAG, "---------oh no----checked----" + checked);
                if (checked) {
                    Settings.System.putInt(getActivity().getContentResolver(), Settings.System.DRIVING_REVERSE_SILENCE, 1);
                } else {
                    Log.w(TAG, "---------fuck you---------");
                    Settings.System.putInt(getActivity().getContentResolver(), Settings.System.DRIVING_REVERSE_SILENCE, 0);
                }

                return true;
            }
        });

        // 镜像
        int defMirr = Settings.System.getInt(getActivity().getContentResolver(), Settings.System.DRIVING_MIRROR, 0);
        mirrPreference.setChecked(defMirr == 0 ? false : true);
        mirrPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object value) {
                boolean checked = (Boolean) value;
                Log.w(TAG, "---------oh no--------" + value.toString());
                if (checked) {
                    Settings.System.putInt(getActivity().getContentResolver(), Settings.System.DRIVING_MIRROR, 1);
                } else {
                    Log.w(TAG, "---------fuck you---------");
                    Settings.System.putInt(getActivity().getContentResolver(), Settings.System.DRIVING_MIRROR, 0);
                }

                return true;
            }
        });

        int defNoVideo = Settings.System.getInt(getActivity().getContentResolver(), Settings.System.DRIVING_NO_VIDEO, 0);
        noVideoPreference.setChecked(defNoVideo == 0 ? false : true);
        noVideoPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object value) {
                boolean checked = (Boolean) value;
                Log.w(TAG, "---------oh no--------" + value.toString());
                if (checked) {
                    Settings.System.putInt(getActivity().getContentResolver(), Settings.System.DRIVING_NO_VIDEO, 1);
                } else {
                    Log.w(TAG, "---------fuck you---------");
                    Settings.System.putInt(getActivity().getContentResolver(), Settings.System.DRIVING_NO_VIDEO, 0);
                }

                return true;
            }
        });
    }
}
