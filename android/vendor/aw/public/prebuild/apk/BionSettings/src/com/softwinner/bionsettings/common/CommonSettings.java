package com.softwinner.bionsettings.common;

import android.content.Context;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import com.softwinner.bionsettings.R;

import java.util.List;
import java.util.Locale;

/**
 * @author Zhiwen, Zhong
 * @time 2017/5/18 7:17
 * @mail zhongzhiwen24@gmail.com
 */

public class CommonSettings extends PreferenceFragment{
    Preference languagePref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_common);
        setHasOptionsMenu(true);

        init();
    }

    private void init() {
        languagePref = findPreference("language");
    }

    @Override
    public void onResume() {
        super.onResume();

        if (languagePref != null) {
            languagePref.setSummary(getLocaleName(getActivity()));
        }
    }

    private static String getLocaleName(Context context) {
        // We want to show the same string that the LocalePicker used.
        // TODO: should this method be in LocalePicker instead?
        Locale currentLocale = context.getResources().getConfiguration().locale;
        List<LocalePicker.LocaleInfo> locales = LocalePicker.getAllAssetLocales(context, true);
        for (LocalePicker.LocaleInfo locale : locales) {
            if (locale.getLocale().equals(currentLocale)) {
                return locale.getLabel();
            }
        }
        // This can't happen as long as the locale was one set by Settings.
        // Fall back in case a developer is testing an unsupported locale.
        return currentLocale.getDisplayName(currentLocale);
    }
}
