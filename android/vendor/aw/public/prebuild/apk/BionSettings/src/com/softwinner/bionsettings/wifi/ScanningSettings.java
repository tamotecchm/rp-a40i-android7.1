package com.softwinner.bionsettings.wifi;


import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.provider.Settings.Global;

import com.android.internal.logging.MetricsLogger;
import com.softwinner.bionsettings.R;


/**
 * A page that configures the background scanning settings for Wi-Fi and Bluetooth.
 */
public class ScanningSettings extends PreferenceFragment {
    private static final String KEY_WIFI_SCAN_ALWAYS_AVAILABLE = "wifi_always_scanning";
    private static final String KEY_BLUETOOTH_SCAN_ALWAYS_AVAILABLE = "bluetooth_always_scanning";


    // protected int getMetricsCategory() {
    //     return MetricsLogger.LOCATION_SCANNING;
    // }

    @Override
    public void onResume() {
        super.onResume();
        createPreferenceHierarchy();
    }

    private PreferenceScreen createPreferenceHierarchy() {
        PreferenceScreen root = getPreferenceScreen();
        if (root != null) {
            root.removeAll();
        }
        addPreferencesFromResource(R.xml.location_scanning);
        root = getPreferenceScreen();
        initPreferences();
        return root;
    }

    private void initPreferences() {
        final SwitchPreference wifiScanAlwaysAvailable =
                (SwitchPreference) findPreference(KEY_WIFI_SCAN_ALWAYS_AVAILABLE);
        wifiScanAlwaysAvailable.setChecked(Global.getInt(getActivity().getContentResolver(),
                Global.WIFI_SCAN_ALWAYS_AVAILABLE, 0) == 1);
        final SwitchPreference bleScanAlwaysAvailable =
                (SwitchPreference) findPreference(KEY_BLUETOOTH_SCAN_ALWAYS_AVAILABLE);
        bleScanAlwaysAvailable.setChecked(Global.getInt(getActivity().getContentResolver(),
                Global.BLE_SCAN_ALWAYS_AVAILABLE, 0) == 1);
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen screen, Preference preference) {
        String key = preference.getKey();
        if (KEY_WIFI_SCAN_ALWAYS_AVAILABLE.equals(key)) {
            Global.putInt(getActivity().getContentResolver(),
                    Global.WIFI_SCAN_ALWAYS_AVAILABLE,
                    ((SwitchPreference) preference).isChecked() ? 1 : 0);
        } else if (KEY_BLUETOOTH_SCAN_ALWAYS_AVAILABLE.equals(key)) {
            Global.putInt(getActivity().getContentResolver(),
                    Global.BLE_SCAN_ALWAYS_AVAILABLE,
                    ((SwitchPreference) preference).isChecked() ? 1 : 0);
        } else {
            return super.onPreferenceTreeClick(screen, preference);
        }
        return true;
    }
}

