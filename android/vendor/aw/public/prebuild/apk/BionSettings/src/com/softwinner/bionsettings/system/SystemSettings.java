package com.softwinner.bionsettings.system;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import com.softwinner.bionsettings.R;

/**
 * @author Zhiwen, Zhong
 * @time 2017/5/18 7:17
 * @mail zhongzhiwen24@gmail.com
 */

public class SystemSettings extends PreferenceFragment{
    Preference rebootPreference;
    Preference logoPreference;
    Preference upgradePreference;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_system);
        setHasOptionsMenu(true);

        init();
    }

    private void init() {
        rebootPreference = findPreference("reboot");
        logoPreference = findPreference("logo");
        upgradePreference = findPreference("upgrade");

        rebootPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("确定要重启？");
                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent=new Intent(Intent.ACTION_REBOOT);
                        intent.putExtra("nowait", 1);
                        intent.putExtra("interval", 1);
                        intent.putExtra("window", 0);
                        getActivity().sendBroadcast(intent);
                    }
                });

                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();

                return true;
            }
        });

        upgradePreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = getActivity().getPackageManager().getLaunchIntentForPackage("com.softwinner.update");
                startActivity(intent);
                return true;
            }
        });
    }
}
