package com.softwinner.bionsettings.navi;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;
import android.provider.Settings;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.softwinner.bionsettings.R;
import com.softwinner.bionsettings.util.Utils;
import com.softwinner.bionsettings.view.BionSeekBarPreference;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Zhiwen, Zhong
 * @time 2017/5/18 7:17
 * @mail zhongzhiwen24@gmail.com
 */

public class NaviSettings extends PreferenceFragment{
    private static final String TAG = "NaviSettings";
    private ListPreference naviListPreference;
    private CharSequence[] naviNameList;
    private CharSequence[] naviValueList;
    private SwitchPreference naviAudioMix;
    private BionSeekBarPreference naviAudioMixPercent;
    private List<String> naviData;
    // 默认导航软件，-1表示没有选择默认导航软件，0表示高德，1表示百度，2表示腾讯，3表示谷歌
    private int defNavi; 


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_navi);
        setHasOptionsMenu(true);

        init();
    }

    private void init() {
        naviListPreference = (ListPreference) findPreference("navi_list");
        defNavi = Settings.System.getInt(getActivity().getContentResolver(), Settings.System.DEF_NAVI, -1);
        Log.d("zhongzhiwen", "--------def navigation------" + defNavi);
        Log.d(TAG, "----getNaviData---  " + getNaviData());
        if (getNaviData() > 0) {
            if (defNavi < 0) {
                // 还未选择默认导航软件
                naviListPreference.setSummary("您还未选择默认导航软件！");
            } else {
                if (naviData.get(defNavi) != null) {
                    naviListPreference.setSummary(naviNameList[defNavi]);
                } else {
                    Settings.System.putInt(getActivity().getContentResolver(), Settings.System.DEF_NAVI, -1);
                    // 说明先前选择的默认导航软件已经被删除，更新数据库并再次检查
                    if (getNaviData() < 0) {
                        naviListPreference.setSummary("您还未安装任何导航软件！");
                        naviListPreference.setEnabled(false);
                    } else {
                        // 还未选择默认导航软件
                        naviListPreference.setSummary("您还未选择默认导航软件！");
                    }
                }
            }
            naviListPreference.setSummary(naviNameList[0]);
            naviListPreference.setEntries(naviNameList);
            naviListPreference.setEntryValues(naviValueList);
        } else {
            naviListPreference.setSummary("您还未安装任何导航软件！");
            naviListPreference.setEnabled(false);
        }
        
        naviAudioMix = (SwitchPreference) findPreference("navi_audio_mix");
        int defAudioMix = Settings.System.getInt(getActivity().getContentResolver(), 
            Settings.System.NAVI_AUDIO_MIX, 0);
        naviAudioMix.setChecked(defAudioMix == 0 ? false : true);
        
        naviAudioMix.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object value) {
                boolean checked = (Boolean) value;
                Log.w(TAG, "---------oh no--------" + value.toString());
                if (checked) { 
                    Settings.System.putInt(getActivity().getContentResolver(), Settings.System.NAVI_AUDIO_MIX, 1);
                } else {
                    Log.w(TAG, "---------fuck you---------");
                    Settings.System.putInt(getActivity().getContentResolver(), Settings.System.NAVI_AUDIO_MIX, 0);
                }

                return true;
            }
        });

        // TODO
        naviAudioMixPercent = (BionSeekBarPreference) findPreference("navi_audio_mix_percent");
        int precent = Settings.System.getInt(getActivity().getContentResolver(), Settings.System.NAVI_AUDIO_MIX_PERCENT, 5);
        naviAudioMixPercent.setProgress(precent);
        naviAudioMixPercent.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Settings.System.putInt(getActivity().getContentResolver(), Settings.System.NAVI_AUDIO_MIX_PERCENT, progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        // TODO 每次先检查有没有安装导航软件，如果么有立即更新数据库，然后再设置
    }

    /**
     * 获取第三方导航软件的信息
     * @return 返回安装的导航软件数目，如果未安装任何导航软件，那么返回-1
     */
    private int getNaviData() {
        naviData = Utils.getNaviData(getActivity());
        if (naviData == null) {
            Log.d(TAG, "----navi data----");
        } else {
            Log.d(TAG, "----navi data size----" + naviData.size());
        }
        naviNameList = new CharSequence[naviData.size()];
        naviValueList = new CharSequence[naviData.size()];
        int index = 0;
        for (String navi : naviData) {
            Log.d(TAG, "-----------navi-----------" + navi);
            if (navi != null) {
                Log.d(TAG, "-----------navi--------fucking shit---" );
                naviValueList[index] = navi;
                if (navi.equals("amap")) {
                    naviNameList[index++] = "高德地图";
                    index++;
                } else if(navi.equals("baidu")) {
                    naviNameList[index++] = "百度地图";
                    index++;
                } else if(navi.equals("tencent")) {
                    naviNameList[index++] = "腾讯地图";
                    index++;
                } else if(navi.equals("google")) {
                    naviNameList[index++] = "谷歌地图";
                    index++;
                }
            } else {
                Log.d(TAG, "-----------navi--------fucking shit null---" );
            }
        }

        return index == 0 ? -1 : index;
    }
}
