package com.softwinner.bionsettings.wifi;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.Button;

/**
 * Foundation interface glues between Activities and UIs like {@link WifiDialog}.
 */
public interface WifiConfigUiBase {
    public Context getContext();
    public WifiConfigController getController();
    public LayoutInflater getLayoutInflater();
    public boolean isEdit();

    public void setTitle(int id);
    public void setTitle(CharSequence title);

    public void setSubmitButton(CharSequence text);
    public void setForgetButton(CharSequence text);
    public void setCancelButton(CharSequence text);
    public Button getSubmitButton();
    public Button getForgetButton();
    public Button getCancelButton();
}