LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_STATIC_JAVA_LIBRARIES := android-support-v7-appcompat  

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-java-files-under, src) 

LOCAL_RESOURCE_DIR := $(LOCAL_PATH)/res \
                      frameworks/support/v7/appcompat/res 
                      
# LOCAL_RESOURCE_DIR += prebuilts/sdk/current/support/v7/appcompat/res
# LOCAL_RESOURCE_DIR += frameworks/base/core/res/res

LOCAL_PACKAGE_NAME := BionSettings

LOCAL_PRIVILEGED_MODULE := true

LOCAL_CERTIFICATE := platform

# LOCAL_PROGUARD_FLAG_FILES := proguard.flags

# include frameworks/opt/setupwizard/navigationbar/common.mk
# include frameworks/opt/setupwizard/library/common.mk
include frameworks/base/packages/SettingsLib/common.mk

include $(BUILD_PACKAGE)

# Use the folloing include to make our test apk.
#include $(call all-makefiles-under,$(LOCAL_PATH))

