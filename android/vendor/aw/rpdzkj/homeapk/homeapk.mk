CUR_PATH := vendor/aw/rpdzkj/homeapk/

PRODUCT_COPY_FILES += vendor/aw/rpdzkj/homeapk/homeapk.sh:system/bin/homeapk.sh
PRODUCT_COPY_FILES += vendor/aw/rpdzkj/homeapk/nohomeapk.sh:system/bin/nohomeapk.sh
modeswitch_files := $(shell ls $(CUR_PATH)/Launcher3)
PRODUCT_COPY_FILES += \
    $(foreach file, $(modeswitch_files), \
    $(CUR_PATH)/Launcher3/$(file):system/usr/homeapk/Launcher3/$(file))
