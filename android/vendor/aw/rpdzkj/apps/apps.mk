
CUR_PATH := vendor/aw/rpdzkj/apps

PRODUCT_COPY_FILES += vendor/aw/rpdzkj/apps/preinstall.sh:system/bin/preinstall.sh
modeswitch_files := $(shell ls $(CUR_PATH)/preinstall)
PRODUCT_COPY_FILES += \
    $(foreach file, $(modeswitch_files), \
    $(CUR_PATH)/preinstall/$(file):system/usr/preinstall/$(file))

