package android.aw;

import android.app.Activity;
import android.app.ActivityManagerNative;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.UserHandle;
import android.os.RemoteException;
import android.util.Log;

import java.util.List;

/**
 * @hide
 */
public final class BackgroundManager {
    private static final String TAG = "BackgroundManager";
    private static final boolean DEBUG = true;

    public static final String SERVICE = "background";

    private final IBackgroundManager mService;
    private final Context mContext;

    private static BackgroundManager sInstance = null;
    /** @hide */
    public synchronized static BackgroundManager get(Context context) {
        if (sInstance == null) {
            sInstance = (BackgroundManager) context.getSystemService(SERVICE);
        }
        return sInstance;
    }

    /** @hide */
    public BackgroundManager(Context context, IBackgroundManager service) {
        mService = service;
        mContext = context;
    }

    /**
     * set background music playing
     * @param key every player has different key
     * @param pid player pid
     * @param playing background music playing
     */
    public static void setBgmusic(String key, int pid, boolean playing) {
        Intent intent = new Intent("com.android.bgmusic");
        intent.putExtra("key", key);
        intent.putExtra("pid", pid);
        intent.putExtra("playing", playing);
        int appOp = 0;
        int userId = UserHandle.USER_CURRENT_OR_SELF;
        try {
            ActivityManagerNative.getDefault().broadcastIntent(
                null, intent, null, null, Activity.RESULT_OK, null, null,
                null /*permission*/, appOp, null, false, false, userId);
        } catch (RemoteException ex) {
            Log.w(TAG, "setBgmusic " + pid + " error", ex);
        }
    }
    /**
     * check if allow background task
     * including whitelist and user custom settings
     * @param packageName package name
     */
    public boolean allowBackgroundTask(String packageName) {
        try {
            return mService.allowBackgroundTask(packageName);
        } catch (RemoteException ex) {
            Log.w(TAG, "allowBackgroundTask " + packageName + " error", ex);
            return true;
        }
    }

    /**
     * kill background task immediately
     */
    public void killBackgroundTaskImmediately() {
        try {
            mService.killBackgroundTaskImmediately();
        } catch (RemoteException ex) {
            Log.w(TAG, "killBackgroundTaskImmediately error", ex);
        }
    }

    /**
     * manager package and auto kill when unuse in background
     * @param packageName Package to manager
     * @param timestamp Timestamp of switching into background, and foreground is -1
     */
    public void managerPackage(String packageName, long timestamp) {
        try {
            mService.managerPackage(packageName, timestamp);
        } catch (RemoteException ex) {
            Log.w(TAG, "managerPackage " + packageName + " error", ex);
        }
    }

    /**
     * resolver receivers and remove not in whitelist
     * @param Intent target intent
     * @param receivers resolver receivers
     */
    public void resolverReceiver(Intent intent, List<ResolveInfo> receivers) {
        try {
            mService.resolverReceiver(intent, receivers);
        } catch (RemoteException ex) {
            Log.w(TAG, "resolverReceiver " + intent + " error", ex);
        }
    }

    /**
     * resolver receivers and remove not in whitelist
     * @param Intent target intent
     * @param receivers resolver receivers
     */
    public boolean skipService(Intent service) {
        try {
            return mService.skipService(service);
        } catch (RemoteException ex) {
            Log.w(TAG, "skipService " + service + " error", ex);
            return false;
        }
    }

}
