package android.aw;

import android.content.Intent;
import android.content.pm.ResolveInfo;

import java.util.List;

/**
 * System private interface to the background manager.
 *
 * {@hide}
 */
interface IBackgroundManager
{
    /**
     * check if allow background task
     * including whitelist and user custom settings
     * @param packageName package name
     */
    boolean allowBackgroundTask(String packageName);

    /**
     * kill background task immediately
     */
    void killBackgroundTaskImmediately();

    /**
     * manager package and auto kill when unuse in background
     * @param packageName Package to manager
     * @param timestamp Timestamp of switching into background, and foreground is -1
     */
    void managerPackage(String packageName, long timestamp);

    /**
     * resolver receivers and remove not in whitelist
     * @param Intent target intent
     * @param receivers resolver receivers
     */
    void resolverReceiver(in Intent intent, inout List<ResolveInfo> receivers);

    /**
     * resolver receivers and remove not in whitelist
     * @param Intent target intent
     * @param receivers resolver receivers
     */
    boolean skipService(in Intent service);
}
