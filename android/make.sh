#!/bin/bash

# changed to relative path, when move top dir to other path, link not works
#LICHEE_DIR=${ROOT_DIR}/../lichee
#UBOOT_DIR=${LICHEE_DIR}/brandy/u-boot-2014.07
#KERNEL_DIR=${LICHEE_DIR}/linux-3.10
#RESULT_DIR=${ROOT_DIR}/result

ROOT_DIR=`pwd`
LICHEE_DIR=../lichee
UBOOT_DIR=${LICHEE_DIR}/brandy/u-boot-2014.07
KERNEL_DIR=${LICHEE_DIR}/linux-3.10
RESULT_DIR=${ROOT_DIR}/result
CPU_NUM=`grep -c processor /proc/cpuinfo`
BSP_NAME=
DATE=`date +%Y%m%d`

function build_uboot(){
	echo "=============== build uboot ======================"
	cd $ROOT_DIR
	if [ ! -L uboot ] && [ -d ${UBOOT_DIR} ]; then
		ln -s ${UBOOT_DIR} uboot
		echo "uboot link to lichee/brandy/u-boot-2014.07"
	elif [ ! -L uboot ] && [ ! -d ${UBOOT_DIR} ]; then
		echo "can not find uboot source code, failed to build uboot"
		exit 1
	fi

	cd ${UBOOT_DIR}
	echo "=======path: `pwd`========="
	make distclean && make sun8iw11p1_config && make -j8	# compile uboot
	make spl	# compile boot0
	
	if [ $? != 0 ]; then
		echo "=================== failed to build uboot ============================"
		exit 1
	fi
}

function build_kernel(){
	echo "=============== build kernel ======================"
	cd $ROOT_DIR
	if [ ! -L kernel ] && [ -d ${KERNEL_DIR} ]; then
		ln -s ${KERNEL_DIR} kernel
		echo "kernel link to lichee/linux-3.10"
	elif [ ! -L kernel ] && [ ! -d ${KERNEL_DIR} ]; then
		echo "can not find kernel source code, failed to build kernel"
		exit 1
	fi
	
	cd ${LICHEE_DIR}
#	echo "export LICHEE_TOOLCHAIN_PATH=${ROOT_DIR}/lichee/out/external-toolchain/gcc-aarch64"
	if [ ! -f def-buildconfig ]; then
		echo "export LICHEE_CHIP=sun8iw11p1" >> def-buildconfig
		echo "export LICHEE_PLATFORM=androidm" >> def-buildconfig
	#	echo "export LICHEE_BUSINESS=7.x" >> def-buildconfig
	#	echo "export LICHEE_ARCH=arm64" >> def-buildconfig
		echo "export LICHEE_KERN_VER=linux-3.10" >> def-buildconfig
		echo "export LICHEE_BOARD=a40-p1" >> def-buildconfig
	#	echo "export LICHEE_BUSINESS=pad" >> def-buildconfig
	#	echo "export LICHEE_CROSS_COMPILER=aarch64-linux-gnu" >> def-buildconfig
	#	echo "export LICHEE_TOOLCHAIN_PATH=${ROOT_DIR}/lichee/out/external-toolchain/gcc-aarch64" >> def-buildconfig
	fi

	if [ ! -e .buildconfig ] ; then
		cp def-buildconfig .buildconfig
	fi
	./build.sh
	
	if [ $? != 0 ]; then
		echo "=================== failed to build kernel ============================"
		exit 1
	fi
}

function build_lichee(){
        echo "=============== build lichee ======================"
        cd $ROOT_DIR
	if [ ! -L uboot ] && [ -d ${UBOOT_DIR} ]; then
		ln -s ${UBOOT_DIR} uboot
		echo "uboot link to lichee/brandy/u-boot-2014.07"
	elif [ ! -L uboot ] && [ ! -d ${UBOOT_DIR} ]; then
		echo "can not find uboot source code, failed to build uboot"
		exit 1
	fi

        if [ ! -L kernel ] && [ -d ${KERNEL_DIR} ]; then
                ln -s ${KERNEL_DIR} kernel
                echo "kernel link to lichee/linux-3.10"
        elif [ ! -L kernel ] && [ ! -d ${KERNEL_DIR} ]; then
                echo "can not find kernel source code, failed to build kernel"
                exit 1
        fi
        
        #if [ ! -L lichee ] && [ -d ${LICHEE_DIR} ]; then
        #        ln -s ${LICHEE_DIR} lichee 
        #        echo "link to lichee"
        #elif [ ! -L lichee ] && [ ! -d ${LICHEE_DIR} ]; then
        #        echo "can not find lichee,  failed to build lichee"
        #        exit 1
        #fi

	if [ ! -L lichee-configs ] && [ -d ${LICHEE_DIR}/tools/pack/chips/sun8iw11p1/configs/ ]; then
                ln -s ${LICHEE_DIR}/tools/pack/chips/sun8iw11p1/configs/ lichee-configs
                echo "link to lichee configs"
        elif [ ! -L lichee-configs ] && [ ! -d ${LICHEE_DIR}/tools/pack/chips/sun8iw11p1/configs/ ]; then
                echo "can not find lichee configs,  failed to build lichee"
                exit 1
        fi

        cd ${LICHEE_DIR}
#       echo "export LICHEE_TOOLCHAIN_PATH=${ROOT_DIR}/lichee/out/external-toolchain/gcc-aarch64"
        if [ ! -f def-buildconfig ]; then
                echo "export LICHEE_CHIP=sun8iw11p1" >> def-buildconfig
                echo "export LICHEE_PLATFORM=androidm" >> def-buildconfig
        #       echo "export LICHEE_BUSINESS=7.x" >> def-buildconfig
        #       echo "export LICHEE_ARCH=arm64" >> def-buildconfig
                echo "export LICHEE_KERN_VER=linux-3.10" >> def-buildconfig
                echo "export LICHEE_BOARD=a40-p1" >> def-buildconfig
        #       echo "export LICHEE_BUSINESS=pad" >> def-buildconfig
        #       echo "export LICHEE_CROSS_COMPILER=aarch64-linux-gnu" >> def-buildconfig
        #       echo "export LICHEE_TOOLCHAIN_PATH=${ROOT_DIR}/lichee/out/external-toolchain/gcc-aarch64" >> def-buildconfig
        fi

        if [ ! -e .buildconfig ] ; then
                cp def-buildconfig .buildconfig
        fi
		
#		rm $KERNEL_DIR/drivers/soc/allwinner/pm/pm_debug.o		# 有可能导致kernel编译失败，删除可以正常编译
        ./build.sh

        if [ $? != 0 ]; then
                echo "=================== failed to build lichee ============================"
                exit 1
        fi
}
function build_android(){
	echo "=============== build android  ======================"
	cd $ROOT_DIR
	source jdk_openJdk18.sh
	if [ $? != 0 ]; then
		echo "=================== please install and config jdk 1.8 ============================"
		exit 1
	fi

	source build/envsetup.sh
	lunch a40_p1-eng
	extract-bsp
	echo "================ cpu processor = ${CPU_NUM} ======================"
	make -j${CPU_NUM}
	
	if [ $? != 0 ]; then
		echo "=================== failed to build android ============================"
		exit 1
	fi
}

cd ${ROOT_DIR}/../
PACKAGE_PATH=`pwd`
PACKAGE_NAME=`basename ${PACKAGE_PATH}`
function package_code(){
	cd $UBOOT_DIR
	make clean
	cd $KERNEL_DIR
	make clean

	cd $ROOT_DIR
	rm uboot kernel lichee-configs

	cd $LICHEE_DIR
	rm .buildconfig def-buildconfig
	cd $ROOT_DIR/../
	tar czvf - ${PACKAGE_NAME} --exclude=${PACKAGE_NAME}/{result,out,${LICHEE_DIR}/out,${LICHEE_DIR}/tools/pack/out,${LICHEE_DIR}/tools/pack/*.img} | split -b 2000m -d -a 2 - ${PACKAGE_NAME}-${DATE}.tgz
#	tar cvf ${PACKAGE_NAME}-${DATE}.tar --exclude=${PACKAGE_NAME}/{result,out,lichee/out,lichee/tools/pack/out,lichee/tools/pack/sun50iw6p1_android_petrel-p1_uart0.img} ${PACKAGE_NAME}
	md5sum ${PACKAGE_NAME}-${DATE}.tgz* > md5sum.txt
	
}

function pack_image(){
	cd $ROOT_DIR
	pack -d > /dev/null 2>&1
	if [ $? != 0 ]; then
		source build/envsetup.sh
		lunch a40_p1-eng
		pack -d
	fi	
}
function help_func(){
	echo "==================================================================================="
	echo "./make.sh				build all image"
	echo "./make.sh lichee:			build lichee"
	echo "./make.sh android:		build android"
	echo "./make.sh all:			build all"
	echo "./make.sh clean:			clean uboot kernel"
	echo "./make.sh distclean:		distclean uboot kernel android"
	echo "./make.sh package:		package source code and generate md5 checksum" 
	echo "./make.sh pack:			pack image" 
	echo "./make.sh help:			this message" 
	echo "==================================================================================="

}

if [ X$1 == "Xuboot" ]; then
	build_uboot
    cd $ROOT_DIR
    source build/envsetup.sh
    lunch a40_p1-eng
    extract-bsp
    pack -d
elif [ X$1 == "Xdts" ]; then	# dts for just arch/arm/boot/dts code change
   build_lichee
   cd $ROOT_DIR
   source build/envsetup.sh
   lunch a40_p1-eng
   extract-bsp
   pack -d
elif [ X$1 == "Xkernel" ]; then
	build_lichee
	build_android	# modifed kernel or uboot code, must build android
	pack -d
elif [ X$1 == "Xlichee" ]; then
	build_lichee
	build_android	# modifed kernel or uboot code, must build android
	pack -d
elif [ X$1 == "Xandroid" ]; then
	build_android
	pack -d
elif [ X$1 == "Xall" ]; then
	build_uboot
	build_lichee
	build_android
	pack -d
elif [ X$1 == "Xpackage" ]; then
	package_code
elif [ X$1 == "Xpack" ]; then	# for just sys_config.fex changed
	pack_image
elif [ X$1 == "Xclean" ]; then
	cd $UBOOT_DIR
	make clean
	cd $KERNEL_DIR
	make clean
elif [ X$1 == "Xdistclean" ]; then
	cd $ROOT_DIR
	rm uboot kernel
	cd $UBOOT_DIR
	make distclean
	cd $KERNEL_DIR
	make clean
	cd $LICHEE_DIR
	rm .buildconfig def-buildconfig
	cd $ROOT_DIR
	rm uboot kernel lichee-configs
	rm -rf out result
elif [ X$1 == "Xhelp" ]; then
	help_func
else 
	build_lichee
	build_android
	pack -d
fi

cd $ROOT_DIR
if [ ! -L result ]; then
	ln -s ${LICHEE_DIR}/tools/pack result
fi


