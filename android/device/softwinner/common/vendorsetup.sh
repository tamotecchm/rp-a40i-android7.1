#
# Copyright (C) 2012 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# This file is executed by build/envsetup.sh, and can use anything
# defined in envsetup.sh.
#
# In particular, you can add lunch options with the add_lunch_combo
# function: add_lunch_combo generic-eng

export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export PATH=$JAVA_HOME/bin:$PATH
export CLASSPATH=.:$JAVA_HOME/lib

function get_device_dir()
{
    DEVICE=$(gettop)/device/softwinner/$(get_build_var TARGET_DEVICE)
}

function cdevice()
{
    get_device_dir
    cd $DEVICE
}

function cout()
{
    cd $OUT
}

function get_lichee_out_dir()
{
    LICHEE_DIR=$ANDROID_BUILD_TOP/../lichee

    TARGET_BOARD_PLATFORM=$(get_build_var TARGET_BOARD_PLATFORM)

    if [ "$TARGET_BOARD_PLATFORM" == "kylin" ]; then
        LINUXOUT_DIR=$LICHEE_DIR/out/sun9iw1p1/android/common
    fi
    if [ "$TARGET_BOARD_PLATFORM" == "astar" ]; then
        LINUXOUT_DIR=$LICHEE_DIR/out/sun8iw5p1/android/common
    fi
    if [ "$TARGET_BOARD_PLATFORM" == "octopus" ]; then
        LINUXOUT_DIR=$LICHEE_DIR/out/sun8iw6p1/android/common
    fi
    if [ "$TARGET_BOARD_PLATFORM" == "eagle" ]; then
        LINUXOUT_DIR=$LICHEE_DIR/out/sun8iw6p1/android/common
    fi
    if [ "$TARGET_BOARD_PLATFORM" == "tulip" ]; then
        LINUXOUT_DIR=$LICHEE_DIR/out/sun50iw1p1/android/common
    fi
    if [ "$TARGET_BOARD_PLATFORM" == "t3" ]; then
        LINUXOUT_DIR=$LICHEE_DIR/out/sun8iw11p1/androidm/common
    fi
    if [ "$TARGET_BOARD_PLATFORM" == "a40" ]; then
        LINUXOUT_DIR=$LICHEE_DIR/out/sun8iw11p1/androidm/common
    fi
    LINUXOUT_MODULE_DIR=$LINUXOUT_DIR/lib/modules/*/*
}

function extract-bsp()
{
    CURDIR=$PWD

    get_lichee_out_dir
    get_device_dir

    cd $DEVICE

    #extract kernel
    if [ -f kernel ] ; then
        rm kernel
    fi
    cp $LINUXOUT_DIR/bImage kernel
    echo "$DEVICE/bImage copied!"

    #extract linux modules
    if [ -d modules ] ; then
        rm -rf modules
    fi
    mkdir -p modules/modules
    cp -rf $LINUXOUT_MODULE_DIR modules/modules
    echo "$DEVICE/modules copied!"
    chmod 0755 modules/modules/*

# create modules.mk
(cat << EOF) > ./modules/modules.mk
# modules.mk generate by extract-files.sh, do not edit it.
PRODUCT_COPY_FILES += \\
    \$(call find-copy-subdir-files,*,\$(LOCAL_PATH)/modules,system/vendor/modules)
EOF

    cd $CURDIR
}

function package_usage()
{
    printf "Usage: pack [-cCHIP] [-pPLATFORM] [-bBOARD] [-d] [-s] [-v] [-h]
    -c CHIP (default: $chip)
    -p PLATFORM (default: $platform)
    -b BOARD (default: $board)
    -d pack firmware with debug info output to card0
    -s pack firmware with signature
    -v pack firmware with secureboot
    -h print this help message
"
}

function package()
{
    chip=$(get_build_var TARGET_BOARD_CHIP)
    platform=android
    board=$(get_build_var PRODUCT_BOARD)
    debug=uart0
    sigmode=none
    securemode=none

    while getopts "c:p:b:dsvh" arg
    do
        case $arg in
            c)
                chip=$OPTARG
                ;;
            p)
                platform=$OPTARG
                ;;
            b)
                board=$OPTARG
                ;;
            d)
                debug=card0
                ;;
            s)
                sigmode=sig
                ;;
            v)
                securemode=secure
                ;;
            h)
                package_usage
                exit 0
                ;;
            ?)
                exit 1
                ;;
        esac
    done

    cd $PACKAGE
    ./pack -c $chip -p $platform -b $board -d $debug -s $sigmode -v $securemode
    cd -
}

function pack()
{
    T=$(gettop)
    get_device_dir
    export ANDROID_IMAGE_OUT=$OUT
    export PACKAGE=$T/../lichee/tools/pack

	sh $DEVICE/package.sh $*
}

function fex_copy()
{
    if [ -e $1 ]; then
        cp -vf $1 $2
    else
        echo $1" not exist"
    fi
}

function update_uboot()
{
    echo "-------------------------------------"
    TARGET_BOARD_PLATFORM=$(get_build_var TARGET_BOARD_PLATFORM)
    UBOOT_FEX=boot_package.fex

    echo "copy fex into $1"
    fex_copy $PACKAGE/out/boot-resource.fex ./boot-resource.fex
    fex_copy $PACKAGE/out/env.fex ./env.fex
    fex_copy $PACKAGE/out/boot0_nand.fex ./boot0_nand.fex
    fex_copy $PACKAGE/out/boot0_sdcard.fex ./boot0_sdcard.fex
    fex_copy $PACKAGE/out/$UBOOT_FEX ./u-boot.fex
    fex_copy $PACKAGE/out/toc1.fex ./toc1.fex
    fex_copy $PACKAGE/out/toc0.fex ./toc0.fex
    fex_copy $PACKAGE/out/verity_block.fex ./verity_block.fex
    zip -m $1 ./boot-resource.fex
    zip -m $1 ./env.fex
    zip -m $1 ./boot0_nand.fex
    zip -m $1 ./boot0_sdcard.fex
    zip -m $1 ./u-boot.fex
    zip -m $1 ./toc1.fex
    zip -m $1 ./toc0.fex
    zip -m $1 ./verity_block.fex
}

function pack4dist()
{
    # Found out the number of cores we can use
    cpu_cores=`cat /proc/cpuinfo | grep "processor" | wc -l`
    if [ ${cpu_cores} -le 8 ] ; then
        JOBS=${cpu_cores}
    else
        JOBS=`expr ${cpu_cores} / 2`
    fi

    DATE=`date +%Y%m%d`
    keys_dir="./vendor/security"
    target_files="$OUT/obj/PACKAGING/target_files_intermediates/$TARGET_PRODUCT-target_files-$DATE.zip"
    signed_target_files="$OUT/$TARGET_PRODUCT-signed_target_files-$DATE.zip"
    full_ota="$OUT/$TARGET_PRODUCT-full_ota-$DATE.zip"
    inc_ota="$OUT/$TARGET_PRODUCT-inc_ota-$DATE.zip"
    target_images="$OUT/target_images.zip"

    make -j $JOBS target-files-package

    if [ -d $keys_dir ] ; then
        ./build/tools/releasetools/sign_target_files_apks \
            -d $keys_dir $target_files $signed_target_files
        ./build/tools/releasetools/img_from_target_files \
            $signed_target_files $target_images
        final_target_files=$signed_target_files
    else
        ./build/tools/releasetools/img_from_target_files \
            $target_files $target_images
        final_target_files=$target_files
    fi

    unzip -o $target_images -d $OUT
    rm $target_images

    pack $@
    update_uboot $final_target_files
    ./build/tools/releasetools/ota_from_target_files --block $final_target_files $full_ota
    ./build/tools/releasetools/ota_from_target_files --block -i $final_target_files $final_target_files $inc_ota
    echo -e "target files package: \033[31m$final_target_files\033[0m"
    echo -e "full ota zip: \033[31m$full_ota\033[0m"
    echo -e "inc ota zip: \033[31m$inc_ota\033[0m"
}

function verity_key_init()
{
    echo "-----verity_key_init-----"
    device/softwinner/common/verity/gen_dm_verity_key.sh
}

function verity_data_init()
{
    echo "-----verity_data_init-----"
    device/softwinner/common/verity/gen_dm_verity_data.sh ${OUT}/system.img ${OUT}/verity_block
    cp -vf ${OUT}/verity_block ${OUT}/verity_block.img
}

function read_var()
{
    read -p "please enter $1($2): " TMP
    eval $1="${TMP:=\"$2\"}"
}

function clone()
{
    if [ $# -ne 0 ]; then
        echo "don't enter any params"
        return
    fi
    source_device=$(get_build_var TARGET_DEVICE)
    source_product=$(get_build_var TARGET_PRODUCT)
    source_path=$(gettop)/device/softwinner/$source_device
    # PRODUCT_DEVICE
    read_var PRODUCT_DEVICE "$source_device"
    if [ $source_device == $PRODUCT_DEVICE ]; then
        echo "don't have the same device name!"
        return
    fi
    TARGET_PATH=$(gettop)/device/softwinner/$PRODUCT_DEVICE
    if [ -e $TARGET_PATH ]; then
        read -p "$PRODUCT_DEVICE is already exists, delete it?(y/n)"
        case $REPLY in
            [Yy])
                echo "delete"
                rm -rf $TARGET_PATH
                ;;
            [Nn])
                echo "do nothing"
                return
                ;;
            *)
                echo "do nothing"
                return
                ;;
        esac
    fi
    # copy device
    cp -r $source_path $TARGET_PATH
    rm -rf $TARGET_PATH/.git*
    sed -i "s/$source_device/$PRODUCT_DEVICE/g" `grep -rl $source_device $TARGET_PATH`
    # PRODUCT_NAME
    read_var PRODUCT_NAME "$source_product"
    if [ $source_product == $PRODUCT_NAME ]; then
        echo "don't have the same product name!"
        return
    fi
    mv $TARGET_PATH/$source_product.mk $TARGET_PATH/$PRODUCT_NAME.mk
    sed -i "s/$source_product/$PRODUCT_NAME/g" `grep -rl $source_product $TARGET_PATH`
    # config
    read_var PRODUCT_BOARD "$(get_build_var PRODUCT_BOARD)"
    sed -i "s/\(PRODUCT_BOARD := \).*/\1$PRODUCT_BOARD/g" $TARGET_PATH/$PRODUCT_NAME.mk
    read_var PRODUCT_MODEL "$(get_build_var PRODUCT_MODEL)"
    sed -i "s/\(PRODUCT_MODEL := \).*/\1$PRODUCT_MODEL/g" $TARGET_PATH/$PRODUCT_NAME.mk
    density=`sed -n 's/.*ro\.sf\.lcd_density=\([0-9]\+\).*/\1/p' $TARGET_PATH/$PRODUCT_NAME.mk`
    read_var DENSITY "$density"
    sed -i "s/\(ro\.sf\.lcd_density=\).*/\1$DENSITY/g" $TARGET_PATH/$PRODUCT_NAME.mk
    # 160(mdpi) 213(tvdpi) 240(hdpi) 320(xhdpi) 400(400dpi) 480(xxhdpi) 560(560dpi) 640(xxxhdpi)
    if [ $DENSITY -lt 186 ]; then
        PRODUCT_AAPT_PREF_CONFIG=mdpi
    elif [ $DENSITY -lt 226 ]; then
        PRODUCT_AAPT_PREF_CONFIG=tvdpi
    elif [ $DENSITY -lt 280 ]; then
        PRODUCT_AAPT_PREF_CONFIG=hdpi
    elif [ $DENSITY -lt 360 ]; then
        PRODUCT_AAPT_PREF_CONFIG=xhdpi
    elif [ $DENSITY -lt 440 ]; then
        PRODUCT_AAPT_PREF_CONFIG=400dpi
    elif [ $DENSITY -lt 520 ]; then
        PRODUCT_AAPT_PREF_CONFIG=xxhdpi
    elif [ $DENSITY -lt 600 ]; then
        PRODUCT_AAPT_PREF_CONFIG=560dpi
    else
        PRODUCT_AAPT_PREF_CONFIG=xxxhdpi
    fi
    sed -i "s/\(PRODUCT_AAPT_PREF_CONFIG := \).*/\1$PRODUCT_AAPT_PREF_CONFIG/g" $TARGET_PATH/$PRODUCT_NAME.mk
}
