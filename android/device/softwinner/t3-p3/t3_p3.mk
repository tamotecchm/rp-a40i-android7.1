$(call inherit-product, build/target/product/full_base.mk)
$(call inherit-product, device/softwinner/t3-common/t3-common.mk)
$(call inherit-product-if-exists, device/softwinner/t3-p3/modules/modules.mk)

DEVICE_PACKAGE_OVERLAYS := device/softwinner/t3-p3/overlay \
                           $(DEVICE_PACKAGE_OVERLAYS)

PRODUCT_PACKAGES += \
    ESFileExplorer \
    VideoPlayer \
    Launcher3 \
    WallpaperPicker \
    GocSdk \
    BionLauncher \
    CarletDriverRecorder \
    BionSettings

	
LOCAL_SHARED_LIBRARIES += \
    libwater_mark

	
# copy init.xx.rc file for realtek wifi/bt module.
PRODUCT_COPY_FILES += \
    device/softwinner/common/init.wireless.realtek.rc:root/init.wireless.realtek.rc

PRODUCT_COPY_FILES += \
    device/softwinner/t3-p3/kernel:kernel \
    device/softwinner/t3-p3/fstab.sun8iw11p1:root/fstab.sun8iw11p1 \
    device/softwinner/t3-p3/init.device.rc:root/init.device.rc \
    device/softwinner/common/verity/rsa_key/verity_key:root/verity_key \
    device/softwinner/t3-p3/modules/modules/nand.ko:root/nand.ko \
    device/softwinner/t3-p3/modules/modules/gslX680new.ko:root/gslX680new.ko \
    device/softwinner/t3-p3/modules/modules/sw-device.ko:root/sw-device.ko

PRODUCT_COPY_FILES += \
    device/softwinner/t3-p3/configs/camera.cfg:system/etc/camera.cfg \
    device/softwinner/t3-p3/configs/cfg-Gallery2.xml:system/etc/cfg-Gallery2.xml \
    device/softwinner/t3-p3/configs/gsensor.cfg:system/usr/gsensor.cfg \
    device/softwinner/t3-p3/configs/media_profiles.xml:system/etc/media_profiles.xml \
    device/softwinner/t3-p3/configs/sunxi-keyboard.kl:system/usr/keylayout/sunxi-keyboard.kl \
    device/softwinner/t3-p3/configs/sunxi-ir.kl:system/usr/keylayout/sunxi-ir.kl \
    device/softwinner/t3-p3/configs/tp.idc:system/usr/idc/tp.idc \
    device/softwinner/t3-p3/configs/wifi_efuse_8723cs.map:system/etc/wifi/wifi_efuse_8723cs.map

#    frameworks/native/data/etc/android.hardware.bluetooth.xml:system/etc/permissions/android.hardware.bluetooth.xml 
#    frameworks/native/data/etc/android.hardware.bluetooth_le.xml:system/etc/permissions/android.hardware.bluetooth_le.xml 		
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.touchscreen.multitouch.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.xml \
    frameworks/native/data/etc/android.hardware.ethernet.xml:system/etc/permissions/android.hardware.ethernet.xml


PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.camera.xml:system/etc/permissions/android.hardware.camera.xml   \
    frameworks/native/data/etc/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
    frameworks/native/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml

PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.managed_users.xml:system/etc/permissions/android.software.managed_users.xml 

PRODUCT_COPY_FILES += \
	device/softwinner/common/gps/gps.default.so:system/lib/hw/gps.default.so \
	device/softwinner/common/gps/miniBL.bin:system/bin/miniBL.bin \
	device/softwinner/common/gps/martianFW.bin:system/bin/martianFW.bin \
	device/softwinner/common/gps/zsgps.conf:system/etc/zsgps.conf 
	
	
PRODUCT_COPY_FILES += \
  device/softwinner/t3-p3/media/bootanimation.zip:system/media/bootanimation.zip
#    device/softwinner/t3-p3/media/boot.wav:system/media/boot.wav
#    device/softwinner/t3-p3/media/audio_conf.txt:system/media/audio_conf.txt

PRODUCT_COPY_FILES += \
	hardware/aw/xr819/kernel-firmware/boot_xr819.bin:system/etc/firmware/boot_xr819.bin \
	hardware/aw/xr819/kernel-firmware/fw_xr819.bin:system/etc/firmware/fw_xr819.bin \
	hardware/aw/xr819/kernel-firmware/sdd_xr819.bin:system/etc/firmware/sdd_xr819.bin  

PRODUCT_COPY_FILES += \
 	device/softwinner/t3-p3/insmodlate.sh:system/etc/insmodlate.sh
 	
#bc5&bc6 bt
PRODUCT_COPY_FILES += \
	device/softwinner/common/bt_bc8/config.ini:system/config.ini \
	device/softwinner/common/bt_bc8/gocsdk:system/bin/gocsdk \
	device/softwinner/common/bt_bc8/ring.mp3:system/ring.mp3 \
	device/softwinner/common/bt_bc8/libwapm.so:system/lib/libwapm.so 	
	
PRODUCT_PROPERTY_OVERRIDES += \
    persist.sys.usb.config=mtp,adb \
    ro.adb.secure=0 \
    rw.logger=0

PRODUCT_PROPERTY_OVERRIDES += \
    dalvik.vm.heapsize=512m \
    dalvik.vm.heapstartsize=8m \
    dalvik.vm.heapgrowthlimit=192m \
    dalvik.vm.heaptargetutilization=0.75 \
    dalvik.vm.heapminfree=2m \
    dalvik.vm.heapmaxfree=8m 

PRODUCT_PROPERTY_OVERRIDES += \
	persis.sys.bluetooth_goc=0
	
PRODUCT_PROPERTY_OVERRIDES += \
    ro.zygote.disable_gl_preload=true

PRODUCT_PROPERTY_OVERRIDES += \
    ro.sf.lcd_density=160 \
    ro.display.sdcard=1 \
    ro.part.sdcard=1


PRODUCT_PROPERTY_OVERRIDES += \
    ro.spk_dul.used=false \

PRODUCT_PROPERTY_OVERRIDES += \
    persist.sys.timezone=Asia/Shanghai \
    persist.sys.country=CN \
    persist.sys.language=zh
    
PRODUCT_PROPERTY_OVERRIDES += \
	persis.sys.bluetooth_goc=1    

# stoarge
PRODUCT_PROPERTY_OVERRIDES += \
    persist.fw.force_adoptable=true

PRODUCT_PROPERTY_OVERRIDES += \
	ro.lockscreen.disable.default=true
	
PRODUCT_PROPERTY_OVERRIDES += \
    ro.product.first_api_level=24

PRODUCT_CHARACTERISTICS := tablet

PRODUCT_AAPT_CONFIG := tvdpi xlarge hdpi xhdpi large
PRODUCT_AAPT_PREF_CONFIG := tvdpi

PRODUCT_BRAND := Allwinner
PRODUCT_NAME := t3_p3
PRODUCT_DEVICE := t3-p3
PRODUCT_MODEL := QUAD-CORE T3 p3
PRODUCT_MANUFACTURER := Allwinner
PACK_BOARD := t3-p3
# 4. when 3g_dongle BOARD_MODEL_TYPE := usb_modeswitch;
#    when r2 4G,    BOARD_MODEL_TYPE := r2_4g
BOARD_MODEL_TYPE := usb_modeswitch

ifeq ($(BOARD_MODEL_TYPE),usb_modeswitch)
$(call inherit-product, device/softwinner/common/rild/radio_common.mk)
endif

ifeq ($(BOARD_MODEL_TYPE),r2_4g)
PRODUCT_PACKAGES += \
    xin_logTcpSend \
    xin_rom_update	\
		libr2_t3-ril
PRODUCT_PACKAGES += rild
			
PRODUCT_COPY_FILES += \
	device/softwinner/t3-p3/r2ril/libr2_t3-ril.so:system/lib/libr2_t3-ril.so \
	device/softwinner/t3-p3/r2ril/apns-conf_sdk.xml:system/etc/apns-conf.xml 
#PRODUCT_COPY_FILES += \
#	device/softwinner/t3-p3/r2ril/apns-conf_sdk.xml:system/etc/apns-conf.xml 
#R2Update    
#PRODUCT_PACKAGES += \
#    R2Update

# Radio parameter
PRODUCT_PROPERTY_OVERRIDES += \
	rild.libargs=-d/dev/ttyVCOM0  \
	rild.libpath=libr2_t3-ril.so \
	ro.sw.embeded.telephony=false
endif


#PRODUCT_PROPERTY_OVERRIDES += \
#	ro.kernel.qemu=1 \
#	debug.sf.no_hw_vsync=1

ifeq ($(BOARD_USE_ADAS_MODULE),allwinner)
PRODUCT_PACKAGES += \
    libAdas
endif
