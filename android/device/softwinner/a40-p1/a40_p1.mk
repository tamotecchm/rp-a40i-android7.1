include vendor/aw/rpdzkj/rpdzkj.mk
$(call inherit-product, build/target/product/full_base.mk)
$(call inherit-product, device/softwinner/a40-common/a40-common.mk)
$(call inherit-product-if-exists, device/softwinner/a40-p1/modules/modules.mk)

DEVICE_PACKAGE_OVERLAYS := device/softwinner/a40-p1/overlay \
                           $(DEVICE_PACKAGE_OVERLAYS)

PRODUCT_PACKAGES += \
    ESFileExplorer \
    VideoPlayer \
    Launcher3 \
    WallpaperPicker \
    GocSdk \
    CarletDriverRecorder \
    ComAssistant \
    DoubleScreenDisplay \
	GPIO \
	GPStest \
	TDMonitor

#    BionLauncher \
#    BionSettings


PRODUCT_PACKAGES += \
	libtinyalsa \
    	tinyplay    \
    	tinycap     \
    	tinymix     \
    	tinypcminfo
	
LOCAL_SHARED_LIBRARIES += \
    libwater_mark

	
# copy init.xx.rc file for realtek wifi/bt module.

PRODUCT_COPY_FILES += \
    device/softwinner/a40-p1/kernel:kernel \
    device/softwinner/a40-p1/fstab.sun8iw11p1:root/fstab.sun8iw11p1 \
    device/softwinner/a40-p1/init.device.rc:root/init.device.rc \
    device/softwinner/common/verity/rsa_key/verity_key:root/verity_key \
    device/softwinner/a40-p1/modules/modules/nand.ko:root/nand.ko \
    device/softwinner/a40-p1/modules/modules/gslX680new.ko:root/gslX680new.ko \
    device/softwinner/a40-p1/modules/modules/sw-device.ko:root/sw-device.ko

PRODUCT_COPY_FILES += \
    device/softwinner/a40-p1/configs/camera.cfg:system/etc/camera.cfg \
    device/softwinner/a40-p1/configs/cfg-Gallery2.xml:system/etc/cfg-Gallery2.xml \
    device/softwinner/a40-p1/configs/gsensor.cfg:system/usr/gsensor.cfg \
    device/softwinner/a40-p1/configs/media_profiles.xml:system/etc/media_profiles.xml \
    device/softwinner/a40-p1/configs/sunxi-keyboard.kl:system/usr/keylayout/sunxi-keyboard.kl \
    device/softwinner/a40-p1/configs/sunxi-ir.kl:system/usr/keylayout/sunxi-ir.kl \
    device/softwinner/a40-p1/configs/sunxi-ir.kl:system/usr/keylayout/sunxi-ir-recv.kl \
    device/softwinner/a40-p1/configs/tp.idc:system/usr/idc/tp.idc \
    device/softwinner/a40-p1/configs/wifi_efuse_8723cs.map:system/etc/wifi/wifi_efuse_8723cs.map
    

PRODUCT_COPY_FILES += \
    device/softwinner/a40-p1/configs/virtual-remote.kl:system/usr/keylayout/virtual-remote.kl \
    device/softwinner/a40-p1/configs/customer_ir_9f00.kl:system/usr/keylayout/customer_ir_9f00.kl \
    device/softwinner/a40-p1/configs/customer_ir_dd22.kl:system/usr/keylayout/customer_ir_dd22.kl \
    device/softwinner/a40-p1/configs/customer_ir_fb04.kl:system/usr/keylayout/customer_ir_fb04.kl \
    device/softwinner/a40-p1/configs/customer_ir_ff00.kl:system/usr/keylayout/customer_ir_ff00.kl \
    device/softwinner/a40-p1/configs/customer_ir_4cb3.kl:system/usr/keylayout/customer_ir_4cb3.kl \
    device/softwinner/a40-p1/configs/customer_ir_bc00.kl:system/usr/keylayout/customer_ir_bc00.kl \
    device/softwinner/a40-p1/configs/customer_ir_fc00.kl:system/usr/keylayout/customer_ir_fc00.kl \
    device/softwinner/a40-p1/configs/customer_ir_2992.kl:system/usr/keylayout/customer_ir_2992.kl \
    device/softwinner/a40-p1/configs/customer_rc5_ir_04.kl:system/usr/keylayout/customer_rc5_ir_04.kl \
    device/softwinner/a40-p1/configs/sunxi-ir-uinput.kl:system/usr/keylayout/sunxi-ir-uinput.kl


PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.bluetooth.xml:system/etc/permissions/android.hardware.bluetooth.xml \
    frameworks/native/data/etc/android.hardware.bluetooth_le.xml:system/etc/permissions/android.hardware.bluetooth_le.xml \
	device/softwinner/a40-p1/configs/bluetooth/bt_vendor.conf:system/etc/bluetooth/bt_vendor.conf


PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.touchscreen.multitouch.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.xml \
    frameworks/native/data/etc/android.hardware.ethernet.xml:system/etc/permissions/android.hardware.ethernet.xml


PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.camera.xml:system/etc/permissions/android.hardware.camera.xml   \
    frameworks/native/data/etc/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
    frameworks/native/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml

PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.managed_users.xml:system/etc/permissions/android.software.managed_users.xml 

#PRODUCT_COPY_FILES += \
#	device/softwinner/common/gps/gps.default.so:system/lib/hw/gps.default.so \
#	device/softwinner/common/gps/miniBL.bin:system/bin/miniBL.bin \
#	device/softwinner/common/gps/martianFW.bin:system/bin/martianFW.bin \
#	device/softwinner/common/gps/zsgps.conf:system/etc/zsgps.conf 
PRODUCT_COPY_FILES += \
			hardware/aw/gps/TD1030HAL/AGNSS/gps.default.so:system/lib/hw/gps.default.so \
			hardware/aw/gps/TD1030HAL/AGNSS/libtdcrypto.so:system/lib/libtdcrypto.so \
			hardware/aw/gps/TD1030HAL/AGNSS/libtdssl.so:system/lib/libtdssl.so \
			hardware/aw/gps/TD1030HAL/AGNSS/libtdsupl.so:system/lib/libtdsupl.so \
			hardware/aw/gps/TD1030HAL/AGNSS/supl-client:system/bin/supl-client \
			hardware/aw/gps/TD1030HAL/AGNSS/tdgnss.conf:system/etc/tdgnss.conf



	
	
PRODUCT_COPY_FILES += \
  device/softwinner/a40-p1/media/bootanimation.zip:system/media/bootanimation.zip
#    device/softwinner/a40-p1/media/boot.wav:system/media/boot.wav
#    device/softwinner/a40-p1/media/audio_conf.txt:system/media/audio_conf.txt

PRODUCT_COPY_FILES += \
	hardware/aw/xr819/kernel-firmware/boot_xr819.bin:system/etc/firmware/boot_xr819.bin \
	hardware/aw/xr819/kernel-firmware/fw_xr819.bin:system/etc/firmware/fw_xr819.bin \
	hardware/aw/xr819/kernel-firmware/sdd_xr819.bin:system/etc/firmware/sdd_xr819.bin  

PRODUCT_COPY_FILES += \
 	device/softwinner/a40-p1/insmodlate.sh:system/etc/insmodlate.sh
 	
#bc5&bc6 bt
PRODUCT_COPY_FILES += \
	device/softwinner/common/bt_bc8/config.ini:system/config.ini \
	device/softwinner/common/bt_bc8/gocsdk:system/bin/gocsdk \
	device/softwinner/common/bt_bc8/ring.mp3:system/ring.mp3 \
	device/softwinner/common/bt_bc8/libwapm.so:system/lib/libwapm.so 	
	
PRODUCT_COPY_FILES += \
	device/softwinner/a40-p1/r2ril/libreference-ril-ec20.so:system/lib/libreference-ril-ec20.so 
	
PRODUCT_PROPERTY_OVERRIDES += \
    persist.sys.usb.config=mtp,adb \
    ro.adb.secure=0 \
    rw.logger=0

PRODUCT_PROPERTY_OVERRIDES += \
    dalvik.vm.heapsize=512m \
    dalvik.vm.heapstartsize=8m \
    dalvik.vm.heapgrowthlimit=192m \
    dalvik.vm.heaptargetutilization=0.75 \
    dalvik.vm.heapminfree=2m \
    dalvik.vm.heapmaxfree=8m 

PRODUCT_PROPERTY_OVERRIDES += \
	persis.sys.bluetooth_goc=0
	
PRODUCT_PROPERTY_OVERRIDES += \
    ro.zygote.disable_gl_preload=true

PRODUCT_PROPERTY_OVERRIDES += \
    ro.display.sdcard=1 \
    ro.part.sdcard=1 

PRODUCT_PROPERTY_OVERRIDES += \
							  config.disable_bluetooth=false

PRODUCT_PROPERTY_OVERRIDES += \
    ro.spk_dul.used=false \

#PRODUCT_PROPERTY_OVERRIDES += \
#    persist.sys.timezone=Asia/Shanghai \
#    persist.sys.country=CN \
#    persist.sys.language=zh
    
PRODUCT_PROPERTY_OVERRIDES += \
	persis.sys.bluetooth_goc=1    

# stoarge
PRODUCT_PROPERTY_OVERRIDES += \
    persist.fw.force_adoptable=true

PRODUCT_PROPERTY_OVERRIDES += \
	ro.lockscreen.disable.default=true
	
PRODUCT_PROPERTY_OVERRIDES += \
    ro.product.first_api_level=24

PRODUCT_CHARACTERISTICS := tablet

PRODUCT_AAPT_CONFIG := tvdpi xlarge hdpi xhdpi large
PRODUCT_AAPT_PREF_CONFIG := tvdpi

PRODUCT_BRAND := Allwinner
PRODUCT_NAME := a40_p1
PRODUCT_DEVICE := a40-p1
PRODUCT_MODEL := QUAD-CORE T3 p3
PRODUCT_MANUFACTURER := Allwinner
PACK_BOARD := a40-p1
# 4. when 3g_dongle BOARD_MODEL_TYPE := usb_modeswitch;
#    when r2 4G,    BOARD_MODEL_TYPE := r2_4g
BOARD_MODEL_TYPE := usb_modeswitch

ifeq ($(BOARD_MODEL_TYPE),usb_modeswitch)
$(call inherit-product, device/softwinner/common/rild/radio_common.mk)
endif

ifeq ($(BOARD_MODEL_TYPE),r2_4g)
PRODUCT_PACKAGES += \
    xin_logTcpSend \
    xin_rom_update	\
		libr2_a40-ril
PRODUCT_PACKAGES += rild
			
PRODUCT_COPY_FILES += \
	device/softwinner/a40-p1/r2ril/libr2_a40-ril.so:system/lib/libr2_a40-ril.so \
	device/softwinner/a40-p1/r2ril/apns-conf_sdk.xml:system/etc/apns-conf.xml 
#PRODUCT_COPY_FILES += \
#	device/softwinner/a40-p1/r2ril/apns-conf_sdk.xml:system/etc/apns-conf.xml 
#R2Update    
#PRODUCT_PACKAGES += \
#    R2Update

# Radio parameter
PRODUCT_PROPERTY_OVERRIDES += \
	rild.libargs=-d/dev/ttyUSB2  \
	rild.libpath=libreference-ril-ec20.so \
	ro.sw.embeded.telephony=true
endif


PRODUCT_PROPERTY_OVERRIDES += \
	ro.telephony.default_network=9


#PRODUCT_PROPERTY_OVERRIDES += \
#	ro.kernel.qemu=1 \
#	debug.sf.no_hw_vsync=1

ifeq ($(BOARD_USE_ADAS_MODULE),allwinner)
PRODUCT_PACKAGES += \
    libAdas
endif

#superuser
PRODUCT_COPY_FILES += \
	vendor/aw/rpdzkj/root/su:system/xbin/su \
	vendor/aw/rpdzkj/root/su:system/xbin/daemonsu \
	vendor/aw/rpdzkj/root/su:system/xbin/sugote \
	vendor/aw/rpdzkj/root/supolicy:system/xbin/supolicy \
	vendor/aw/rpdzkj/root/install-recovery.sh:system/bin/install-recovery.sh

