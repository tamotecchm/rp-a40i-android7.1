# inherit common.mk
$(call inherit-product, device/softwinner/common/common.mk)

DEVICE_PACKAGE_OVERLAYS := \
    device/softwinner/t3-common/overlay \
    $(DEVICE_PACKAGE_OVERLAYS)

PRODUCT_PACKAGES += \
    hwcomposer.t3 \
    display.t3 \
    camera.t3 \
    lights.t3 \
    power.t3

PRODUCT_PACKAGES += \
    libion

# audio
PRODUCT_PACKAGES += \
    audio.primary.t3 \
    audio.external.t3 \
    audio.a2dp.default \
    audio.usb.default \
    audio.r_submix.default

PRODUCT_COPY_FILES += \
    device/softwinner/t3-common/configs/audio_policy_configuration.xml:system/etc/audio_policy_configuration.xml \
    device/softwinner/t3-common/configs/audio_policy_volumes_drc.xml:system/etc/audio_policy_volumes_drc.xml \
    hardware/aw/audio/codec_paths.xml:system/etc/codec_paths.xml \
    device/softwinner/t3-common/configs/cfg-videoplayer.xml:system/etc/cfg-videoplayer.xml \

PRODUCT_COPY_FILES += \
    device/softwinner/t3-common/configs/media_codecs.xml:system/etc/media_codecs.xml \
    device/softwinner/t3-common/configs/media_codecs_performance.xml:system/etc/media_codecs_performance.xml \
    device/softwinner/t3-common/configs/media_codecs_google_video.xml:system/etc/media_codecs_google_video.xml \
    device/softwinner/t3-common/init.recovery.sun8iw11p1.rc:root/init.recovery.sun8iw11p1.rc \
    device/softwinner/t3-common/init.sun8iw11p1.rc:root/init.sun8iw11p1.rc \
    device/softwinner/t3-common/init.sun8iw11p1.usb.rc:root/init.sun8iw11p1.usb.rc \
    device/softwinner/t3-common/ueventd.sun8iw11p1.rc:root/ueventd.sun8iw11p1.rc

# egl
PRODUCT_COPY_FILES += \
    hardware/aw/egl/t3/egl.cfg:system/lib/egl/egl.cfg \
    hardware/aw/egl/t3/lib/gralloc.sunxi.so:system/lib/hw/gralloc.sun8iw11p1.so \
    hardware/aw/egl/t3/lib/libGLES_mali.so:system/lib/egl/libGLES_mali.so

PRODUCT_PROPERTY_OVERRIDES += \
    wifi.interface=wlan0 \
    wifi.supplicant_scan_interval=15 \
    keyguard.no_require_sim=true

PRODUCT_PROPERTY_OVERRIDES += \
    ro.kernel.android.checkjni=0

# 131072=0x20000 196608=0x30000
PRODUCT_PROPERTY_OVERRIDES += \
    ro.opengles.version=131072

# For mali GPU only
PRODUCT_PROPERTY_OVERRIDES += \
    debug.hwui.render_dirty_regions=false

PRODUCT_PROPERTY_OVERRIDES += \
    persist.sys.strictmode.visual=0 \
    persist.sys.strictmode.disable=1


PRODUCT_PROPERTY_OVERRIDES += \
    ro.sys.cputype=QuadCore-T3

# For Gralloc
PRODUCT_PROPERTY_OVERRIDES += \
    ro.sys.gralloc_carveout_enable=1

# Enabling type-precise GC results in larger optimized DEX files.  The
# additional storage requirements for ".odex" files can cause /system
# to overflow on some devices, so this is configured separately for
# each product.
PRODUCT_TAGS += dalvik.gc.type-precise

PRODUCT_PROPERTY_OVERRIDES += \
    ro.product.firmware=V1.0

# if DISPLAY_BUILD_NUMBER := true then
# BUILD_DISPLAY_ID := $(BUILD_ID).$(BUILD_NUMBER)
# required by gms.
DISPLAY_BUILD_NUMBER := true
BUILD_NUMBER := $(shell date +%Y%m%d)

# widevine
BOARD_WIDEVINE_OEMCRYPTO_LEVEL := 3
SECURE_OS_OPTEE := no

#add widevine libraries
PRODUCT_PROPERTY_OVERRIDES += \
    drm.service.enabled=true \
    ro.sys.widevine_oemcrypto_level=3

# Gralloc parameters
# If ro.sys.ion_flush_cache_range is set to 1, we will
# flush Cache via ioctl ION_IOC_SUNXI_FLUSH_RANGE.
PRODUCT_PROPERTY_OVERRIDES += \
    ro.sys.ion_flush_cache_range=1

PRODUCT_PACKAGES += \
    com.google.widevine.software.drm.xml \
    com.google.widevine.software.drm \
    libdrmwvmplugin \
    libwvm \
    libWVStreamControlAPI_L${BOARD_WIDEVINE_OEMCRYPTO_LEVEL} \
    libwvdrm_L${BOARD_WIDEVINE_OEMCRYPTO_LEVEL} \
    libdrmdecrypt \
    libwvdrmengine

ifeq ($(BOARD_WIDEVINE_OEMCRYPTO_LEVEL), 1)
    PRODUCT_PACKAGES += liboemcrypto \
                        libtee_client
endif

#------------------ watermark -------------------

PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*.bmp,hardware/aw/camera/allwinnertech/water_mark/watermark,system/watermark)
#------------------------------------------------
