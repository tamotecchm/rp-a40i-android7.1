/*
*********************************************************************************************************
*                                                AR100 SYSTEM
*                                     AR100 Software System Develop Kits
*                                                check module
*
*                                    (c) Copyright 2012-2016, Superm China
*                                             All Rights Reserved
*
* File    : check.h
* By      : Superm
* Version : v1.0
* Date    : 2015-1-19
* Descript: check module public header.
* Update  : date                auther      ver     notes
*           2015-1-19 8:58:37   Superm      1.0     Create this file.
*********************************************************************************************************
*/

#ifndef __CHECK_H__
#define __CHECK_H__
#if CHECK_USED
s32 check_board(void);
#endif /* CHECK_USED */
#endif  //__DVFS_H__
