/*
*********************************************************************************************************
*                                                AR100 SYSTEM
*                                     AR100 Software System Develop Kits
*                                            ar100 bare test system
*
*                                    (c) Copyright 2012-2016, Superm China
*                                             All Rights Reserved
*
* File    : bare_test.h
* By      : Superm
* Version : v1.0
* Date    : 2012-10-25
* Descript: ar100 bare test system public header.
* Update  : date                auther      ver     notes
*           2012-10-25 10:29:10 Superm      1.0     Create this file.
*********************************************************************************************************
*/

#ifndef __BARE_TEST_H__
#define __BARE_TEST_H__

/*
*********************************************************************************************************
*                                         DVFS BARE TEST
*
* Description:  dvfs bare test.
*
* Arguments  :  none.
*
* Returns    :  none.
*********************************************************************************************************
*/
void dvfs_bare_test(void);

/*
*********************************************************************************************************
*                                         STANDBY BARE TEST
*
* Description:  standby bare test.
*
* Arguments  :  none.
*
* Returns    :  none.
*********************************************************************************************************
*/
void standby_bare_test(void);

/*
*********************************************************************************************************
*                                         PM BARE TEST
*
* Description:  pm bare test.
*
* Arguments  :  none.
*
* Returns    :  none.
*********************************************************************************************************
*/
void pm_bare_test(void);

/*
*********************************************************************************************************
*                                         AR100 BARE TEST
*
* Description:  ar100 bare test.
*
* Arguments  :  none.
*
* Returns    :  none.
*********************************************************************************************************
*/
void ar100_test(void);

#endif //__BARE_TEST_H__