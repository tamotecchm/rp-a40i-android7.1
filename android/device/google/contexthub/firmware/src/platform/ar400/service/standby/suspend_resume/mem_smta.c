#include <plat/inc/include.h>

#if MEM_USED
#include "mem_smta.h"

int mem_smta_save(void)
{
	mem_reg_save(system_back->smta_back.smta_reg_back, \
	             (const unsigned int *)IO_ADDRESS(SUNXI_SMTA_PBASE + SMTA_REG_START), SMTA_REG_LENGTH, \
	             SMTA_SKIP);

	//mem_reg_debug(__func__, IO_ADDRESS(SUNXI_SMTA_PBASE), SMTA_REG_LENGTH * SMTA_SKIP);

	return OK;
}

int mem_smta_restore(void)
{
	/*
	 * default is secure, so we only need restore no-secure
	 */
	mem_reg_restore((unsigned int *)IO_ADDRESS(SUNXI_SMTA_PBASE + SMTA_REG_START + 0x04), \
	                (const unsigned int *)system_back->smta_back.smta_reg_back, SMTA_REG_LENGTH, \
	                SMTA_SKIP);

	//mem_reg_debug(__func__, IO_ADDRESS(SUNXI_SMTA_PBASE), SMTA_REG_LENGTH * SMTA_SKIP);

	return OK;
}

#endif /* STANDBY_USED */
