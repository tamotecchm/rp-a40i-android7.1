/*
*********************************************************************************************************
*                                                AR100 SYSTEM
*                                     AR100 Software System Develop Kits
*                                                DVFS module
*
*                                    (c) Copyright 2012-2016, Sunny China
*                                             All Rights Reserved
*
* File    : dvfs_i.h
* By      : Sunny
* Version : v1.0
* Date    : 2012-5-14
* Descript: DVFS module internal header.
* Update  : date                auther      ver     notes
*           2012-5-14 8:57:52   Sunny       1.0     Create this file.
*********************************************************************************************************
*/

#ifndef __DVFS_I_H__
#define __DVFS_I_H__

#include <plat/inc/include.h>

//local functions
s32 dvfs_set_freq_fb(s32 result);

#endif  //__DVFS_I_H__
