#define TDA7786_PROTOCOL_IMPLEMENTATION

//#include  "drv_fm_i.h"
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>


#include "tda7786_protocol.h"

//#if (FM_MODULE_TYPE == FM_MODULE_TYPE_TDA7786) || (FM_MODULE_TYPE == FM_MODULE_TYPE_UNION)

//#include "tda7786_boot_data.inc"

#define TDA7703_I2C_WR_BUF_LEN	(255)

static __u8 tda7703_buffer[TDA7703_I2C_WR_BUF_LEN];

#if 0

static void HIT_i2c_delay(u8 delay_time)
{
	__u16 i = 0;
	for(; i < delay_time; i++) 
	{ 
	} 
}

static unsigned char AW_I2C_ReadXByte( unsigned char *buf, unsigned char addr, unsigned short len)
{
	int ret,i;
	u8 rdbuf[512] = {0};
	struct i2c_msg msgs[] = {
		{
			.addr	= this_client->addr,
			.flags	= 0,
			.len	= 1,
			.buf	= rdbuf,
		},
		{
			.addr	= this_client->addr,
			.flags	= I2C_M_RD,
			.len	= len,
			.buf	= rdbuf,
		},
	};

	rdbuf[0] = addr; //modify by wxh
	ret = i2c_transfer(this_client->adapter, msgs, 2);
	if (ret < 0)
		pr_err("msg %s i2c read error: %d\n", __func__, ret);

	for(i = 0; i < len; i++)
	{
		buf[i] = rdbuf[i];
	}

    return ret;
}

static unsigned char AW_I2C_WriteXByte(const unsigned char *buf, unsigned char addr, unsigned short len)
{
	int ret,i;
	u8 wdbuf[512] = {0};
	struct i2c_msg msgs[] = {
		{
			.addr	= this_client->addr,
			.flags	= 0,
			.len	= len+1,
			.buf	= wdbuf,
		}
	};
	wdbuf[0] = addr;  //modify by wxh
	for(i = 0; i < len; i++)
	{
		//wdbuf[i] = buf[i];
		wdbuf[i+1] = buf[i]; //modify by wxh
	}
	//msleep(1);
	ret = i2c_transfer(this_client->adapter, msgs, 1);
	if (ret < 0)
		pr_err("msg %s i2c read error: %d\n", __func__, ret);
    return ret;
}


void TDA7786_HIT_DirectWrite(unsigned char DataNum, u8 *AddrBuf, u8 *WriteBuf, unsigned char mode, unsigned char cmd)//bool cmd)
{
	unsigned int i,j;
	unsigned int checksum[4]={0,0,0,0};
	//DI;//asm sim;
	unsigned int buf_cnt=0;
	u8 buf[512] = {0};
	switch(mode) 
	{
	case TDA7786_RW_NO_AUTOINC_32BIT: 
#if 0
	HIT_StartTWI();
		HIT_TxByteDirect(TDA7786_I2C_MAIN_ADDR);//chipaddress
	  	HIT_TxByteDirect((AddrBuf[0]&0x01)|0x80);//mode+addr1
	  	HIT_TxByteDirect(AddrBuf[1]);
		HIT_TxByteDirect(AddrBuf[2]);
	  	HIT_TxByteDirect(WriteBuf[0]);
	  	HIT_TxByteDirect(WriteBuf[1]);
	  	HIT_TxByteDirect(WriteBuf[2]);
	  	HIT_TxByteDirect(WriteBuf[3]);
		HIT_StopTWI();
#endif		
		break;
	case TDA7786_RW_AUTOINC_24BIT:
	
				
		buf[0] = (AddrBuf[0]&0x01)|0xf0;
		buf[1] = AddrBuf[1];
		buf[2] = AddrBuf[2];
		buf_cnt =2;
		
		//HIT_StartTWI();
		//HIT_TxByteDirect(TDA7786_I2C_MAIN_ADDR);//chipaddress
	  	//HIT_TxByteDirect((AddrBuf[0]&0x01)|0xf0);//mode+addr1
	  	//HIT_TxByteDirect(AddrBuf[1]);
		//HIT_TxByteDirect(AddrBuf[2]);
		for(i=0;i<DataNum;i++)
		{
			//HIT_TxByteDirect(WriteBuf[i*3]);
			//HIT_TxByteDirect(WriteBuf[i*3+1]);
			//HIT_TxByteDirect(WriteBuf[i*3+2]);
			buf[++buf_cnt] = WriteBuf[i*3];
			buf[++buf_cnt] = WriteBuf[i*3+1];
			buf[++buf_cnt] = WriteBuf[i*3+2];
			if(cmd)
			{
				for(j=2; j>=1;j--)
				{
				 	checksum[j]=checksum[j]+WriteBuf[i*3+j];
					if (checksum[j] >= 256)
					{
			                    checksum[j - 1] = checksum[j - 1] + 1;
			                    checksum[j] = checksum[j] - 256;
					}
				}
				 checksum[0]=checksum[0]+WriteBuf[i*3];
				 if (checksum[0]>=256)
				 	checksum[0]=checksum[0]-256;

			}
		}
		if(cmd)
		{
			//HIT_TxByteDirect(checksum[0]&0xff);
			//HIT_TxByteDirect(checksum[1]&0xff);
			//HIT_TxByteDirect(checksum[2]&0xff);
			buf[++buf_cnt] = checksum[0]&0xff;
			buf[++buf_cnt] = checksum[1]&0xff;
			buf[++buf_cnt] = checksum[2]&0xff;
		}
		
		//HIT_StopTWI();
	
		//AW_I2C_WriteXByte(buf,TDA7786_I2C_MAIN_ADDR,buf_cnt+1);
		AW_I2C_WriteXByte(buf,TDA7786_I2C_MAIN_ADDR,buf_cnt+1);//modify by wxh
		break;
	default:
		break;
	}

	//EI;//asm rim;

}
/*********************************************
	Function:		HIT_DirectRead
	Description:	Directly read data from HIT
	Write/Modify:	Yete HUANG
	Time:		2008-March
*********************************************/
void TDA7786_HIT_DirectRead(unsigned int DataNum, u8 *AddrBuf, u8 *ReadBuf, unsigned char mode)//bool cmd)
{
	unsigned int i;
	//DI;//asm sim;
	unsigned int tmp=DataNum;
	unsigned int buf_cnt=0;
	//u8 buf_recv[40] = {0};
	u8 buf_send[3] ={0};
	//HIT_StartTWI();
	//HIT_TxByteDirect(TDA7786_I2C_MAIN_ADDR);//chipaddress/write
	switch(mode)
	{
	case TDA7786_RW_NO_AUTOINC_32BIT:

		break;
	case TDA7786_RW_AUTOINC_24BIT:
 
	//HIT_StartTWI();
		//HIT_TxByteDirect(TDA7786_I2C_MAIN_ADDR);//chipaddress/write
  		//HIT_TxByteDirect((AddrBuf[0]&0x01)|0x70);//mode+addr1
	  	//HIT_TxByteDirect(AddrBuf[1]);
		//HIT_TxByteDirect(AddrBuf[2]);
		//HIT_StopTWI();
		buf_send[0] = (AddrBuf[0]&0x01)|0x70;
		buf_send[1] = AddrBuf[1];
		buf_send[2] = AddrBuf[2];
		buf_cnt+=2;
		AW_I2C_WriteXByte(buf_send,TDA7786_I2C_MAIN_ADDR,buf_cnt+1);
		mdelay(5);
		buf_cnt = 0;
		
		
		//HIT_i2c_delay(50);
		//HIT_StartTWI();
		//HIT_TxByteDirect(TDA7786_I2C_MAIN_ADDR|0x01);//chipaddress/read
		//HIT_i2c_delay(50);

		/*for(i=0;i<tmp;i++)
		{
			ReadBuf[i*3]=HIT_RxByte();
			HIT_AckTWI();
			ReadBuf[i*3+1]=HIT_RxByte();
			HIT_AckTWI();
			ReadBuf[i*3+2]=HIT_RxByte();	
			if(i==DataNum-1)
			{
				HIT_nAckTWI();
			}
			else
			{
				HIT_AckTWI();
			}
		}*/
		
		AW_I2C_ReadXByte(ReadBuf,TDA7786_I2C_MAIN_ADDR,(tmp-1)*3+3);
		//HIT_StopTWI();
	
		break;
	default:
		break;
	}
	//EI;//asm rim;

}

/*********************************************
	Function:		HIT_BootcodeDownload
	Description:	Download the script file data
	Write/Modify:	Yete HUANG
	Time:		March-2008
*********************************************/
void TDA7786_HIT_BootcodeDownload(void)
{
	unsigned char num;
	unsigned int i,j;
	
	u8 buf[512]={0};
	unsigned int buf_cnt=0;
	//DI;							//asm sim;
		i=0;	
	while(tda7786_I2CBootData[i]!=0xff)
	{
		num=tda7786_I2CBootData[i];
		//HIT_StartTWI();
		//HIT_TxByteDirect(TDA7786_I2C_MAIN_ADDR);//chipaddress
	  	//HIT_TxByteDirect((tda7786_I2CBootData[i+1]&0x01)|0xe0);//addr1
	  	//HIT_TxByteDirect(tda7786_I2CBootData[i+2]);
		//HIT_TxByteDirect(tda7786_I2CBootData[i+3]);
		buf[0] = (tda7786_I2CBootData[i+1]&0x01)|0xe0;
		buf[1] =  tda7786_I2CBootData[i+2];
		buf[2] =  tda7786_I2CBootData[i+3];
		buf_cnt = 2;
		for(j=i+4;j<i+4*num+4;j++)
		{
			//HIT_TxByteDirect(tda7786_I2CBootData[j]);
			buf[++buf_cnt] = tda7786_I2CBootData[j];
		}
		//HIT_StopTWI();
		i=i+4*num+4;
		AW_I2C_WriteXByte(buf,TDA7786_I2C_MAIN_ADDR,buf_cnt+1);
		
	}
	//EI;
}

#endif


//#endif

