/*
 * rk29_es8323.c  --  SoC audio for rockchip
 *
 * Driver for rockchip es8323 audio
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 *
 *
 */

#include <linux/module.h>
#include <linux/device.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/gpio.h>
#include <sound/core.h>
#include <linux/platform_device.h>
#include <linux/timer.h>
#include <linux/sched.h>
#include <linux/regulator/consumer.h>
#include <linux/miscdevice.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/sys_config.h>


static int gpio_handler[255], count = 0;
static char gpio_name[16];
static struct timer_list led_timer;
static bool led_status = true;
static char current_name[8];
static char current_value = 0;

#define INTERVAL 400

static void set_led_status(unsigned long arg)
{
    int i;

    for (i = 0; i < 1; i++) {
        if (gpio_handler[i] == 0)
            break;
        gpio_direction_output(gpio_handler[i], led_status);
    }

    led_status = !led_status;
    mod_timer(&led_timer, jiffies + msecs_to_jiffies(INTERVAL));
}


static int gpio_remove(struct platform_device *pdev)
{
    return 0;
}


static ssize_t gzsd_input_write(struct file *file, 
			const char __user *buffer, size_t len, loff_t *offset)
{
    return 0;
}

static ssize_t gzsd_input_read (struct file *filp, char __user *buffer, size_t len, loff_t *ff_t) {

    return 0;
}

static int gzsd_input_open(struct inode *inode, struct file *filp)
{
    return 0;
}

static int gzsd_input_release(struct inode *inode, struct file *filp)
{
    return 0;
}

static const struct file_operations gzsd_dev_fops = {
    //.owner  = THIS_MODULE,
    .open   = gzsd_input_open,
    .write  = gzsd_input_write,
    .read  = gzsd_input_read,
    .release = gzsd_input_release,
};

static struct miscdevice misc = {
    .minor  = MISC_DYNAMIC_MINOR,
    .name   = "gpio_ctl",
    .fops   = &gzsd_dev_fops,
};

static int gpio_used = 0;
static int gpio_probe(struct platform_device *pdev)
{
    struct device_node *np = pdev->dev.of_node;
    enum of_gpio_flags flags;
    struct gpio_config config;
    int i, ret;
    if (np == NULL)
        return -1;

    memset(gpio_handler, 0, sizeof(gpio_handler));

    ret = of_property_read_u32(np, "gpio_used", &gpio_used);
    if (ret) {
        printk("read gpio_used err\n");
        return -1;
    }

    printk("gpio used = %d\n", gpio_used);
    if (gpio_used == 0)
        return 0;

    ret = of_property_read_u32(np, "gpio_count", &count);
    printk("gpio_count = %d\n", count);
    for (i = 0; i < count; i++) {
        sprintf(gpio_name, "gpio%d", i);
        ret = of_get_named_gpio_flags(np, gpio_name, 0, (enum of_gpio_flags *)&config);
        printk("sclu ret=%d\n", ret);
        if (!gpio_is_valid(ret)) {
            printk("%s get gpio%d fail\n", __func__, i);
        } else {
            gpio_handler[i] = ret;
#if 1
            ret = devm_gpio_request(&pdev->dev, gpio_handler[i], gpio_name);
            if (ret < 0) { 
                printk("gpio%d request fail\n", i);
            }  
            gpio_direction_output(gpio_handler[i], config.data > 0 ? 1 : 0);
#endif
        }
    }

    misc_register(&misc);
    init_timer(&led_timer);
    led_timer.expires = jiffies + msecs_to_jiffies(INTERVAL);
    led_timer.data = 100;
    led_timer.function = set_led_status;
    add_timer(&led_timer);


    return 0;
}


#ifdef CONFIG_OF
static const struct of_device_id gpio_of_match[] = {
    { .compatible = "allwinner,sun8i-led-para", },
    {},
};
MODULE_DEVICE_TABLE(of, gpio_of_match);
#endif /* CONFIG_OF */

static struct platform_driver led_gpio_ctl_platform_driver = {
    .driver         = {
        .name   = "led-gpio-para",
        .owner  = THIS_MODULE,
        .of_match_table = of_match_ptr(gpio_of_match),
    },
    .probe          = gpio_probe,
    .remove         = gpio_remove,
};


static int __init led_gpio_ctl_init(void)
{        	
	printk("%s begin!\n", __func__);
	platform_driver_register(&led_gpio_ctl_platform_driver);

	return 0;
}

static void __exit led_gpio_ctl_exit(void)
{
        printk("%s\n", __func__); 
		platform_driver_unregister(&led_gpio_ctl_platform_driver);
    
}
/*************************************************************************************/

/* Module information */
MODULE_AUTHOR("allwinner");
MODULE_DESCRIPTION("gpio control para");
MODULE_LICENSE("GPL");
MODULE_VERSION("1.1");

module_init(led_gpio_ctl_init);
module_exit(led_gpio_ctl_exit);

