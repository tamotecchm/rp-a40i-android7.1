#include "N106_panel.h"

extern s32 bsp_disp_get_panel_info(u32 screen_id, disp_panel_para *info);
static void LCD_power_on(u32 sel);
static void LCD_power_off(u32 sel);
static void LCD_bl_open(u32 sel);
static void LCD_bl_close(u32 sel);

static void LCD_panel_init(u32 sel);
static void LCD_panel_exit(u32 sel);

static u8 const mipi_dcs_pixel_format[4] = {0x77, 0x66, 0x66, 0x55};
#define panel_reset(val) sunxi_lcd_gpio_set_value(sel, 0, val)

static void LCD_cfg_panel_info(panel_extend_para *info)
{
	u32 i = 0, j = 0;
	u32 items;
	u8 lcd_gamma_tbl[][2] = {
		/*{input value, corrected value}*/
		{0, 0},
		{15, 15},
		{30, 30},
		{45, 45},
		{60, 60},
		{75, 75},
		{90, 90},
		{105, 105},
		{120, 120},
		{135, 135},
		{150, 150},
		{165, 165},
		{180, 180},
		{195, 195},
		{210, 210},
		{225, 225},
		{240, 240},
		{255, 255},
	};

	u32 lcd_cmap_tbl[2][3][4] = {
	{
		{LCD_CMAP_G0, LCD_CMAP_B1, LCD_CMAP_G2, LCD_CMAP_B3},
		{LCD_CMAP_B0, LCD_CMAP_R1, LCD_CMAP_B2, LCD_CMAP_R3},
		{LCD_CMAP_R0, LCD_CMAP_G1, LCD_CMAP_R2, LCD_CMAP_G3},
		},
		{
		{LCD_CMAP_B3, LCD_CMAP_G2, LCD_CMAP_B1, LCD_CMAP_G0},
		{LCD_CMAP_R3, LCD_CMAP_B2, LCD_CMAP_R1, LCD_CMAP_B0},
		{LCD_CMAP_G3, LCD_CMAP_R2, LCD_CMAP_G1, LCD_CMAP_R0},
		},
	};

	items = sizeof(lcd_gamma_tbl)/2;
	for (i = 0; i < items-1; i++) {
		u32 num = lcd_gamma_tbl[i+1][0] - lcd_gamma_tbl[i][0];

		for (j = 0; j < num; j++) {
			u32 value = 0;

			value = lcd_gamma_tbl[i][1] + ((lcd_gamma_tbl[i+1][1] - lcd_gamma_tbl[i][1]) * j)/num;
			info->lcd_gamma_tbl[lcd_gamma_tbl[i][0] + j] = (value<<16) + (value<<8) + value;
		}
	}
	info->lcd_gamma_tbl[255] = (lcd_gamma_tbl[items-1][1]<<16) + (lcd_gamma_tbl[items-1][1]<<8) + lcd_gamma_tbl[items-1][1];

	memcpy(info->lcd_cmap_tbl, lcd_cmap_tbl, sizeof(lcd_cmap_tbl));

}

static s32 LCD_open_flow(u32 sel)
{
	LCD_OPEN_FUNC(sel, LCD_power_on, 30);   //open lcd power, and delay 50ms
	printf("flow power on\n");
	LCD_OPEN_FUNC(sel, LCD_panel_init, 50);   //open lcd power, than delay 200ms
	LCD_OPEN_FUNC(sel, sunxi_lcd_tcon_enable, 100);     //open lcd controller, and delay 100ms
	LCD_OPEN_FUNC(sel, LCD_bl_open, 0);     //open lcd backlight, and delay 0ms
	
	return 0;
}

static s32 LCD_close_flow(u32 sel)
{
	LCD_CLOSE_FUNC(sel, LCD_bl_close, 200);
	LCD_CLOSE_FUNC(sel, sunxi_lcd_tcon_disable, 10);
	LCD_CLOSE_FUNC(sel, LCD_panel_exit, 200);
	LCD_CLOSE_FUNC(sel, LCD_power_off, 500);
	return 0;
}

static void LCD_power_on(u32 sel)
{
	sunxi_lcd_gpio_set_value(sel,2,0);
	sunxi_lcd_delay_ms(10);
	sunxi_lcd_power_enable(sel, 0);//config lcd_power pin to open lcd power0
	sunxi_lcd_backlight_enable(sel);
	sunxi_lcd_delay_ms(10);
	sunxi_lcd_gpio_set_value(sel,0,1);
	sunxi_lcd_delay_ms(10);
	sunxi_lcd_gpio_set_value(sel,1,1);
	sunxi_lcd_delay_ms(10);
	sunxi_lcd_gpio_set_value(sel,2,1);
	sunxi_lcd_delay_ms(10);
	sunxi_lcd_gpio_set_value(sel,3,1);
	printf("power on\n");
	sunxi_lcd_pin_cfg(sel, 1);
}

static void LCD_power_off(u32 sel)
{
	sunxi_lcd_pin_cfg(sel, 0);
	sunxi_lcd_delay_ms(20);
	panel_reset(0);
	sunxi_lcd_delay_ms(5);
	sunxi_lcd_power_disable(sel, 1);
	sunxi_lcd_delay_ms(5);
	sunxi_lcd_power_disable(sel, 0);
}

static void LCD_bl_open(u32 sel)
{
	sunxi_lcd_pwm_enable(sel);
	sunxi_lcd_backlight_enable(sel);
}

static void LCD_bl_close(u32 sel)
{
	sunxi_lcd_backlight_disable(sel);
	sunxi_lcd_pwm_disable(sel);
}

#define REGFLAG_DELAY	0XFE
#define REGFLAG_END_OF_TABLE	0xFD  /*END OF REGISTERS MARKER*/

struct LCM_setting_table {
    u8 cmd;
    u32 count;
    u8 para_list[64];
};

/*add panel initialization below*/


static struct LCM_setting_table LCM_LTL106HL02_setting[] = {

			{0xb7, 02, {0x50, 0x00}},
			{0xb8, 02, {0x00, 0x00}},
			{0xb9, 02, {0x00, 0x00}},
			{0xba, 02, {0x14, 0x42}},
			{0xbb, 02, {0x03, 0x00}},
			{0xb9, 02, {0x01, 0x00}},
			{0xde, 02, {0x03, 0x00}},
			{0xc9, 02, {0x02, 0x23}},
			{0xb0, 01, {0x00}},
			{0x14, 05, {0x08, 0xb0, 0x00, 0x22, 0x00}},
			{0xb4, 01, {0x0c}},
			{0xb6, 02, {0x3a, 0xd3}},
			{0x51, 01, {0xe6}},
			{0x53, 01, {0x2c}},
			
 		{0x29,     0,    {0x00}},
 		{REGFLAG_DELAY, REGFLAG_DELAY, {120}},
		{0x11,			0,     {0x00} },
		{REGFLAG_DELAY, REGFLAG_DELAY, {120}},
			
			
			{0xb7, 02, {0x50, 0x00}},
			{0xb8, 02, {0x00, 0x00}},
			{0xb9, 02, {0x00, 0x00}},
			{0xba, 02, {0x8c, 0x83}},
			{0xbb, 02, {0x03, 0x00}},
			{0xb9, 02, {0x01, 0x00}},
			{0xc9, 02, {0x02, 0x23}},
			{0xca, 02, {0x01, 0x23}},
			{0xcb, 02, {0x10, 0x05}},
			{0xcc, 02, {0x05, 0x10}},
			{0xd0, 02, {0x00, 0x00}},
			{0xb6, 02, {0x03, 0x00}},
			{0xde, 02, {0x03, 0x00}},
			{0xd6, 02, {0x05, 0x00}},
			{0xb7, 02, {0x4b, 0x02}},
			{0x2c, 0, {0x00}},
		
	{REGFLAG_END_OF_TABLE, REGFLAG_END_OF_TABLE, {} }
};

static void LCD_panel_init(u32 sel)
{
	u32 i;
	
	printf("\n%s\n\n",__func__);

	sunxi_lcd_dsi_clk_enable(sel);
	sunxi_lcd_delay_ms(100);

	for (i = 0; ; i++) {
			if (LCM_LTL106HL02_setting[i].count == REGFLAG_END_OF_TABLE)
				break;
			else if (LCM_LTL106HL02_setting[i].count == REGFLAG_DELAY)
				sunxi_lcd_delay_ms(LCM_LTL106HL02_setting[i].para_list[0]);
			else{
				//printf("%d %x %x %x\n",i, LCM_LTL106HL02_setting[i].cmd, LCM_LTL106HL02_setting[i].para_list[0], LCM_LTL106HL02_setting[i].count);
				dsi_dcs_wr(sel, LCM_LTL106HL02_setting[i].cmd, LCM_LTL106HL02_setting[i].para_list, LCM_LTL106HL02_setting[i].count);
				}
	}
	printf("\n\t====>%s, 7inch 1200 init finished!\n", __func__);
	return;
}

static void LCD_panel_exit(u32 sel)
{
	sunxi_lcd_dsi_dcs_write_0para(sel, DSI_DCS_SET_DISPLAY_OFF);
	sunxi_lcd_delay_ms(20);
	sunxi_lcd_dsi_dcs_write_0para(sel, DSI_DCS_ENTER_SLEEP_MODE);
	sunxi_lcd_delay_ms(80);

	return ;
}

/*sel: 0:lcd0; 1:lcd1*/
static s32 LCD_user_defined_func(u32 sel, u32 para1, u32 para2, u32 para3)
{
	return 0;
}

/*sel: 0:lcd0; 1:lcd1
static s32 LCD_set_bright(u32 sel, u32 bright)
{
	sunxi_lcd_dsi_dcs_write_1para(sel, 0x51, bright);
	return 0;
}*/

__lcd_panel_t rp_mipi_7_1200_panel = {
	/* panel driver name, must mach the name of lcd_drv_name in sys_config.fex */
	.name = "rp_mipi_7_1200_panel",
	.func = {
		.cfg_panel_info = LCD_cfg_panel_info,
		.cfg_open_flow = LCD_open_flow,
		.cfg_close_flow = LCD_close_flow,
		.lcd_user_defined_func = LCD_user_defined_func,
		/*.set_bright = LCD_set_bright,*/
	},
};
